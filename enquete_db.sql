-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 15 Juin 2017 à 18:47
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `enquete_db`
--

-- --------------------------------------------------------

--
-- Structure de la table `app_participant`
--

CREATE TABLE IF NOT EXISTS `app_participant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `etat` tinyint(1) NOT NULL,
  `fullName` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `contact` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(96) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `createdBy_id` int(11) DEFAULT NULL,
  `sexe` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8E7A4DADE7927C74` (`email`),
  UNIQUE KEY `UNIQ_8E7A4DAD4C62E638` (`contact`),
  KEY `IDX_8E7A4DAD3174800F` (`createdBy_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Contenu de la table `app_participant`
--

INSERT INTO `app_participant` (`id`, `etat`, `fullName`, `contact`, `email`, `createdAt`, `updatedAt`, `deletedAt`, `createdBy_id`, `sexe`) VALUES
(1, 1, 'MAVOUNGOU NadÃ¨ge', '066452525', 'm.nadege@gmail.com', '2017-04-20 10:37:32', '2017-04-20 10:37:32', NULL, 1, ''),
(2, 1, 'TATI paule', '055564545', 'paule.tati@gmail.com', '2017-04-20 10:39:08', '2017-04-20 10:39:08', NULL, 1, ''),
(3, 0, 'DUBOIS Marie-ThÃ©rÃ¨se', '053698585', 'dubois@yahoo.fr', '2017-04-20 10:41:01', '2017-04-20 10:41:01', NULL, 1, ''),
(4, 0, 'BOGARD Terry', '044998984', 'bogard@hotmail.com', '2017-04-20 10:43:38', '2017-04-20 10:43:38', NULL, 1, ''),
(5, 1, 'RETAT', '055242121', 'retate@gmail.com', '2017-04-20 10:45:12', '2017-04-20 10:45:12', '2017-04-20 10:45:21', 1, '');

-- --------------------------------------------------------

--
-- Structure de la table `app_participer`
--

CREATE TABLE IF NOT EXISTS `app_participer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sondage_id` smallint(6) NOT NULL,
  `participant_id` int(11) NOT NULL,
  `etat` tinyint(1) NOT NULL,
  `libelle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `createdBy_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BBA5BD84BAF4AE56` (`sondage_id`),
  KEY `IDX_BBA5BD849D1C3019` (`participant_id`),
  KEY `IDX_BBA5BD843174800F` (`createdBy_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Contenu de la table `app_participer`
--

INSERT INTO `app_participer` (`id`, `sondage_id`, `participant_id`, `etat`, `libelle`, `createdAt`, `updatedAt`, `deletedAt`, `createdBy_id`) VALUES
(1, 1, 4, 1, NULL, '2017-05-08 09:37:22', '2017-05-08 09:37:22', NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `app_proposition`
--

CREATE TABLE IF NOT EXISTS `app_proposition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `etat` tinyint(1) NOT NULL,
  `libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `createdBy_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9E28E5EF1E27F6BF` (`question_id`),
  KEY `IDX_9E28E5EF3174800F` (`createdBy_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Contenu de la table `app_proposition`
--

INSERT INTO `app_proposition` (`id`, `question_id`, `etat`, `libelle`, `createdAt`, `updatedAt`, `deletedAt`, `createdBy_id`) VALUES
(1, 1, 1, 'Espace de detente des communautaire', '2017-04-20 11:08:21', '2017-04-20 11:08:21', NULL, 1),
(2, 2, 0, 'Espace de santÃ© publique', '2017-04-20 11:06:56', '2017-04-20 11:06:56', NULL, 1),
(3, 1, 1, 'Espace entourÃ© d''herbe et de vÃ©gÃ©tations multiple', '2017-04-20 11:07:33', '2017-04-20 11:07:33', NULL, 1),
(4, 1, 0, 'Youpi', '2017-04-20 11:07:53', '2017-04-20 11:07:53', '2017-04-20 11:08:01', 1),
(5, 4, 1, 'oui ', '2017-05-15 12:13:15', '2017-05-15 12:13:15', NULL, 1),
(6, 4, 1, 'non ', '2017-05-15 12:13:15', '2017-05-15 12:13:15', NULL, 1),
(7, 4, 1, '', '2017-05-15 12:13:15', '2017-05-15 12:13:15', NULL, 1),
(8, 5, 1, '2000', '2017-05-15 12:13:15', '2017-05-15 12:13:15', NULL, 1),
(9, 5, 1, '5000', '2017-05-15 12:13:15', '2017-05-15 12:13:15', NULL, 1),
(10, 5, 1, 'PLus de 5000', '2017-05-15 12:13:15', '2017-05-15 12:13:15', NULL, 1),
(11, 7, 1, 'Natation', '2017-05-24 18:23:03', '2017-05-24 18:23:03', NULL, 1),
(12, 7, 1, 'Voyage', '2017-05-24 18:23:03', '2017-05-24 18:23:03', NULL, 1),
(13, 7, 1, 'Lecture', '2017-05-24 18:23:03', '2017-05-24 18:23:03', NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `app_question`
--

CREATE TABLE IF NOT EXISTS `app_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sondage_id` smallint(6) NOT NULL,
  `question_id` int(11) DEFAULT NULL,
  `proposition_id` int(11) DEFAULT NULL,
  `etat` tinyint(1) NOT NULL,
  `libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `typeQuestion_id` smallint(6) NOT NULL,
  `createdBy_id` int(11) DEFAULT NULL,
  `valeurHierachie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BE7729E3E6F35F35` (`typeQuestion_id`),
  KEY `IDX_BE7729E3BAF4AE56` (`sondage_id`),
  KEY `IDX_BE7729E31E27F6BF` (`question_id`),
  KEY `IDX_BE7729E3DB96F9E` (`proposition_id`),
  KEY `IDX_BE7729E33174800F` (`createdBy_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Contenu de la table `app_question`
--

INSERT INTO `app_question` (`id`, `sondage_id`, `question_id`, `proposition_id`, `etat`, `libelle`, `createdAt`, `updatedAt`, `deletedAt`, `typeQuestion_id`, `createdBy_id`, `valeurHierachie`) VALUES
(1, 1, NULL, NULL, 1, 'QU''est-ce qu''un espace vert', '2017-04-20 10:57:52', '2017-05-08 10:12:16', NULL, 4, 1, NULL),
(2, 1, 2, NULL, 0, 'Pourquoi avoir un espace vert', '2017-04-20 10:59:51', '2017-04-20 10:59:51', NULL, 3, 1, NULL),
(3, 3, NULL, NULL, 0, 'quel impact sur votre village', '2017-04-20 11:05:12', '2017-04-20 11:05:12', '2017-04-20 11:05:24', 1, 1, NULL),
(4, 6, NULL, NULL, 1, 'achetez vous vos graines en local ', '2017-05-15 12:13:15', '2017-05-15 12:13:15', NULL, 4, 1, NULL),
(5, 6, NULL, NULL, 1, 'A quel prix etes vous pret a acheter les semences ', '2017-05-15 12:13:15', '2017-05-15 12:13:15', NULL, 4, 1, NULL),
(6, 6, NULL, NULL, 1, '', '2017-05-15 12:13:15', '2017-05-15 12:13:15', NULL, 3, 1, NULL),
(7, 7, NULL, NULL, 1, 'Quels sont vos loisirs', '2017-05-24 18:23:03', '2017-05-24 18:23:03', NULL, 4, 1, NULL),
(8, 6, NULL, NULL, 1, 'utilisez vous des graines ', '2017-06-02 13:26:57', '2017-06-02 13:26:57', NULL, 3, 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `app_reponse`
--

CREATE TABLE IF NOT EXISTS `app_reponse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proposition_id` int(11) NOT NULL,
  `participant_id` int(11) NOT NULL,
  `etat` tinyint(1) NOT NULL,
  `libre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `createdBy_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B2EB5E8ADB96F9E` (`proposition_id`),
  KEY `IDX_B2EB5E8A9D1C3019` (`participant_id`),
  KEY `IDX_B2EB5E8A3174800F` (`createdBy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `app_sondage`
--

CREATE TABLE IF NOT EXISTS `app_sondage` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `groupe_id` smallint(6) NOT NULL,
  `etat` tinyint(1) NOT NULL,
  `libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `createdBy_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_982448D27A45358C` (`groupe_id`),
  KEY `IDX_982448D23174800F` (`createdBy_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Contenu de la table `app_sondage`
--

INSERT INTO `app_sondage` (`id`, `groupe_id`, `etat`, `libelle`, `createdAt`, `updatedAt`, `deletedAt`, `createdBy_id`) VALUES
(1, 4, 1, 'EnquÃªte sur les espaces verts', '2017-04-20 10:46:28', '2017-04-21 11:05:04', NULL, 1),
(2, 4, 1, 'Enquete etat des sols au congo', '2017-04-20 10:55:34', '2017-04-20 10:55:34', '2017-04-20 10:55:43', 1),
(3, 2, 1, 'Enquete authentifier ', '2017-04-20 10:51:24', '2017-04-20 10:51:24', NULL, 1),
(4, 1, 0, 'Enquete publique', '2017-04-20 10:54:50', '2017-04-20 10:54:50', NULL, 1),
(5, 1, 0, 'Enquete communautaire', '2017-04-20 10:55:14', '2017-04-20 10:55:14', NULL, 1),
(6, 4, 1, 'ETUDE DE MARCHE SUR LES SEMENCE ', '2017-05-15 12:13:15', '2017-05-15 12:13:15', NULL, 1),
(7, 4, 1, 'Test', '2017-05-24 18:23:03', '2017-05-24 18:23:03', NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `auth_acl_menu`
--

CREATE TABLE IF NOT EXISTS `auth_acl_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `classCss` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_7EF3DC065E237E06` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `auth_acl_privilege`
--

CREATE TABLE IF NOT EXISTS `auth_acl_privilege` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` smallint(6) NOT NULL,
  `ressource_id` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F7B670A2D60322AC` (`role_id`),
  KEY `IDX_F7B670A2FC6CD52A` (`ressource_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `auth_acl_ressource`
--

CREATE TABLE IF NOT EXISTS `auth_acl_ressource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `route_id` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `action` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `classCss` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E309AF6134ECB4E6` (`route_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `auth_acl_role`
--

CREATE TABLE IF NOT EXISTS `auth_acl_role` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `isDefault` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_549F6CFF5E237E06` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Contenu de la table `auth_acl_role`
--

INSERT INTO `auth_acl_role` (`id`, `createdAt`, `updatedAt`, `name`, `isDefault`) VALUES
(1, '2017-02-21 18:22:50', '2017-02-21 18:22:50', 'Administrateur', 0),
(2, '2017-02-21 18:23:08', '2017-02-21 18:23:08', 'Agent immobilier', 0),
(3, '2017-03-22 19:12:57', '2017-03-22 19:12:57', 'Superviseur', 0),
(4, '2017-04-11 18:38:40', '2017-04-11 18:38:50', 'Editeurs', 0);

-- --------------------------------------------------------

--
-- Structure de la table `auth_acl_route`
--

CREATE TABLE IF NOT EXISTS `auth_acl_route` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `module` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `controller` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `proteger` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_82A2B1545E237E06` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `auth_acl_sous_menu`
--

CREATE TABLE IF NOT EXISTS `auth_acl_sous_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `ressource_id` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_839E8EFACCD7E912` (`menu_id`),
  KEY `IDX_839E8EFAFC6CD52A` (`ressource_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `auth_utilisateur`
--

CREATE TABLE IF NOT EXISTS `auth_utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` smallint(6) NOT NULL,
  `createdAt` datetime NOT NULL,
  `fullName` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `contact` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(96) COLLATE utf8_unicode_ci NOT NULL,
  `loginAttempts` smallint(6) NOT NULL,
  `loginAttemptsTime` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `lastSignedIn` datetime DEFAULT NULL,
  `androidId` longtext COLLATE utf8_unicode_ci,
  `civilite` varchar(8) COLLATE utf8_unicode_ci DEFAULT 'M.',
  `iosId` longtext COLLATE utf8_unicode_ci,
  `blackberryId` longtext COLLATE utf8_unicode_ci,
  `firefoxId` longtext COLLATE utf8_unicode_ci,
  `windowId` longtext COLLATE utf8_unicode_ci,
  `updatedAt` datetime DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdBy_id` int(11) DEFAULT NULL,
  `participant_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_D10E112BE7927C74` (`email`),
  UNIQUE KEY `UNIQ_D10E112B4C62E638` (`contact`),
  KEY `IDX_D10E112BD60322AC` (`role_id`),
  KEY `IDX_D10E112B3174800F` (`createdBy_id`),
  KEY `IDX_D10E112B9D1C3019` (`participant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Contenu de la table `auth_utilisateur`
--

INSERT INTO `auth_utilisateur` (`id`, `role_id`, `createdAt`, `fullName`, `contact`, `email`, `loginAttempts`, `loginAttemptsTime`, `status`, `lastSignedIn`, `androidId`, `civilite`, `iosId`, `blackberryId`, `firefoxId`, `windowId`, `updatedAt`, `deletedAt`, `password`, `createdBy_id`, `participant_id`) VALUES
(1, 1, '2017-02-21 18:23:46', 'Wilfried Kouakou', '', 'wkouakou@gmail.com', 105, '2017-06-07 14:10:16', 1, '2017-06-07 14:10:16', NULL, NULL, NULL, NULL, NULL, NULL, '2017-06-07 14:10:16', NULL, '$2y$10$46WvQBZF3hf6z1jkOnwNy.RSBs4EtfL/wzcCPBWpPHW34E0mre8ty', NULL, NULL),
(2, 3, '2017-04-13 12:22:14', 'Remi OYONO', '058652356', 'rome.mouy@gmail.com', 0, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-13 12:22:14', NULL, '$2y$10$uUjfSg3ugcrtp82F3CRfHu0VXNT5DfmD7fqzNuNFKHOdqkn8vMmw2', 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `par_groupe`
--

CREATE TABLE IF NOT EXISTS `par_groupe` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `etat` tinyint(1) NOT NULL,
  `libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `createdBy_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_D0485A38A4D60759` (`libelle`),
  KEY `IDX_D0485A383174800F` (`createdBy_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Contenu de la table `par_groupe`
--

INSERT INTO `par_groupe` (`id`, `etat`, `libelle`, `createdAt`, `updatedAt`, `deletedAt`, `createdBy_id`) VALUES
(1, 1, 'Groupe A', '2017-04-20 10:31:24', '2017-04-20 10:31:24', NULL, 1),
(2, 1, 'Groupe B', '2017-04-20 10:31:36', '2017-04-20 10:31:36', NULL, 1),
(3, 1, 'Groupe F', '2017-04-20 10:32:21', '2017-04-20 10:32:21', '2017-04-20 10:32:31', 1),
(4, 0, 'Gourpe D', '2017-04-20 10:32:00', '2017-04-20 10:32:00', NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `par_type_question`
--

CREATE TABLE IF NOT EXISTS `par_type_question` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `etat` tinyint(1) NOT NULL,
  `libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `createdBy_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_914B1FBEA4D60759` (`libelle`),
  KEY `IDX_914B1FBE3174800F` (`createdBy_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Contenu de la table `par_type_question`
--

INSERT INTO `par_type_question` (`id`, `etat`, `libelle`, `createdAt`, `updatedAt`, `deletedAt`, `createdBy_id`, `type`) VALUES
(1, 1, 'Menu dÃ©roulant', '2017-04-20 10:24:20', '2017-05-24 18:35:14', NULL, 1, 'select'),
(2, 0, 'Logique', '2017-04-19 18:04:25', '2017-04-19 18:04:25', '2017-04-19 18:04:32', 1, NULL),
(3, 1, 'Basic', '2017-04-19 18:05:09', '2017-05-08 10:06:14', NULL, 1, 'checkbox'),
(4, 1, 'Question Ã  choix multiples', '2017-05-08 10:07:36', '2017-05-24 18:35:32', NULL, 1, 'checkbox'),
(5, 1, 'Saisie libre', '2017-05-24 18:36:17', '2017-05-24 18:36:17', NULL, 1, 'text');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `app_participant`
--
ALTER TABLE `app_participant`
  ADD CONSTRAINT `FK_8E7A4DAD3174800F` FOREIGN KEY (`createdBy_id`) REFERENCES `auth_utilisateur` (`id`);

--
-- Contraintes pour la table `app_participer`
--
ALTER TABLE `app_participer`
  ADD CONSTRAINT `FK_BBA5BD843174800F` FOREIGN KEY (`createdBy_id`) REFERENCES `auth_utilisateur` (`id`),
  ADD CONSTRAINT `FK_BBA5BD849D1C3019` FOREIGN KEY (`participant_id`) REFERENCES `app_participant` (`id`),
  ADD CONSTRAINT `FK_BBA5BD84BAF4AE56` FOREIGN KEY (`sondage_id`) REFERENCES `app_sondage` (`id`);

--
-- Contraintes pour la table `app_proposition`
--
ALTER TABLE `app_proposition`
  ADD CONSTRAINT `FK_9E28E5EF1E27F6BF` FOREIGN KEY (`question_id`) REFERENCES `app_question` (`id`),
  ADD CONSTRAINT `FK_9E28E5EF3174800F` FOREIGN KEY (`createdBy_id`) REFERENCES `auth_utilisateur` (`id`);

--
-- Contraintes pour la table `app_question`
--
ALTER TABLE `app_question`
  ADD CONSTRAINT `FK_BE7729E31E27F6BF` FOREIGN KEY (`question_id`) REFERENCES `app_question` (`id`),
  ADD CONSTRAINT `FK_BE7729E33174800F` FOREIGN KEY (`createdBy_id`) REFERENCES `auth_utilisateur` (`id`),
  ADD CONSTRAINT `FK_BE7729E3BAF4AE56` FOREIGN KEY (`sondage_id`) REFERENCES `app_sondage` (`id`),
  ADD CONSTRAINT `FK_BE7729E3DB96F9E` FOREIGN KEY (`proposition_id`) REFERENCES `app_proposition` (`id`),
  ADD CONSTRAINT `FK_BE7729E3E6F35F35` FOREIGN KEY (`typeQuestion_id`) REFERENCES `par_type_question` (`id`);

--
-- Contraintes pour la table `app_reponse`
--
ALTER TABLE `app_reponse`
  ADD CONSTRAINT `FK_B2EB5E8A3174800F` FOREIGN KEY (`createdBy_id`) REFERENCES `auth_utilisateur` (`id`),
  ADD CONSTRAINT `FK_B2EB5E8A9D1C3019` FOREIGN KEY (`participant_id`) REFERENCES `app_participant` (`id`),
  ADD CONSTRAINT `FK_B2EB5E8ADB96F9E` FOREIGN KEY (`proposition_id`) REFERENCES `app_proposition` (`id`);

--
-- Contraintes pour la table `app_sondage`
--
ALTER TABLE `app_sondage`
  ADD CONSTRAINT `FK_982448D23174800F` FOREIGN KEY (`createdBy_id`) REFERENCES `auth_utilisateur` (`id`),
  ADD CONSTRAINT `FK_982448D27A45358C` FOREIGN KEY (`groupe_id`) REFERENCES `par_groupe` (`id`);

--
-- Contraintes pour la table `auth_acl_privilege`
--
ALTER TABLE `auth_acl_privilege`
  ADD CONSTRAINT `FK_F7B670A2D60322AC` FOREIGN KEY (`role_id`) REFERENCES `auth_acl_role` (`id`),
  ADD CONSTRAINT `FK_F7B670A2FC6CD52A` FOREIGN KEY (`ressource_id`) REFERENCES `auth_acl_ressource` (`id`);

--
-- Contraintes pour la table `auth_acl_ressource`
--
ALTER TABLE `auth_acl_ressource`
  ADD CONSTRAINT `FK_E309AF6134ECB4E6` FOREIGN KEY (`route_id`) REFERENCES `auth_acl_route` (`id`);

--
-- Contraintes pour la table `auth_acl_sous_menu`
--
ALTER TABLE `auth_acl_sous_menu`
  ADD CONSTRAINT `FK_839E8EFACCD7E912` FOREIGN KEY (`menu_id`) REFERENCES `auth_acl_menu` (`id`),
  ADD CONSTRAINT `FK_839E8EFAFC6CD52A` FOREIGN KEY (`ressource_id`) REFERENCES `auth_acl_ressource` (`id`);

--
-- Contraintes pour la table `auth_utilisateur`
--
ALTER TABLE `auth_utilisateur`
  ADD CONSTRAINT `FK_D10E112B3174800F` FOREIGN KEY (`createdBy_id`) REFERENCES `auth_utilisateur` (`id`),
  ADD CONSTRAINT `FK_D10E112B9D1C3019` FOREIGN KEY (`participant_id`) REFERENCES `app_participant` (`id`),
  ADD CONSTRAINT `FK_D10E112BD60322AC` FOREIGN KEY (`role_id`) REFERENCES `auth_acl_role` (`id`);

--
-- Contraintes pour la table `par_groupe`
--
ALTER TABLE `par_groupe`
  ADD CONSTRAINT `FK_D0485A383174800F` FOREIGN KEY (`createdBy_id`) REFERENCES `auth_utilisateur` (`id`);

--
-- Contraintes pour la table `par_type_question`
--
ALTER TABLE `par_type_question`
  ADD CONSTRAINT `FK_914B1FBE3174800F` FOREIGN KEY (`createdBy_id`) REFERENCES `auth_utilisateur` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
