/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function notify(title, message) {
    jQuery.gritter.add({
        title: title,
        text: message,
        time: 20000,
        position: 'right'
    });
}
