<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Controller\Plugin;

/**
 * Description of AitAclPlugin
 *
 * @author Wilfried KOUAKOU
 */
use Exception;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Mvc\MvcEvent;
use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Zend\ServiceManager\ServiceManager;

class AitAclPlugin extends AbstractPlugin {

    protected $whitelist = array(
        'accueil',
        'auth',
        'access',
        'auth/default',
        'app/default',
        'auth/logout',
    );
    protected $sesscontainer;
    protected $serviceManager;
    //
    private $acl;
    protected $roles;
    protected $ressources;

    private function init(MvcEvent $e) {
        if (!$this->roles) {
            try {
                $roleService = $e->getApplication()->getServiceManager()->get('role_service');
                $this->setRoles($roleService->lister());
            } catch (Exception $exc) {
                $this->setRoles(array());
            }
        }
        if (!$this->ressources) {
            try {
                $ressourceService = $e->getApplication()->getServiceManager()->get('ressource_service');
                $this->setRessources($ressourceService->lister());
            } catch (Exception $exc) {
                $this->setRessources(array());
            }
        }
    }

    private function getPrivilege(MvcEvent $e, $idRole) {
        try {
            $privilegeService = $e->getApplication()->getServiceManager()->get('privilege_service');
            return $privilegeService->lister(array("active" => true, "role"=>$idRole));
        } catch (Exception $exc) {
            return array();
        }
    }

    public function doAuthorization(MvcEvent $e, $user) {
        $this->init($e);
        $idRole = $user->getRole()->getId();
        $roleName = $user->getRole()->getName() ? $user->getRole()->getName() : 'invite';

        // set ACL
        $this->acl = new Acl();
        $this->acl->deny(); // Par defaut, on retir tous les acces a tout le monde
        //$acl->allow(); // Si par defaut on veux donner tous les droits a tout le monde    
        //ROLES ############################################
        $this->acl->addRole(new Role('invite'));
        try {
            foreach ($this->getRoles() as $role) {
                if (!$this->acl->hasRole($role->getName())) {
                    $this->acl->addRole(new Role($role->getName()));
                }
            }
        } catch (Exception $exc) {
            //echo $exc->getTraceAsString();
        }

        //RESOURCES
        //Declaration de tous les ressources de l'application
        $this->addRessources($roleName);
        
        //PERMISSIONS
        if ($idRole === 1) { //Id = 1 : Administrateur
            $this->acl->allow($user->getRole()->getName());
        } else {
            $myPrivileges = $this->getPrivilege($e, $idRole);
            foreach ($myPrivileges as $privilege) {
                $this->acl->allow($roleName, $privilege->getRessource()->getRoute()->getName(), $privilege->getRessource()->getName());
            }
        }

        $layout = $e->getViewModel();
        $layout->setVariable("acl", $this->acl);

        //$controller = $e->getTarget();
        //$controllerClass = get_class($controller);

        $routeMatch = $e->getRouteMatch();
        $routeName = $routeMatch->getMatchedRouteName();

        $actionName = strtolower($routeMatch->getParam('action', 'not-found')); // get the action name  
        //$urlController = $routeMatch->getParam('controller', 'not-found');     // get the controller name    
        //$explodeController = explode('\\', $urlController);
        //$controllerName = strtolower(array_pop($explodeController));
        #################### Check Access ########################
        if (!$this->acl->isAllowed($roleName, $routeName, $actionName)) {
            $router = $e->getRouter();
            $url = $router->assemble(array(), array('name' => 'access'));
            $response = $e->getResponse();
            $response->setStatusCode(302);
            // redirect to privilege page.
            $response->getHeaders()->addHeaderLine('Location', $url);
            $e->stopPropagation();
        }
    }

    /**
     * Retrieve service manager instance
     *
     * @return ServiceManager
     */
    public function getServiceManager() {
        return $this->serviceManager->getServiceLocator();
    }

    /**
     * Set service manager instance
     *
     * @param ServiceManager $serviceManager
     * @return void
     */
    public function setServiceManager(ServiceManager $serviceManager) {
        $this->serviceManager = $serviceManager;
    }

    public function getRoles() {
        return $this->roles;
    }

    public function getDefaultRessources() {
        return $this->defaultRessources;
    }

    public function getRessources() {
        return $this->ressources;
    }

    public function setRoles($roles) {
        $this->roles = $roles;
        return $this;
    }

    public function setDefaultRessources($defaultRessources) {
        $this->defaultRessources = $defaultRessources;
        return $this;
    }

    public function setRessources($ressources) {
        $this->ressources = $ressources;
        return $this;
    }

    public function addRessources($roleName) {
        try {
            //Default ressource
            foreach ($this->whitelist as $defaulRessource) {
                if (!$this->acl->hasResource($defaulRessource)) {
                    $this->acl->addResource(new Resource($defaulRessource));
                    $this->acl->allow($roleName, $defaulRessource);
                }
            }
            //Own ressources
            foreach ($this->getRessources() as $ressource) {
                if (!$this->acl->hasResource($ressource->getRoute()->getName())) {
                    $this->acl->addResource(new Resource($ressource->getRoute()->getName()));
                    if ($ressource->getRoute()->getProtected() === FALSE) {
                        $this->acl->allow($roleName, $ressource->getRoute()->getName(), $ressource->getAction());
                    }
                }
            }
        } catch (Exception $exc) {
            //echo $exc->getTraceAsString();
        }
    }

}
