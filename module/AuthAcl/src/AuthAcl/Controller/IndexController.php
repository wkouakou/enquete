<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace AuthAcl\Controller;

use Application\Model\Entity\Utilisateur;
use DateTime;
use Exception;
use MailMan\Message;
use MailMan\Service\MailService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController {

    private $em;
    private $serializer;
    protected $storage;
    protected $authservice;

    public function init() {
        $this->em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $this->serializer = $this->getServiceLocator()->get('jms_serializer.serializer');
    }

    public function indexAction() {
        $this->layout()->setTemplate("layout/login");
        return new ViewModel();
    }

    public function inactiveAction() {
        return new ViewModel();
    }

    public function loginAction() {
        //$this->layout("layout/login");
        $responseData = array("code" => 0, "msg" => "Email ou mot de passe invalide", "data" => NULL);
        $request = $this->getRequest();
        $heders = $request->getHeaders();
        $token = false;
        $iToken = false;
        if ($heders->get("token") !== FALSE) {
            $token = $heders->get("token")->getFieldValue();
        }
        if ($heders->get("iToken") !== FALSE) {
            $iToken = $heders->get("iToken")->getFieldValue();
        }

        $authService = $this->getAuthService();
        if ($request->isPost()) {
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                $utilisateurService = $this->getServiceLocator()->get('utilisateur_service');

                $adapter = $authService->getAdapter();
                $adapter->setIdentity($data['email']);
                $adapter->setCredential($data['password']);
                $authResult = $authService->authenticate();

                if ($authResult->isValid()) {
                    $utilisateur = $this->identity();
                    $utilisateur->setLastSignedIn($dateOperation);
                    $utilisateur->setLoginAttempts($utilisateur->getLoginAttempts() + 1);
                    $utilisateur->setLoginAttemptsTime($dateOperation);
                    $utilisateur->setUpdatedAt($dateOperation);
                    $dataUser = $utilisateurService->editer($utilisateur);
                    //
                    if (isset($data["rememberMe"]) && $data["rememberMe"]) {
                        $this->getSessionStorage()->setRememberMe(1);
                    }
                    $this->getAuthService()->setStorage($this->getSessionStorage());

                    if ($token || $iToken) {
                        $responseData["code"] = 1;
                        $responseData["msg"] = "Authentification réussie";
                        $responseData["data"] = json_decode($this->serializer->serialize($dataUser, 'json'));
                        return new JsonModel($responseData);
                    }
                    elseif ($utilisateur->getStatus() === false) {
                        return $this->redirect()->toRoute('inactive');
                    }

                    return $this->redirect()->toRoute('home');
                }
                elseif ($utilisateur = $utilisateurService->getByEmail($data["email"])) {//Connexion échouée
                    $utilisateur->setLoginAttempts($utilisateur->getLoginAttempts() + 1);
                    $utilisateur->setLoginAttemptsTime($dateOperation);
                    $utilisateur->setUpdatedAt($dateOperation);
                    $dataUser = $utilisateurService->editer($utilisateur);
                    $responseData["code"] = 0;
                    $responseData["msg"] = "Mot de passe invalide";
//                    $responseData["data"] = json_decode($this->serializer->serialize($dataUser, 'json'));
                }
                else {
                    $responseData["msg"] = "Compte inexistant";
                }
            }
            catch (Exception $exc) {
                $responseData["msg"] = $exc->getMessage();
            }
        }
        $authService->clearIdentity();
        if ($token) {
            return new JsonModel($responseData);
        }
        return new ViewModel($responseData);
    }

    public function logoutAction() {
        $this->getSessionStorage()->forgetMe();
        $this->getAuthService()->clearIdentity();
        //TODO mobile deconnection
        $request = $this->getRequest();
        $heders = $request->getHeaders();
        $iToken = false;

        if ($heders->get("iToken") !== FALSE) {
            $iToken = $heders->get("iToken")->getFieldValue();
            if ($iToken) {
                $responseData["code"] = 1;
                $responseData["msg"] = "Deconnexion réussie";
                $responseData["data"] = NULL;
                return new JsonModel($responseData);
            }
        }

        return $this->redirect()->toRoute('auth');
    }

    public function pingAction() {
        return new JsonModel(array("ping" => "pong"));
    }

    public function resetPasswordAction() {
        return new ViewModel();
    }

    public function forgotPasswordAction() {
        //$this->layout("layout/login");
        $responseData = array("code" => -1, "msg" => "Requête non supportée", "data" => NULL);
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                $utilisateurService = $this->getServiceLocator()->get('utilisateur_service');
                $utilisateur = $utilisateurService->getByEmail($data["email"]);
                if (!$utilisateur) {
                    throw new Exception("Compte utilisateur inexistant.");
                }

                $password = Utilisateur::generatePassword();
                //Send email
                $message = new Message();
                //$message->addTextPart('Votre nouveau mot de passe est: <b>'.$password.'</b>');
                $message->addHtmlPart('Bonjour ' . $utilisateur->getCivilite() . ' ' . $utilisateur->getFullName() . ','
                        . '<br>Votre nouveau mot de passe est: <b>' . $password . '</b>'
                        . '<br><a href="http://www.ait-ci.com">AIT Stock</a> vous remercie pour votre confiance.');
                $message->setSubject('Réinitialisation de votre mot de passe.');
                $message->addFrom('info@ait-ci.com', 'AIT Stock');
                $message->addTo($utilisateur->getEmail(), $utilisateur->getFullName());
                /** @var MailService $mailService */
                $mailService = $this->getServiceLocator()->get('MailMan\Smtp');
                $mailService->send($message);

                $utilisateur->setPassword($password);
                $utilisateur->setUpdatedAt($dateOperation);
                $utilisateurService->editer($utilisateur);
                $responseData["code"] = 1;
                $responseData["data"] = json_decode($this->serializer->serialize($utilisateur, 'json'));
                $responseData["msg"] = "Votre mot de passe a été réinitialisé avec succès";
            }
            catch (Exception $exc) {
                $responseData["code"] = 0;
                $responseData["msg"] = $exc->getMessage();
            }
        }

        return new JsonModel($responseData);
    }

    public function privilegeAction() {
        return new ViewModel();
    }

    public function getAuthService() {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()->get('Zend\Authentication\AuthenticationService');
        }

        return $this->authservice;
    }

    public function getSessionStorage() {
        if (!$this->storage) {
            $this->storage = $this->getServiceLocator()->get('AitAuthStorage');
        }

        return $this->storage;
    }

    private function getUniqueMachineID($salt = "") {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
//        $temp = sys_get_temp_dir().DIRECTORY_SEPARATOR."diskpartscript.txt";
//        if(!file_exists($temp) && !is_file($temp)) file_put_contents($temp, "select disk 0\ndetail disk");
            $output = shell_exec("diskpart /s ");
            $lines = explode("\n", $output);
            $result = array_filter($lines, function($line) {
                return stripos($line, "ID:") !== false;
            });
            if (count($result) > 0) {
                $result = array_shift(array_values($result));
                $result = explode(":", $result);
                $result = trim(end($result));
            }
            else
                $result = $output;
        } else {
            $result = shell_exec("blkid -o value -s UUID");
            if (stripos($result, "blkid") !== false) {
                $result = $_SERVER['HTTP_HOST'];
            }
        }
        return md5($salt . md5($result));
    }

}
