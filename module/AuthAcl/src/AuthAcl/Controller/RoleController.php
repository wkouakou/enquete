<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace AuthAcl\Controller;

use AuthAcl\Model\Entity\Privilege;
use AuthAcl\Model\Entity\Role;
use DateTime;
use Exception;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class RoleController extends AbstractActionController {

    private $em;
    private $serializer;

    private function init() {
        $this->em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $this->serializer = $this->getServiceLocator()->get('jms_serializer.serializer');
    }

    public function indexAction() {
        //Variable Globale pour COMPTABILITE  liensInterne
        $this->layout()->setVariable("btnModalAjout", true);
        $this->layout()->setVariable("menuPrincipal", "ACL");
        $this->layout()->setVariable("icone", "fa fa-key");
        $this->layout()->setVariable("lienPrincipal", "/role");
        $this->layout()->setVariable("menuInterne", "role");
        $this->layout()->setVariable("titleControlleur", "Role");
        $this->layout()->setVariable("aclActive", "active");
        $this->layout()->setVariable("roleActive", "active");

        $viewData = array("listeRoles" => array());
        $this->init();
        $roleService = $this->getServiceLocator()->get('role_service');
        $viewData["listeRoles"] = $roleService->lister(array());
        return new ViewModel($viewData);
    }

    public function droitAction() {
        $this->init();
        $viewData = array("listeModule" => array(), "listeRoute" => array(), 'listeRole' => array());
        try {
            $idRole = $this->params()->fromRoute('id');

            $routeService = $this->getServiceLocator()->get('route_service');

            $roleService = $this->getServiceLocator()->get('role_service');

            $privilegeService = $this->getServiceLocator()->get('privilege_service');

            $viewData["listePrivilege"] = $privilegeService->getCheckedRessource();

//            print_r(count($viewData["listePrivilege"]));exit;

            $viewData["listeRole"] = $roleService->getRoleById($idRole);

            $viewData["listeRole"] = $roleService->getRoleById($idRole);

            $viewData["listeModule"] = $routeService->getModuleByRoute();

            /* ----- Routes --------------------------------------- */

            $viewData["listeRoute"] = $routeService->getRoute();

            $ressourceService = $this->getServiceLocator()->get('ressource_service');

            foreach ($viewData["listeRoute"] as $listeRoute) {

                $routeRessource[$listeRoute->getId()] = $ressourceService->getRessourceByRoute($listeRoute->getId());
                $nbr[] = count($ressourceService->getRessourceByRoute($listeRoute->getId()));
            }
            $viewData['nbr'] = max($nbr);
            $viewData['listeRessource'] = $routeRessource;
        } catch (Exception $ex) {
            //
        }
//        print_r($idRole);exit;

        return new ViewModel($viewData);
    }

    public function addPrivilegeAction() {

        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost()) {
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                $privilegeService = $this->getServiceLocator()->get('privilege_service');

                $privilege = new Privilege();

                /* -------------Role ---- */
                $roleService = $this->getServiceLocator()->get('role_service');
                $dataRole = $roleService->getRoleById($data['rol']);


                /* -------------Ressource ---- */
                $ressourceService = $this->getServiceLocator()->get('ressource_service');
                $dataRessource = $ressourceService->getRessourceById($data['ress']);

                $oldPrivilege = $privilegeService->getPrivilegeByRoleAndRessource($data['rol'], $data['ress']);

                if (count($oldPrivilege)) {
                    $etat = ($oldPrivilege->getActive() > 0) ? 0 : 1;
                    $oldPrivilege->setActive($etat);
                    $dataPrivilege = $privilegeService->editer($oldPrivilege);
//                    echo $this->ser->serialize($leprivilege, 'json');exit;
                    $msg = "Privilege modifie avec success";
                } else {
                    $privilege = new Privilege();
                    $privilege->setCreatedAt($dateOperation);
                    $privilege->setUpdatedAt($dateOperation);
                    $privilege->setRole($dataRole);
                    $privilege->setRessource($dataRessource);
                    $dataPrivilege = $privilegeService->editer($privilege);
                    $msg = "Privilege ajouter";
                }

                $jsonData["data"] = json_decode($this->serializer->serialize($dataPrivilege, 'json'));

                return new JsonModel($jsonData);
            } catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));


        return new ViewModel();
    }

    public function profileAction() {
        return new ViewModel();
    }

    public function ressourceAction() {
        return new ViewModel();
    }

    public function listAction() {
        $jsonData = array();
        $this->init();
        $roleService = $this->getServiceLocator()->get('role_service');

        $limit = $this->params()->fromQuery('limit', 10);
        $offset = $this->params()->fromQuery('offset', 0);

        $listeOperations = $roleService->lister(array(), array('name' => 'asc'), $limit, $offset);

        $tabData = array();
        foreach ($listeOperations as $role) {
            $tabData[] = json_decode($this->serializer->serialize($role, 'json'));
        }
        $countData = $roleService->lister();
        $jsonData["rows"] = $tabData;
        $jsonData["total"] = count($countData);
        return new JsonModel($jsonData);
    }

    public function addAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost()) {
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                $roleService = $this->getServiceLocator()->get('role_service');
                $role = new Role();
                $role->setCreatedAt($dateOperation);
                $role->setName($data["name"]);
                $role->setUpdatedAt($dateOperation);
                $dataRole = $roleService->editer($role);
                if ($roleService->initPrivilege($role)) {
                    $jsonData["msg"] = "Opération effectuée avec succès.\nPrivilège initialisé.";
                } else {
                    $jsonData["msg"] = "Opération effectuée avec succès.\nPrivilège non initialisé.";
                }
                $jsonData["data"] = json_decode($this->serializer->serialize($dataRole, 'json'));
                return new JsonModel($jsonData);
            } catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function updateAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost()) {
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                $roleService = $this->getServiceLocator()->get('role_service');
                $role = $roleService->getRoleById($data["idRole"]);
                if (!$role) {
                    throw new Exception("Impossible d'effectuer cette opération.\nVeuillez réessayer plus tard.");
                }
                $role->setName($data["name"]);
                $role->setUpdatedAt($dateOperation);
                $dataRole = $roleService->editer($role);
                $jsonData["data"] = json_decode($this->serializer->serialize($dataRole, 'json'));
                return new JsonModel($jsonData);
            } catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function deleteAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost()) {
            try {
                $this->init();
                $data = $request->getPost();
                $jsonData["test"] = $data;
                $roleService = $this->getServiceLocator()->get('role_service');
                $role = $roleService->getRoleById($data["id"]);
                if (!$role) {
                    throw new Exception("Impossible d'effectuer cette opération.\nVeuillez réessayer plus tard.");
                }
                $dataRole = $roleService->supprimer($role);
                $jsonData["data"] = json_decode($this->serializer->serialize($dataRole, 'json'));
                return new JsonModel($jsonData);
            } catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

}
