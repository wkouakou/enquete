<?php

/*
  ##############  Author : AFOLABI Jamal Deen
  ##############  Email  : jamaldeen25@gmail.com
  ##############  Date   : 27 avr. 2016 à 12:22:47
  ##############  File   : MenuController.php
  ################  Edit Part ###################
  ##############  Date   : MenuController.php
  ##############  Author : MenuController.php
 */

namespace AuthAcl\Controller;

use AuthAcl\Model\Entity\Menu;
use DateTime;
use Exception;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class MenuController extends AbstractActionController {

    private $serializer;
    private $user = null;

    private function init() {
        $this->serializer = $this->getServiceLocator()->get('jms_serializer.serializer');
        $this->user = $this->identity();
    }

    public function indexAction() {
        $viewData = array();

        //Variable Global pour COMPTABILITE  liensInterne
        $this->layout()->setVariable("btnModalAjout", true);
        $this->layout()->setVariable("menuPrincipal", "ACL");
        $this->layout()->setVariable("icone", "fa fa-key");
        $this->layout()->setVariable("lienPrincipal", "/menu");
        $this->layout()->setVariable("menuInterne", "role");
        $this->layout()->setVariable("titleControlleur", "Menu");
        $this->layout()->setVariable("aclActive", "active");
        $this->layout()->setVariable("menuActive", "active");

        return new ViewModel($viewData);
    }

    public function listerAction() {
        $jsonData = array();
        $this->init();
        $menuService = $this->getServiceLocator()->get('menu_service');

        $limit = $this->params()->fromQuery('limit', 10);
        $offset = $this->params()->fromQuery('offset', 0);

        $listeMenus = $menuService->lister(array(), array('name' => 'asc'), $limit, $offset);
        $countData = $menuService->lister(array());

        $tabData = array();
        foreach ($listeMenus as $menu) {
            $tabData[] = json_decode($this->serializer->serialize($menu, 'json'));
        }
        $jsonData["rows"] = $tabData;
        $jsonData["total"] = count($countData);
        return new JsonModel($jsonData);
    }

    public function ajouterAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["name"])) {
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                // apel des sevices des champs
                $menuService = $this->getServiceLocator()->get('menu_service');

                $menu = new Menu();
                $menu->setCreatedAt($dateOperation);
                $menu->setUpdatedAt($dateOperation);
                $menu->setName($data["name"]);
                $menu->setClassCss($data["classCss"]);

                $dataMenu = $menuService->editer($menu);
                $jsonData["data"] = json_decode($this->serializer->serialize($dataMenu, 'json'));
                return new JsonModel($jsonData);
            } catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function modifierAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        try {
            $this->init();
            $dateOperation = new DateTime('now');
            $data = $request->getPost();
            $menuService = $this->getServiceLocator()->get('menu_service');
            $menu = $menuService->getById($data["idMenu"]);
            if (!$menu) {
                throw new Exception("Impossible d'effectuer cette opération.\nVeuillez réessayer plus tard.");
            }
            $menu->setUpdatedAt($dateOperation);
            $menu->setName($data["name"]);
            $menu->setClassCss($data["classCss"]);

            $dataMenu = $menuService->editer($menu);
            $jsonData["data"] = json_decode($this->serializer->serialize($dataMenu, 'json'));
            return new JsonModel($jsonData);
        } catch (Exception $exc) {
            $jsonData["code"] = -1;
            $jsonData["data"] = NULL;
            $jsonData["msg"] = $exc->getMessage();
            return new JsonModel($jsonData);
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function supprimerAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["idMenu"])) {
            try {
                $this->init();
                $data = $request->getPost();
                $menuService = $this->getServiceLocator()->get('menu_service');
                $menu = $menuService->getById($data["idMenu"]);
                if (!$menu) {
                    throw new Exception("Impossible d'effectuer cette opération.\nVeuillez réessayer plus tard.");
                }
                $dataMenu = $menuService->supprimer($menu);
                $jsonData["data"] = json_decode($this->serializer->serialize($dataMenu, 'json'));
                return new JsonModel($jsonData);
            } catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

}
