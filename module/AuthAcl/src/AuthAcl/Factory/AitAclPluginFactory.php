<?php

namespace AuthAcl\Factory;

use AuthAcl\Controller\Plugin\AitAclPlugin;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Description of MyAclPluginFactory
 *
 * @author Afolabi Jamal Deen
 */
class AitAclPluginFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {
        $service = new AitAclPlugin();
        $service->setServiceManager($serviceLocator);
        return $service;
    }
}
