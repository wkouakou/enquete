<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Services;

use AuthAcl\Model\Entity\Route;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

/**
 *
 * @author wilfried
 */
interface RouteService extends ServiceLocatorAwareInterface{
    
    function editer(Route $route);
    
    function lister();
    
    function getDefaultRoutes();
    
    function getModuleByRoute();
    
    function getRoute();
       
    function supprimer(Route $route);
}
