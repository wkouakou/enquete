<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Services;

use AuthAcl\Model\Entity\Menu;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

/**
 *
 * @author wilfried
 */
interface MenuService extends ServiceLocatorAwareInterface {
    
    function editer(Menu $menu);
    
    function getById($menu);
    
    function lister();
    
    function supprimer(Menu $menu);
}
