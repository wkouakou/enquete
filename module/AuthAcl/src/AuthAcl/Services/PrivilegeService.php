<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Services;

use AuthAcl\Model\Entity\Privilege;
use AuthAcl\Model\Entity\Role;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

/**
 *
 * @author wilfried
 */
interface PrivilegeService extends ServiceLocatorAwareInterface {

    function editer(Privilege $privilege);

    function lister();

    function getPrivilegeByRole(Role $role);
    
    function getCheckedRessource();
    
    function getPrivilegeByRoleAndRessource($idRole, $idRessource);

    function supprimer(Privilege $privilege);
}
