<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Services;

use AuthAcl\Model\Entity\SousMenu;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

/**
 *
 * @author wilfried
 */
interface SousMenuService extends ServiceLocatorAwareInterface {
    
    function editer(SousMenu $sousMenu);
    
    function getById($sousMenu);
    
    function getByMenu($menu);
    
    function lister();
    
    function supprimer(SousMenu $sousMenu);
}
