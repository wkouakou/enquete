<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Services;

use AuthAcl\Model\Entity\Ressource;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

/**
 *
 * @author wilfried
 */
interface RessourceService extends ServiceLocatorAwareInterface {
    
    function editer(Ressource $ressource);
    
    function lister();
    
    function getDefaultRessources();
    
    function getRessourceById($ressource);
    
    function supprimer(Ressource $ressource);
}
