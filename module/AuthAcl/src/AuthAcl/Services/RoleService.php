<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Services;

use AuthAcl\Model\Entity\Role;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

/**
 *
 * @author wilfried
 */
interface RoleService extends ServiceLocatorAwareInterface {
    
    function editer(Role $role);
    
    function getRoleById($role);
    
    function getRoleByName($name);
    
    function initPrivilege($role);
    
    function lister();
    
    function supprimer(Role $role);
}
