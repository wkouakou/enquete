<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Services\Impl;

use AuthAcl\Model\Entity\Route;
use AuthAcl\Services\RouteService;
use Exception;

/**
 * Description of RouteServiceImpl
 *
 * @author wilfried
 */
class RouteServiceImpl implements RouteService {

    use \Zend\ServiceManager\ServiceLocatorAwareTrait;

    private function init() {
        $this->em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $this->repository = $this->em->getRepository('AuthAcl\Model\Entity\Route');
    }
//  
    public function editer(Route $route) {
        try {
            $this->em->persist($route);
            if ($this->getFlush() === TRUE) {
                $this->em->flush();
            }
            return $route;
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue lors de l'édition.");
        }
    }

    public function getDefaultRoutes() {
        try {
            return $this->repository->getDefaultRoutes();
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue, repository introuvable.");
        }
    }

    public function lister() {
        try {
            return $this->repository->findAll();
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue, repository introuvable.");
        }
    }

    public function supprimer(Route $route) {
        try {
            $this->em->remove($route);
            if ($this->getFlush() === TRUE) {
                $this->em->flush();
            }
            return $route;
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue lors de la suppression.");
        }
    }

    public function getModuleByRoute() {
        try {
            $this->init();
            
            return $this->repository->findModuleByRoute();
            
        } catch (Exception $exc) {
            
            throw new Exception("Aucun enregistrement trouvé.");
        }
    }
    
    public function getRoute() {
        try {
            $this->init();
            
            return $this->repository->findRoute();
            
        } catch (Exception $exc) {
            
            throw new Exception("Aucun enregistrement trouvé.");
        }
    }

    public function getFlush() {
        return $this->flush;
    }

    public function setFlush($flush) {
        $this->flush = $flush;
        return $this;
    }

}
