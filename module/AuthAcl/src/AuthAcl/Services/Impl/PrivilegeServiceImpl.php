<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Services\Impl;

use AuthAcl\Model\Entity\Privilege;
use AuthAcl\Model\Entity\Role;
use AuthAcl\Services\PrivilegeService;
use Exception;

/**
 * Description of PrivilegeServiceImpl
 *
 * @author wilfried
 */
class PrivilegeServiceImpl implements PrivilegeService {

    use \Zend\ServiceManager\ServiceLocatorAwareTrait;

    protected $em;
    protected $repository;
    protected $flush = true;

    public function init() {
        $this->em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $this->repository = $this->em->getRepository('AuthAcl\Model\Entity\Privilege');
    }

    public function editer(Privilege $privilege) {
        try {
            $this->init();
            $this->em->persist($privilege);
            if ($this->getFlush() === TRUE) {
                $this->em->flush();
            }
            return $privilege;
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue lors de l'édition.");
        }
    }

    public function getPrivilegeByRole(Role $role) {
        try {
            $this->init();
            return $this->repository->getPrivilegeByRole($role);
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue, repository introuvable.");
        }
    }

    public function getPrivilegeByRoleAndRessource($idRole, $idRessource) {
        try {
            $this->init();
            return $this->repository->findOneBy(array('role' => $idRole, 'ressource' => $idRessource));
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue, repository introuvable.");
        }
    }

    public function lister() {
        try {
            $this->init();
            return $this->repository->findAll();
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue, repository introuvable.");
        }
    }

    public function supprimer(Privilege $privilege) {
        try {
            $this->init();
            $this->em->remove($privilege);
            if ($this->getFlush() === TRUE) {
                $this->em->flush();
            }
            return $privilege;
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue lors de la suppression.");
        }
    }

    function getCheckedRessource() {
        try {
            
            $this->init();
            $Actives = $this->repository->findByActive(1);
            
           $listeActive = array();
           
            foreach ($Actives as $Active) {
                
                $listeActive[] = $Active->getRessource()->getId();
            }
            return $listeActive;
            
        } catch (Exception $exc) {
            
            throw new Exception("Erreur survenue, repository introuvable.");
            
        }
    }

    public function getFlush() {
        return $this->flush;
    }

    public function setFlush($flush) {
        $this->flush = $flush;
        return $this;
    }

}
