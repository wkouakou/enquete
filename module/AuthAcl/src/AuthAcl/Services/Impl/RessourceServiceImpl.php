<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Services\Impl;

use AuthAcl\Model\Entity\Ressource;
use AuthAcl\Services\RessourceService;
use Exception;

/**
 * Description of RessourceServiceImpl
 *
 * @author wilfried
 */
class RessourceServiceImpl implements RessourceService {
    use \Zend\ServiceManager\ServiceLocatorAwareTrait;
    
    protected $em;
    protected $repository;
    protected $flush = true;
    
    private function init() {
        $this->em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $this->repository = $this->em->getRepository('AuthAcl\Model\Entity\Ressource');
    }

    public function editer(Ressource $ressource) {
        try {
            $this->init();
            $this->em->persist($ressource);
            if ($this->getFlush() === TRUE) {
                $this->em->flush();
            }
            return $ressource;
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue lors de l'édition.");
        }
    }

    public function getDefaultRessources() {
        try {
            $this->init();
            return $this->repository->getDefaultRessources();
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue, repository introuvable.");
        }
    }

    public function lister() {
        try {
            $this->init();
            return $this->repository->findAll();
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue, repository introuvable.");
        }
    }
    
    public function getRessourceByRoute($idRoute) {

        try {
            $this->init();
            return $this->repository->findRessourceByRoute($idRoute);
        } catch (Exception $exc) {

            throw new Exception("Aucun enregistrement trouvÃ©.");
        }
    }

    public function supprimer(Ressource $ressource) {
        try {
            $this->init();
            $this->em->remove($ressource);
            if ($this->getFlush() === TRUE) {
                $this->em->flush();
            }
            return $ressource;
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue lors de la suppression.");
        }
    }

    public function getFlush() {
        return $this->flush;
    }

    public function setFlush($flush) {
        $this->flush = $flush;
        return $this;
    }

    public function getRessourceById($ressource) {
        try {
            $this->init();
            return $this->repository->find($ressource);
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue, repository introuvable.");
        }
    }
}
