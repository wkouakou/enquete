<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Services\Impl;

use AuthAcl\Model\Entity\Privilege;
use AuthAcl\Model\Entity\Ressource;
use AuthAcl\Model\Entity\Role;
use AuthAcl\Services\RoleService;
use DateTime;
use Exception;

/**
 *
 * @author wilfried
 */
class RoleServiceImpl implements RoleService {

    use \Zend\ServiceManager\ServiceLocatorAwareTrait;

    protected $em;
    protected $repository;
    protected $flush = true;

    private function init() {
        $this->em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $this->repository = $this->em->getRepository('AuthAcl\Model\Entity\Role');
    }

    public function editer(Role $role) {
        try {
            $this->init();
            $this->em->persist($role);
            if ($this->getFlush() === TRUE) {
                $this->em->flush();
            }
            return $role;
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue lors de l'édition.\n" . $exc->getMessage());
        }
    }

    public function lister($criteria = array(), $orderBy = null, $limit = null, $offset = null) {
        try {
            $this->init();
            return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue, repository introuvable.");
        }
    }

    public function supprimer(Role $role) {
        try {
            $this->init();
            $this->em->remove($role);
            if ($this->getFlush() === TRUE) {
                $this->em->flush();
            }
            return $role;
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue lors de la suppression.");
        }
    }

    public function getFlush() {
        $this->init();
        return $this->flush;
    }

    public function setFlush($flush) {
        $this->init();
        $this->flush = $flush;
        return $this;
    }

    public function getRoleById($role) {
        try {
            $this->init();
            return $this->repository->find($role);
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue, repository introuvable.");
        }
    }

    public function getRoleByName($name) {
        try {
            $this->init();
            return $this->repository->findOneByName($name);
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue, repository introuvable.");
        }
    }

    public function initPrivilege($role) {
        $ressourceService = $this->getServiceLocator()->get('ressource_service');
        $listeRessources = $ressourceService->lister();
        if (count($listeRessources) && $role) {
            $privilegeService = $this->getServiceLocator()->get('privilege_service');
            $dateOperation = new DateTime('now');
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
            $this->em->getConnection()->beginTransaction();
            $success = true;
            foreach ($listeRessources as $ressource) {
                try {
                    $privilege = new Privilege();
                    $privilege->setCreatedAt($dateOperation);
                    $privilege->setUpdatedAt($dateOperation);
                    $privilege->setRole($role);
                    $privilege->setRessource($ressource);
                    //Droit
                    if($ressource->getVisible()){
                        $privilege->setActive(true);
                    }
                    $privilegeService->editer($privilege);
                } catch (Exception $exc) {
                    $success = false;
                    break;
                }
            }
            if($success){
                $this->em->getConnection()->commit();
            }
            else{
                $this->em->getConnection()->rollback();
                return false;
            }
        }
        return true;
    }

}
