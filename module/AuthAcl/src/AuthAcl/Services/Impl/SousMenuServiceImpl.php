<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Services\Impl;

use AuthAcl\Model\Entity\SousMenu;
use AuthAcl\Services\SousMenuService;
use Exception;

/**
 *
 * @author wilfried
 */
class SousMenuServiceImpl implements SousMenuService {

    use \Zend\ServiceManager\ServiceLocatorAwareTrait;

    protected $em;
    protected $repository;
    protected $flush = true;

    private function init() {
        $this->em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $this->repository = $this->em->getRepository('AuthAcl\Model\Entity\SousMenu');
    }

    public function getById($sousMenu) {
        try {
            $this->init();
            return $this->repository->find($sousMenu);
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue, repository introuvable.");
        }
    }

    public function editer(SousMenu $sousMenu) {
        try {
            $this->init();
            $this->em->persist($sousMenu);
            if ($this->getFlush() === TRUE) {
                $this->em->flush();
            }
            return $sousMenu;
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue lors de l'édition.\n" . $exc->getMessage());
        }
    }

    public function getByMenu($menu) {
        try {
            $this->init();
            return $this->repository->findByMenu($menu);
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue, repository introuvable.");
        }
    }

    public function lister($criteria = array(), $orderBy = null, $limit = null, $offset = null) {
        try {
            $this->init();
            return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue, repository introuvable.\n".$exc->getMessage());
        }
    }

    public function supprimer(SousMenu $sousMenu) {
        try {
            $this->init();
            $this->em->remove($sousMenu);
            if ($this->getFlush() === TRUE) {
                $this->em->flush();
            }
            return $sousMenu;
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue lors de la suppression.");
        }
    }

    public function getFlush() {
        $this->init();
        return $this->flush;
    }

    public function setFlush($flush) {
        $this->init();
        $this->flush = $flush;
        return $this;
    }

}
