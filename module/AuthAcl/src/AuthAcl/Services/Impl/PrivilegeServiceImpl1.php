<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Services\Impl;

use AuthAcl\Model\Entity\Privilege;
use AuthAcl\Model\Entity\Role;
use AuthAcl\Model\Entity\Route;
use AuthAcl\Services\PrivilegeService;
use Exception;

/**
 * Description of PrivilegeServiceImpl
 *
 * @author wilfried
 */
class PrivilegeServiceImpl implements PrivilegeService {

    use \Zend\ServiceManager\ServiceLocatorAwareTrait;

    protected $em;
    protected $repository;
    protected $flush = true;

    public function init() {
        $this->em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $this->repository = $this->em->getRepository('AuthAcl\Model\Entity\Privilege');
    }

    public function initRessource() {
        $this->em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $this->repository = $this->em->getRepository('AuthAcl\Model\Entity\Ressource');
    }

    public function initModule() {
        $this->em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $this->repository = $this->em->getRepository('AuthAcl\Model\Entity\Route');
    }

    public function editer(Privilege $privilege) {
        try {
            $this->init();
            $this->em->persist($privilege);
            if ($this->getFlush() === TRUE) {
                $this->em->flush();
            }
            return $privilege;
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue lors de l'édition.");
        }
    }

    public function getPrivilegeByRole(Role $role) {
        try {
            $this->init();
            return $this->repository->getPrivilegeByRole($role);
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue, repository introuvable.");
        }
    }

    public function getRessouceByIdRole($idRole = NULL) {
        // exit($idRole); exit;
        $this->init();
        if ($idRole) {
            $tabPrivilege = array();
            $rowsPrivileges = $this->repository->findBy(array('role' => $idRole));

            foreach ($rowsPrivileges as $privilege) {
                if ($privilege->getActive() == 1) {
                    $tabPrivilege[] = $privilege->getRole()->getId() . "_" . $privilege->getRessource()->getId();
                }
            }
            return $tabPrivilege;
        } else {

            $this->initRessource();
            
            $this->initModule();

            $Modules = $this->repository->findAll();
            
            print_r($Modules);exit;

            $rowModule = $this->repository->findAll();

            $tabModule = array();

            foreach ($Modules as $Module) {
                foreach ($rowModule as $libModule) {

                    $tabModule[$Module->getId()] = array("id" => $libModule->getId(),
                        "module" => $libModule->getRoute()->getModule(),
                        "controller" => $libModule->getRoute()->getController(),
                        "action" => $libModule->getAction());
                }
            }
            return $tabModule;
        }
    }

    public function lister() {
        try {
            $this->init();
            return $this->repository->findAll();
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue, repository introuvable.");
        }
    }

    public function supprimer(Privilege $privilege) {
        try {
            $this->init();
            $this->em->remove($privilege);
            if ($this->getFlush() === TRUE) {
                $this->em->flush();
            }
            return $privilege;
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue lors de la suppression.");
        }
    }

    public function getFlush() {
        return $this->flush;
    }

    public function setFlush($flush) {
        $this->flush = $flush;
        return $this;
    }

}
