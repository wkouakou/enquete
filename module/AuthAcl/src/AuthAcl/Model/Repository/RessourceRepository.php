<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Model\Repository;

/**
 * Description of RessourceRepository
 *
 * @author Wilfried
 */
use Doctrine\ORM\EntityRepository;
use Exception;

class RessourceRepository extends EntityRepository {

    public function getDefaultRessources() {
        try {
            return $this->findByIsDefault(TRUE);
        } catch (Exception $exc) {
            throw new Exception("Erreur interne (repository).");
        }
    }

    public function findRessourceByRoute($idRoute) {
        $qb = $this->createQueryBuilder('r')
                ->distinct('r.route')
                ->where('r.route = ?1');
                
        $qb->setParameter(1, $idRoute);
        
        $query = $qb->getQuery();
        return $query->execute();
    }

}
