<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Model\Repository;

/**
 * Description of RessourceRepository
 *
 * @author Wilfried
 */
use Doctrine\ORM\EntityRepository;
use Exception;

class RouteRepository extends EntityRepository {

    public function getDefaultRoute() {
        try {
            return $this->findByIsDefault(TRUE);
        } catch (Exception $exc) {
            throw new Exception("Erreur interne (repository).");
        }
    }

    public function findModuleByRoute() {
        $query = $this->createQueryBuilder('r')
                ->groupBy('r.module')
                ->getQuery();
        return $query->execute();
    }
    
    public function findRoute() {
        $query = $this->createQueryBuilder('r')
                ->distinct('name')
                ->getQuery();
        return $query->execute();
    }
}
