<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Model\Repository;

use Doctrine\ORM\Query\Expr\Join;
/**
 * Description of PrivilegeRepository
 *
 * @author Wilfried
 */
use Doctrine\ORM\EntityRepository;
use Exception;

class PrivilegeRepository extends EntityRepository {

    public function getPrivilegeByRole($role) {
        try {
            return $this->findByRole($role);
        } catch (Exception $exc) {
            throw new Exception("Erreur interne (repository).");
        }
    }

   

}
