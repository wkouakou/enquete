<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Model\Entity;

/**
 * Description of Route
 *
 * @author Wilfried
 */

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AuthAcl\Model\Repository\RouteRepository")
 * @ORM\Table(name="auth_acl_route")
 */
class Route extends BaseEntity{
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
    */
    private $id;
    
    /** @ORM\Column(type="string", length=64, unique=true) */
    private $name;
    
    /** @ORM\Column(type="string", length=32) */
    private $module;
    
    /** @ORM\Column(type="string", length=32) */
    private $controller;
    
    /** @ORM\Column(type="boolean") */
    private $proteger = true;
    
    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getModule() {
        return $this->module;
    }

    public function getController() {
        return $this->controller;
    }

    public function getProteger() {
        return $this->proteger;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function setModule($module) {
        $this->module = $module;
        return $this;
    }

    public function setController($controller) {
        $this->controller = $controller;
        return $this;
    }

    public function setProteger($proteger) {
        $this->proteger = $proteger;
        return $this;
    }
}
