<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Model\Entity;

/**
 * Description of Menu
 *
 * @author Wilfried
 */

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="auth_acl_menu")
 */
class Menu extends BaseEntity{
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
    */
    private $id;
    
    /** @ORM\Column(type="string", length=64, unique=true) */
    private $name;
    
    /** @ORM\Column(type="string", length=255)
     * @JMS\SerializedName("classCss") */
    private $classCss;
    
    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getClassCss() {
        return $this->classCss;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function setClassCss($classCss) {
        $this->classCss = $classCss;
        return $this;
    }
}
