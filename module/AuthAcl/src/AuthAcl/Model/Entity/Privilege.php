<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Model\Entity;

/**
 * Description of Privilege
 *
 * @author Wilfried
 */

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AuthAcl\Model\Repository\PrivilegeRepository")
 * @ORM\Table(name="auth_acl_privilege")
 */
class Privilege extends BaseEntity {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Role")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $role;

    /**
     * @ORM\ManyToOne(targetEntity="Ressource")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $ressource;

    /** @ORM\Column(type="boolean") */
    private $active = false;

    public function getId() {
        return $this->id;
    }

    public function getRole() {
        return $this->role;
    }

    public function getRessource() {
        return $this->ressource;
    }

    public function getActive() {
        return $this->active;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setRole($role) {
        $this->role = $role;
        return $this;
    }

    public function setRessource($ressource) {
        $this->ressource = $ressource;
        return $this;
    }

    public function setActive($active) {
        $this->active = $active;
        return $this;
    }
}
