<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Model\Entity;

/**
 * Description of User
 *
 * @author wilfried
 */
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Zend\Crypt\Password\Bcrypt;

/**
 * @ORM\MappedSuperclass
 */
class User {

    /**
     * @JMS\SerializedName("createdAt")
     * @JMS\Type("DateTime<'d-m-Y H:i:s'>") 
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /** @ORM\Column(type="string", length=128)
     * @JMS\SerializedName("fullName") */
    private $fullName;

    /** @ORM\Column(type="string", length=16, unique=true) */
    private $contact;

    /** @ORM\Column(type="string", length=96, unique=true) */
    private $email;

    /** @ORM\Column(type="string", length=255)
     * @JMS\Exclude
     */
    protected $password;

    /** @ORM\Column(type="smallint")
     * @JMS\Exclude
     */
    private $loginAttempts = 0;

    /**
     * @ORM\Column(type="datetime", nullable=true) 
     * @JMS\Exclude
     */
    private $loginAttemptsTime;

    /** @ORM\Column(type="boolean") */
    private $status = true;

    /**
     * @ORM\Column(type="datetime", nullable=true) 
     * @JMS\SerializedName("lastSignedIn")
     * @JMS\Type("DateTime<'d-m-Y H:i:s'>")
     * 
     */
    private $lastSignedIn;

    public function getCreatedAt() {
        return $this->createdAt;
    }

    public function getFullName() {
        return $this->fullName;
    }

    public function getContact() {
        return $this->contact;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getLoginAttempts() {
        return $this->loginAttempts;
    }

    public function getLoginAttemptsTime() {
        return $this->loginAttemptsTime;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getLastSignedIn() {
        return $this->lastSignedIn;
    }

    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function setFullName($fullName) {
        $this->fullName = $fullName;
        return $this;
    }

    public function setContact($contact) {
        $this->contact = $contact;
        return $this;
    }

    public function setEmail($email) {
        $this->email = $email;
        return $this;
    }

    public function setPassword($password) {
        $bcript = new Bcrypt();
        $this->password = $bcript->create($password);
        return $this;
    }

    public function setLoginAttempts($loginAttempts) {
        $this->loginAttempts = $loginAttempts;
        return $this;
    }

    public function setLoginAttemptsTime($loginAttemptsTime) {
        $this->loginAttemptsTime = $loginAttemptsTime;
        return $this;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    public function setLastSignedIn($lastSignedIn) {
        $this->lastSignedIn = $lastSignedIn;
        return $this;
    }

    /**
     * Password Generator
     */
    public static function generatePassword($length = 6) {
        $ranges = array(range('a', 'z'), range('A', 'Z'), range(1, 9));
        $code = '';
        for ($i = 0; $i < $length; $i++) {
            $rkey = array_rand($ranges);
            $vkey = array_rand($ranges[$rkey]);
            $code .= $ranges[$rkey][$vkey];
        }
        return $code;
    }

}
