<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Model\Entity;

/**
 * Description of Ressource
 *
 * @author Wilfried
 */

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AuthAcl\Model\Repository\RessourceRepository")
 * @ORM\Table(name="auth_acl_ressource")
 */
class Ressource extends BaseEntity{
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
    */
    private $id;
    
    /** @ORM\Column(type="string", length=32) */
    private $action;
    
    /** @ORM\Column(type="string", length=32) */
    private $label;
    
    /** @ORM\Column(type="string", length=255) */
    private $classCss;
    
    /** @ORM\Column(type="boolean") */
    private $visible = false;
    
    /**
     * @ORM\ManyToOne(targetEntity="Route")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $route;
    
    public function getId() {
        return $this->id;
    }

    public function getAction() {
        return $this->action;
    }

    public function getLabel() {
        return $this->label;
    }

    public function getClassCss() {
        return $this->classCss;
    }

    public function getVisible() {
        return $this->visible;
    }

    public function getRoute() {
        return $this->route;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setAction($action) {
        $this->action = $action;
        return $this;
    }

    public function setLabel($label) {
        $this->label = $label;
        return $this;
    }

    public function setClassCss($classCss) {
        $this->classCss = $classCss;
        return $this;
    }

    public function setVisible($visible) {
        $this->visible = $visible;
        return $this;
    }

    public function setRoute($route) {
        $this->route = $route;
        return $this;
    }
}
