<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Model\Entity;

/**
 * Description of BaseEntity
 *
 * @author Wilfried
 */

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\MappedSuperclass
 */
class BaseEntity {
    /** 
     * @ORM\Column(type="datetime") 
     * @JMS\SerializedName("dateCreation")
     * @JMS\Type("DateTime<'d-m-Y H:i:s'>")
     * 
     */
    private $createdAt;

    /** 
     * @ORM\Column(type="datetime") 
     * @JMS\SerializedName("dateModification")
     * @JMS\Type("DateTime<'d-m-Y H:i:s'>") 
     */
    private $updatedAt;
    
    public function getCreatedAt() {
        return $this->createdAt;
    }

    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;
        return $this;
    }


}
