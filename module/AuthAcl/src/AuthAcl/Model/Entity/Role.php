<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Model\Entity;

/**
 * Description of Role
 *
 * @author Wilfried
 */

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AuthAcl\Model\Repository\RoleRepository")
 * @ORM\Table(name="auth_acl_role")
 */
class Role extends BaseEntity {
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="smallint", length=2)
    */
    private $id;
    
    /** @ORM\Column(type="string", length=30, unique=true) */
    private $name;
    
    /** @ORM\Column(type="boolean") */
    private $isDefault = false;
    
    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getIsDefault() {
        return $this->isDefault;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function setIsDefault($isDefault) {
        $this->isDefault = $isDefault;
        return $this;
    }
}
