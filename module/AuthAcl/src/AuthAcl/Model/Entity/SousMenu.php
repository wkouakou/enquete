<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Model\Entity;

/**
 * Description of SousMenu
 *
 * @author Wilfried
 */

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AuthAcl\Model\Repository\SousMenuRepository")
 * @ORM\Table(name="auth_acl_sous_menu")
 */
class SousMenu extends BaseEntity{
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
    */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Menu")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $menu;
    
    /**
     * @ORM\ManyToOne(targetEntity="Ressource")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $ressource;
    
    public function getId() {
        return $this->id;
    }

    public function getMenu() {
        return $this->menu;
    }

    public function getRessource() {
        return $this->ressource;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setMenu($menu) {
        $this->menu = $menu;
        return $this;
    }

    public function setRessource($ressource) {
        $this->ressource = $ressource;
        return $this;
    }
}
