<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AuthAcl\Model;

use Zend\Authentication\Storage\Session;

/**
 * Description of AitAuthStorage
 *
 * @author wilfried
 */
class AitAuthStorage extends Session {
    public function setRememberMe($rememberMe = 0, $time = 1209600)
    {
         if ($rememberMe == 1) {
             $this->session->getManager()->rememberMe($time);
         }
    }
     
    public function forgetMe()
    {
        $this->session->getManager()->forgetMe();
    }
    
    public function getCompte() {
        return $this->session->offsetGet("compte");
    }
    
    public function setCompte(\Application\Model\Entity\Compte $compte) {
        $this->session->offsetSet("compte", $compte);
    }
    
    public function getKeyFacture() {
        return $this->session->offsetGet("uniqueFacture");
    }
    
    public function setKeyFacture($uniqueFacture) {
        $this->session->offsetSet("uniqueFacture", $uniqueFacture);
    }
    
    public function getKeyReglement() {
        return $this->session->offsetGet("uniqueReglement");
    }
    
    public function setKeyReglement($uniqueReglement) {
        $this->session->offsetSet("uniqueReglement", $uniqueReglement);
    }
}
