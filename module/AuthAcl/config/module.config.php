<?php

namespace AuthAcl;

return array(
    'router' => array(
        'routes' => array(
            'auth' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/auth',
                    'defaults' => array(
                        'controller' => 'AuthAcl\Controller\Index',
                        'action' => 'login',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:action[/:id]]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                    'forgot-password' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/forgot-password[/:id]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'action' => 'forgot-password',
                            ),
                        ),
                    ),
                ),
            ),
            'menu' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/menu',
                    'defaults' => array(
                        'controller' => 'AuthAcl\Controller\Menu',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:action[/:id]]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                    'sous-menu' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/sous-menu[/:id]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'action' => 'sous-menu',
                            ),
                        ),
                    ),
                ),
            ),
            'role' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/role',
                    'defaults' => array(
                        'controller' => 'AuthAcl\Controller\Role',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:action[/:id]]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                    'droit' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/droit[/:id]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'action' => 'droit',
                            ),
                        ),
                    ),
                    'add-privilege' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/add-privilege[/:id]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'action' => 'add-privilege',
                            ),
                        ),
                    ),
                ),
            ),
            'profile' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/profile',
                    'defaults' => array(
                        'controller' => 'AuthAcl\Controller\Role',
                        'action' => 'profile',
                    ),
                ),
            ),
            'ping' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/ping',
                    'defaults' => array(
                        'controller' => 'AuthAcl\Controller\Index',
                        'action' => 'ping',
                    ),
                ),
            ),
            'access' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/access-denied',
                    'defaults' => array(
                        'controller' => 'AuthAcl\Controller\Index',
                        'action' => 'privilege',
                    ),
                ),
            ),
            'inactive' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/inactive',
                    'defaults' => array(
                        'controller' => 'AuthAcl\Controller\Index',
                        'action' => 'inactive',
                    ),
                ),
            ),
        ),
    ),
    'whitelist' => array(
        'auth',
        'ping',
//        'auth/default',
        'access',
        'inactive',
        'auth/logout',
        'auth/forgot-password'
    ),
    'service_manager' => array(
        'abstract_factories' => array(),
        'invokables' => array(
            'role_service' => 'AuthAcl\Services\Impl\RoleServiceImpl',
            'ressource_service' => 'AuthAcl\Services\Impl\RessourceServiceImpl',
            'route_service' => 'AuthAcl\Services\Impl\RouteServiceImpl',
            'privilege_service' => 'AuthAcl\Services\Impl\PrivilegeServiceImpl',
            'menu_service' => 'AuthAcl\Services\Impl\MenuServiceImpl',
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'AuthAcl\Controller\Index' => 'AuthAcl\Controller\IndexController',
            'AuthAcl\Controller\Role' => 'AuthAcl\Controller\RoleController',
            'AuthAcl\Controller\Menu' => 'AuthAcl\Controller\MenuController',
        ),
    ),
    'controller_plugins' => array(
        'invokables' => array(
        ),
        'factories' => array(
            'IcAclPlugin' => 'AuthAcl\Factory\IcAclPluginFactory',
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'authacl' => __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Model/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Model\Entity' => __NAMESPACE__ . '_driver'
                )
            )
        ),
    ),
);
