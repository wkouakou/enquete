<?php

namespace AuthAcl;

use AuthAcl\Model\AitAuthStorage;
use Zend\Authentication\AuthenticationService;

class Module {

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'AitAclPlugin' => 'AuthAcl\Factory\AitAclPluginFactory',
                'Zend\Authentication\AuthenticationService' => function($serviceManager) {
                    return $serviceManager->get('doctrine.authenticationservice.orm_default');
                },
                'AitAuthStorage' => function($sm) {
                    return new AitAuthStorage("fut_pl2017");
                }
            )
        );
    }

}
