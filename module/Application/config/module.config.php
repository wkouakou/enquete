<?php

namespace Application;

use Application\Model\Entity\Utilisateur;
use Zend\Crypt\Password\Bcrypt;

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'index',
                    ),
                ),
            ),
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'app' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/app',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'utilisateur' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/utilisateur[/:action][/:id]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'utilisateur',
                                'action' => 'index',
                            ),
                        )
                    ),
                    'groupe' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/groupe[/:action][/:id]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'groupe',
                                'action' => 'index',
                            ),
                        )
                    ),
                    'participant' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/participant[/:action][/:id]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'participant',
                                'action' => 'index',
                            ),
                        )
                    ),
                    'proposition' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/proposition[/:action][/:id]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'proposition',
                                'action' => 'index',
                            ),
                        )
                    ),
                    'question' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/question[/:action][/:id]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'question',
                                'action' => 'index',
                            ),
                        )
                    ),
                    'reponse' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/reponse[/:action][/:id]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'reponse',
                                'action' => 'index',
                            ),
                        )
                    ),
                    'sondage' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/sondage[/:action][/:id]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'sondage',
                                'action' => 'index',
                            ),
                        )
                    ),
                    'type-question' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/type-question[/:action][/:id]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'typequestion',
                                'action' => 'index',
                            ),
                        )
                    ),
                    'type-proposition' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/type-proposition[/:action][/:id]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'typeproposition',
                                'action' => 'index',
                            ),
                        )
                    ),
                    'statistique' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/statistique[/:action][/:id]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'statistique',
                                'action' => 'index',
                            ),
                        )
                    ),
                    'participer' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/participer[/:action][/:id]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'participer',
                                'action' => 'index',
                            ),
                        )
                    ),
                    'questionnaire' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/questionnaire[/:action][/:id]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'questionnaire',
                                'action' => 'index',
                            ),
                        )
                    ),
                ),
            ),
            'missing' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/missing[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'utilisateur',
                        'action' => 'missing',
                    ),
                )
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'factories' => array(
            'translator' => 'Zend\Mvc\Service\TranslatorServiceFactory',
        ),
        'invokables' => array(
            'groupe_service' => Services\Impl\GroupeServiceImpl::class,
            'groupe_utilisateur_service' => Services\Impl\GroupeUtilisateurServiceImpl::class,
            'participant_service' => Services\Impl\ParticipantServiceImpl::class,
            'participer_service' => Services\Impl\ParticiperServiceImpl::class,
            'proposition_service' => Services\Impl\PropositionServiceImpl::class,
            'entete_service' => Services\Impl\EnteteServiceImpl::class,
            'question_service' => Services\Impl\QuestionServiceImpl::class,
            'sondage_service' => Services\Impl\SondageServiceImpl::class,
            'type_question_service' => Services\Impl\TypeQuestionServiceImpl::class,
            'type_proposition_service' => Services\Impl\TypePropositionServiceImpl::class,
            'utilisateur_service' => Services\Impl\UtilisateurServiceImpl::class,
            'reponse_service' => Services\Impl\ReponseServiceImpl::class,
        )
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => Controller\IndexController::class,
            'Application\Controller\Groupe' => Controller\GroupeController::class,
            'Application\Controller\Participant' => Controller\ParticipantController::class,
            'Application\Controller\Participer' => Controller\ParticiperController::class,
            'Application\Controller\Proposition' => Controller\PropositionController::class,
            'Application\Controller\Question' => Controller\QuestionController::class,
            'Application\Controller\Reponse' => Controller\ReponseController::class,
            'Application\Controller\Sondage' => Controller\SondageController::class,
            'Application\Controller\TypeQuestion' => Controller\TypeQuestionController::class,
            'Application\Controller\TypeProposition' => Controller\TypePropositionController::class,
            'Application\Controller\Statistique' => Controller\StatistiqueController::class,
            'Application\Controller\Questionnaire' => Controller\QuestionnaireController::class,
            'Application\Controller\Utilisateur' => Controller\UtilisateurController::class,
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Model/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Model\Entity' => __NAMESPACE__ . '_driver'
                )
            )
        ),
        'authentication' => array(
            'orm_default' => array(
                'object_manager' => 'Doctrine\ORM\EntityManager',
                'identity_class' => 'Application\Model\Entity\Utilisateur',
                'identity_property' => 'email',
                'credential_property' => 'password',
                'credential_callable' => function(Utilisateur $utilisateur, $passwordGiven) {
                    $bcrypt = new Bcrypt();
                    return $bcrypt->verify($passwordGiven, $utilisateur->getPassword());
                },
            ),
        ),
        'eventmanager' => array(
            'orm_default' => array(
                'subscribers' => array(
                    'Gedmo\SoftDeleteable\SoftDeleteableListener',
                    'Gedmo\Timestampable\TimestampableListener',
                // 'Gedmo\Translatable\TranslatableListener',
                // 'Gedmo\Blameable\BlameableListener',
                // 'Gedmo\Loggable\LoggableListener',
                // 'Gedmo\Sluggable\SluggableListener',
                // 'Gedmo\Sortable\SortableListener',
                // 'Gedmo\Tree\TreeListener',
                ),
            ),
        )
    ),
    'mailman' => array(
        'MailMan\Smtp' => array(
            'default_sender' => 'info@ait-ci.com',
            'transport' => array(
                'type' => 'smtp',
                'options' => array(
                    'host' => 'mail.ait-ci.com',
                    'port' => '25',
                    'connection_class' => 'plain',
                    'connection_config' => array(
                        //'ssl' => 'tls',
                        'username' => 'info@ait-ci.com',
                        'password' => 'jhh',
                    ),
                )
            )
        )
    )
);
