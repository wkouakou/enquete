<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Utils;

/**
 * Description of UploadFile
 *
 * @author wilfried
 */
class UploadFile {

    public $relativePath = "docs/projets/";
    public $maxSize = 3000000;
    protected $extension;
    protected $extensions;

    /**
     * 
     * @param array $ext Accept type 
     * Default array('txt', 'png', 'jpg', 'jpeg', 'doc', 'docx', 'pdf')
     */
    public function __construct($ext = array('txt', 'png', 'jpg', 'jpeg', 'doc', 'docx', 'pdf')) {
        $this->extensions = array('.txt', '.png', '.jpg', '.jpeg', '.doc', '.docx', '.pdf');
        $this->extension = new \Zend\Validator\File\Extension(array('extension' => $ext));
    }

    public function upload($files, $fileToUpload, $prefixe) {
        try {
            $ext = pathinfo($files[$fileToUpload]['name'], PATHINFO_EXTENSION);
            $httpadapter = new \Zend\File\Transfer\Adapter\Http();
            $filesize = new \Zend\Validator\File\Size(array('min' => 1000)); //1KB  
            $filter = new \Zend\Filter\File\Rename(array(
                "target" => PUBLIC_PATH . '/' . $this->relativePath . $prefixe . '.' . $ext,
                "randomize" => true,
                'overwrite' => true
            ));

            $httpadapter->setValidators(array($filesize, $this->extension), $files[$fileToUpload]['name']);
            $httpadapter->setFilters(array($filter), $files[$fileToUpload]['name']);
            if ($httpadapter->isValid()) {
                $httpadapter->setDestination(PUBLIC_PATH . '/' . $this->relativePath);
                if ($httpadapter->receive()) {
                    return $httpadapter->getFileInfo()[$fileToUpload]["name"];
                }
            }
        } catch (\Exception $exc) {
//            ob_start();
//            echo $exc->getMessage();
//            $cont = ob_get_clean();
//            file_put_contents("uploadError.txt", $cont);
        }

        return false;
    }

    public function uploadSingle($files, $prefixe) {
        try {
            $ext = pathinfo($files['name'], PATHINFO_EXTENSION);
            $httpadapter = new \Zend\File\Transfer\Adapter\Http();
            $filesize = new \Zend\Validator\File\Size(array('min' => 1000)); //1KB  
            $filter = new \Zend\Filter\File\Rename(array(
                "target" => PUBLIC_PATH . '/' . $this->relativePath . $prefixe . '.' . $ext,
                "randomize" => true,
            ));

            $httpadapter->setValidators(array($filesize, $this->extension), $files['name']);
            $httpadapter->setFilters(array($filter), $files['name']);
            if ($httpadapter->isValid()) {
                $httpadapter->setDestination(PUBLIC_PATH . '/' . $this->relativePath);
                if ($httpadapter->receive($files['name'])) {
                    return $httpadapter->getFileInfo();
                }
            }
        } catch (\Exception $exc) {
            ob_start();
            echo $exc->getMessage();
            $cont = ob_get_clean();
            file_put_contents("uploadError.txt", $cont);
        }

        return false;
    }

    public function native($file, $prefixe = 'doc') {
        try {
            $taille = filesize($file['tmp_name']);
            $extension = strrchr($file['name'], '.');
            if (!in_array($extension, $this->extensions) || $taille > $this->maxSize) {
                return false;
            }
            $fileName = $prefixe . '_' . uniqid() . $extension;
            if (@move_uploaded_file($file['tmp_name'], PUBLIC_PATH . '/' . $this->relativePath . $fileName)) {
                $fileInfo = new FileInfo();
                $fileInfo->setName($fileName);
                $fileInfo->setSize($taille);
                $fileInfo->setExtension($extension);
                $fileInfo->setLien($this->relativePath . $fileName);
                return $fileInfo;
            }
            return false;
        } catch (\Exception $exc) {
//            ob_start();
//            echo $exc->getMessage();
//            $cont = ob_get_clean();
//            file_put_contents("uploadError.txt", $cont);
        }

        return false;
    }

}
