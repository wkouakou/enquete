<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Utils;

use DateTime;
use Exception;

/**
 * Description of Formatter
 *
 * @author wilfried
 */
class Formatter {
    public static function createDateFromFormat($stringDate, $format = 'd-m-Y') {
        $mDate = null;
        try {
            $mDate = DateTime::createFromFormat($format, str_replace("/", "-", $stringDate));
        } catch (Exception $exc) {
            return null;
        }
        return $mDate === false ? NULL : $mDate;
    }
}
