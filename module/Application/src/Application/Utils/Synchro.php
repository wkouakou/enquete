<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Utils;

use Zend\Http\Client;
use Zend\Http\Request;

/**
 * Description of Synchro
 *
 * @author wilfried
 */
class Synchro {

    public function getData($url, $arrayData = array("server" => "gestimmoServer")) {
        $request = new Request();
        $request->setUri($url);
        $request->setMethod('POST');
        $request->getPost()->fromArray($arrayData);
        
        $client = new Client($url);
        $headers = $client->getRequest()->getHeaders();
        $headers->addHeaderLine('Host', 'localhost.ittech');
        $headers->addHeaderLine('X-Powered-By', 'ittech');
        $client->setEncType(Client::ENC_FORMDATA);
        $reponse = $client->dispatch($request);
        if ($reponse->isSuccess()) {
            return $reponse->getBody();
        } else {
            return array();
        }
    }

    public function sendData($url, $arrayData = array(), $rawBody = null) {
        $client = new Client($url);
        $client->setParameterPost($arrayData);
        if ($rawBody) {
            $client->setRawBody($rawBody);
        }
        $headers = $client->getRequest()->getHeaders();
        $headers->addHeaderLine('Host', 'localhost.ittech');
        $headers->addHeaderLine('X-Powered-By', 'ittech');
        //$headers->addHeaderLine('Content-Type', 'application/json; charset=UTF-8');
        $client->setMethod('POST');
        $reponse = $client->send();
        if ($reponse->isSuccess()) {
            ob_start();
            //$reponse->getContent();
            print_r($reponse->getBody());
            $cont = ob_get_clean();
            file_put_contents("PostResponse.txt", $cont);
        } else {
            ob_start();
            echo "Erreur survenue";
            print("\n" . $reponse->getBody());
            $cont = ob_get_clean();
            file_put_contents("PostResponseError.txt", $cont);
        }
    }

    public function sendRequest($url, $data = array()) {

        $request = new Request();
        $request->setUri($url);
        $request->setMethod('POST');
        $request->getPost()->fromArray($data);
        //$request->getPost()->fromString($string);

        $client = new Client();
        $client->setEncType(Client::ENC_FORMDATA);
        $response = $client->dispatch($request);

        if ($response->isSuccess()) {
            ob_start();
            //$reponse->getContent();
            print_r($reponse->getBody());
            $cont = ob_get_clean();
            file_put_contents("PostResponse.txt", $cont);
        } else {
            ob_start();
            echo "Erreur survenue";
            print("\n" . $reponse->getBody());
            $cont = ob_get_clean();
            file_put_contents("PostResponseError.txt", $cont);
        }
    }

}
