<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Utils;

use Application\Model\Entity\Utilisateur;

/**
 * Description of Util
 *
 * @author wilfried
 */
class Util {

    /**
     * 
     * @param float $taux
     * @param integer $montant
     * @return interger
     */
    public static function getMontantTaux($taux, $montant) {
        return ceil(($montant * $taux ) / 100);
    }

    public static function getTaux($montant, $montantTaux) {
        return ceil(($montantTaux * 100 ) / $montant);
    }

    public static function isAdmin($identity) {
        return strtolower($identity->getRole()->getName()) == strtolower(Utilisateur::ACTEUR_ADMIN) ? true : false;
    }

    public static function isAssurance($identity) {
        return strtolower($identity->getRole()->getName()) == strtolower(Utilisateur::ACTEUR) && $identity->getAgentAssurance() !== null ? true : false;
    }

    public static function isGendarme($identity) {
        return strtolower($identity->getRole()->getName()) == strtolower(Utilisateur::ACTEUR) && $identity->getGendarme() !== null ? true : false;
    }

    public static function isPolicier($identity) {
        return strtolower($identity->getRole()->getName()) == strtolower(Utilisateur::ACTEUR) && $identity->getPolicier() !== null ? true : false;
    }

    public static function isPompeFunebre($identity) {
        return strtolower($identity->getRole()->getName()) == strtolower(Utilisateur::ACTEUR) && $identity->getAgentPompeFunebre() !== null ? true : false;
    }

    public static function isPompier($identity) {
        return strtolower($identity->getRole()->getName()) == strtolower(Utilisateur::ACTEUR) && $identity->getPompier() !== null ? true : false;
    }

    public static function isMedecin($identity) {
        return strtolower($identity->getRole()->getName()) == strtolower(Utilisateur::ACTEUR) && $identity->getMedecin() !== null ? true : false;
    }

    public static function isSamu($identity) {
        return strtolower($identity->getRole()->getName()) == strtolower(Utilisateur::ACTEUR) && $identity->getSamu() !== null ? true : false;
    }

    public static function getRoleName($identity) {
        if (strtolower($identity->getRole()->getName()) == strtolower(Utilisateur::ACTEUR) && $identity->getAgentAssurance() !== null) {
            return "Assurance";
        } elseif (strtolower($identity->getRole()->getName()) == strtolower(Utilisateur::ACTEUR) && $identity->getGendarme() !== null) {
            return "Gendarme";
        } elseif (strtolower($identity->getRole()->getName()) == strtolower(Utilisateur::ACTEUR) && $identity->getPolicier() !== null) {
            return "Policier";
        } elseif (strtolower($identity->getRole()->getName()) == strtolower(Utilisateur::ACTEUR) && $identity->getAgentPompeFunebre() !== null) {
            return "Pompe funèbre";
        } elseif (strtolower($identity->getRole()->getName()) == strtolower(Utilisateur::ACTEUR) && $identity->getPompier() !== null) {
            return "Pompier";
        } elseif (strtolower($identity->getRole()->getName()) == strtolower(Utilisateur::ACTEUR) && $identity->getMedecin() !== null) {
            return "Medecin";
        } elseif (strtolower($identity->getRole()->getName()) == strtolower(Utilisateur::ACTEUR) && $identity->getSamu() !== null) {
            return "Samu";
        }
        else{
            return "";
        }
    }

}
