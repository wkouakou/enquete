<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Utils;

use Exception;
use Zend\ServiceManager\ServiceManager;

/**
 * Description of ServiceLocatorFactory
 *
 * @author wilfried
 */
class ServiceLocatorFactory {

    /**
     * @var ServiceManager
     */
    private static $serviceManager = null;

    /**
     * Disable constructor
     */
    private function __construct() {
        
    }

    /**
     * @throw Exception
     * @return ServiceManager
     */
    public static function getInstance() {
        if (null === self::$serviceManager) {
            throw new Exception('ServiceLocator is not set');
        }
        return self::$serviceManager;
    }

    /**
     * @param ServiceManager
     */
    public static function setInstance(ServiceManager $sm) {
        self::$serviceManager = $sm;
    }

}
