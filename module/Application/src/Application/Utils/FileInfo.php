<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Utils;

/**
 * Description of FileInfo
 *
 * @author wilfried
 */
class FileInfo {

    private $size = 0;
    private $name = '';
    private $lien = '';
    private $extension = '';

    public function getSize() {
        return $this->size;
    }

    public function getName() {
        return $this->name;
    }

    public function getLien() {
        return $this->lien;
    }

    public function getExtension() {
        return $this->extension;
    }

    public function setSize($size) {
        $this->size = $size;
        return $this;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function setLien($lien) {
        $this->lien = $lien;
        return $this;
    }

    public function setExtension($extension) {
        $this->extension = $extension;
        return $this;
    }
}
