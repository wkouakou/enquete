<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 10 avr. 2017 A  04:59:18
  ##############  File     : ParticipantController.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Controller;

use Application\Model\Entity\Participant;
use DateTime;
use Exception;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class ParticipantController extends AbstractActionController {

    private $serializer;
    private $user = null;

    private function init() {
        $this->serializer = $this->getServiceLocator()->get('jms_serializer.serializer');
        $this->user = $this->identity();
    }

    public function indexAction() {
        $this->layout()->setVariable("btnModalAjout", true);
        $this->layout()->setVariable("menuPrincipal", "Participant");
        $this->layout()->setVariable("icone", "fa fa-delicious");
        $this->layout()->setVariable("lienPrincipal", "app/participant");
        $this->layout()->setVariable("menuInterne", "participant");
        $this->layout()->setVariable("titleControlleur", "Participant");
        //$this->layout()->setVariable("parametreActive", "active");
        $this->layout()->setVariable("participantActive", "active");

        $viewData = array();
        return new ViewModel($viewData);
    }

    public function listerAction() {
        $jsonData = array();
        $this->init();
        $participantService = $this->getServiceLocator()->get('participant_service');

        $limit = $this->params()->fromQuery('limit', 10);
        $offset = $this->params()->fromQuery('offset', 0);
        $search = $this->params()->fromQuery('search', null);
        //$sondage = $this->params()->fromQuery('sondage', null);
        $criteres = array('deletedAt' => NULL);
//        if (isset($sondage) && is_numeric($sondage) && $sondage > 0)
//            $criteres["sondage"] = $sondage;

        if ($search) {
            $criteres["question"] = "%" . $search . "%";
            $listeParticipants = $participantService->getByNameLike($criteres, array("quest.fullName" => "ASC"), $limit, $offset);
            $countData = $participantService->getByNameLike($criteres);
        }
        else {
            $listeParticipants = $participantService->lister($criteres, array('fullName' => 'ASC'), $limit, $offset);
            $countData = $participantService->lister($criteres);
        }

        //$listeParticipants = $participantService->lister(array('deletedAt' => NULL), array('libelle' => 'asc'), $limit, $offset);

        $tabData = array();
        foreach ($listeParticipants as $participant) {
            $tabData[] = json_decode($this->serializer->serialize($participant, 'json'));
        }
        $countData = $participantService->lister(array('deletedAt' => NULL));
        $jsonData["rows"] = $tabData;
        $jsonData["total"] = count($countData);
        return new JsonModel($jsonData);
    }

    public function ajouterAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["fullName"])) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
            $this->em->getConnection()->beginTransaction();
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                $participantService = $this->getServiceLocator()->get('participant_service');
                $roleService = $this->getServiceLocator()->get('role_service');
                $utilisateurService = $this->getServiceLocator()->get('utilisateur_service');

                $participant = new Participant();
                $participant->setCreatedAt($dateOperation);
                $participant->setUpdatedAt($dateOperation);
                $participant->setContact($data["contact"]);
                $participant->setEmail($data["email"]);
                $participant->setSexe($data["sexe"]);
                $participant->setFullName($data["fullName"]);
                $participant->setEtat(TRUE);
                $participant->setCreatedBy($this->user);
                $dataParticipant = $participantService->editer($participant);

//                if ($dataParticipant) {
//                    $utilisateur = new \Application\Model\Entity\Utilisateur();
//                    $utilisateur->setCreatedAt($dateOperation);
//                    $utilisateur->setFullName($data["fullName"]);
//                    $utilisateur->setEmail($data["email"]);
//                    $utilisateur->setAndroidId($data["androidId"]);
//                    $utilisateur->setContact($data["contact"]);
//                    $utilisateur->setCivilite($data["sexe"]);
//                    $utilisateur->setPassword($data["password"]);
//                    $utilisateur->setStatus(true);
//                    $utilisateur->setParticipant($dataParticipant);
//                    if (isset($data["role"]) && $data["role"]) {
//                        $role = $roleService->getRoleById($data["role"]);
//                    }
//                    else {
//                        $role = $roleService->getRoleByName("Participant");
//                        if (!$role) {
//                            $role = new \AuthAcl\Model\Entity\Role();
//                            $role->setCreatedAt($dateOperation);
//                            $role->setIsDefault(1);
//                            $role->setUpdatedAt($dateOperation);
//                            $role->setName("Participant");
//                            $roleService->editer($role);
//                        }
//                    }
//
//                    $utilisateur->setRole($role);
//                    $utilisateur->setUpdatedAt($dateOperation);
//                    $utilisateur->setCreatedBy($this->user ? $this->user : NULL);
//                    $dataUtilisateur = $utilisateurService->editer($utilisateur);
//                }

                $jsonData["data"] = json_decode($this->serializer->serialize($dataParticipant, 'json'));
                $this->em->getConnection()->commit();
                return new JsonModel($jsonData);
            }
            catch (Exception $exc) {
                $this->em->getConnection()->rollback();
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function modifierAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        try {
            $this->init();
            $dateOperation = new DateTime('now');
            $data = $request->getPost();

            if ($data["utilisateurID"]) {
                $utilisateurService = $this->getServiceLocator()->get('utilisateur_service');
                $utilisateur = $utilisateurService->getById($data["utilisateurID"]);
                if (!$utilisateur) {
                    throw new Exception("Utilisateur introuvable");
                }
                else {
                    $idParticipant = $utilisateur->getParticipant()->getId();
                }
            }

            $participantService = $this->getServiceLocator()->get('participant_service');
            $participant = $participantService->getById($data["idParticipant"] ? $data["idParticipant"] : $idParticipant);
            if (!$participant) {
                throw new Exception("Impossible d'effectuer cette opération.\nVeuillez réessayer plus tard.");
            }
            $participant->setCreatedAt($dateOperation);
            $participant->setUpdatedAt($dateOperation);
            $participant->setContact($data["contact"]);
            $participant->setEmail($data["email"]);
            $participant->setFullName($data["fullName"]);
            $participant->setSexe($data["sexe"]);
            if (!$data["utilisateurID"]) {
                if (isset($data["etat"]))
                    $participant->setEtat(TRUE);
                else
                    $participant->setEtat(FALSE);
            }
            $dataParticipant = $participantService->editer($participant);

//            if ($dataParticipant && $data["utilisateurID"]) {
//                $utilisateur->setFullName($data["fullName"]);
//                $utilisateur->setEmail($data["email"]);
//                $utilisateur->setAndroidId($data["androidId"]);
//                $utilisateur->setContact($data["contact"]);
//                $utilisateur->setCivilite($data["sexe"]);
//                $dataUtilisateur = $utilisateurService->editer($utilisateur);
//            }

//            $jsonData["data"] = json_decode($this->serializer->serialize($data["utilisateurID"] ? $dataUtilisateur : $dataParticipant, 'json'));
            $jsonData["data"] = json_decode($this->serializer->serialize($dataParticipant, 'json'));
            return new JsonModel($jsonData);
        }
        catch (Exception $exc) {
            $jsonData["code"] = -1;
            $jsonData["data"] = NULL;
            $jsonData["msg"] = $exc->getMessage();
            return new JsonModel($jsonData);
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function supprimerAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["idParticipant"])) {
            try {
                $this->init();
                $data = $request->getPost();
                $participantService = $this->getServiceLocator()->get('participant_service');
                $participant = $participantService->getById($data["idParticipant"]);
                if (!$participant) {
                    throw new Exception("Impossible d'effectuer cette opération.\nVeuillez réessayer plus tard.");
                }

                $dataParticipant = $participantService->supprimer($participant);
                $jsonData["data"] = json_decode($this->serializer->serialize($dataParticipant, 'json'));

                return new JsonModel($jsonData);
            }
            catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

}
