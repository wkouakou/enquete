<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 2 mai 2017 A  06:15:57
  ##############  File     : QuestionnaireController.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Controller;

use Application\Model\Entity\Participant;
use Application\Model\Entity\Proposition;
use Application\Model\Entity\Reponse;
use DateTime;
use Exception;
use mPDF;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class QuestionnaireController extends AbstractActionController {

    private $serializer;
    private $user = null;

    private function init() {
        $this->serializer = $this->getServiceLocator()->get('jms_serializer.serializer');
        $this->user = $this->identity();
    }

    public function indexAction() {
        $this->layout()->setVariable("btnModalAjout", FALSE);
        $this->layout()->setVariable("menuPrincipal", "Questionnaie");
        $this->layout()->setVariable("icone", "fa fa-qrcode");
        $this->layout()->setVariable("lienPrincipal", "app/questionnaire");
        $this->layout()->setVariable("menuInterne", "questionnaire");
        $this->layout()->setVariable("titleControlleur", "Questionnaire - choix du groupe");
        //$this->layout()->setVariable("parametreActive", "active");
        $this->layout()->setVariable("questionnaireActive", "active");

        $viewData = array();

        $sondageService = $this->getServiceLocator()->get('sondage_service');
        $groupeService = $this->getServiceLocator()->get('groupe_service');
        $listeGroupes = $groupeService->lister(array('deletedAt' => NULL), array('libelle' => 'ASC'), NULL, NULL);
        $listeTab = [];
        $nbSondage = 0;
        foreach ($listeGroupes as $groupe) {
            $nbSondage = count($sondageService->lister(["groupe" => $groupe->getId(), "deletedAt" => NULL]));
            $groupe->setSondages($nbSondage);
            $listeTab[] = $groupe;
        }
        $viewData["listeGroupes"] = $listeTab;

        return new ViewModel($viewData);
    }

    public function sondageAction() {
        $this->layout()->setVariable("btnModalAjout", FALSE);
        $this->layout()->setVariable("menuPrincipal", "Questionnaie");
        $this->layout()->setVariable("icone", "fa fa-qrcode");
        $this->layout()->setVariable("lienPrincipal", "app/questionnaire");
        $this->layout()->setVariable("menuInterne", "questionnaire");
        $this->layout()->setVariable("titleControlleur", "Questionnaire - choix de l'enquete");
        //$this->layout()->setVariable("parametreActive", "active");
        $this->layout()->setVariable("questionnaireActive", "active");

        $this->init();
        $modelData = array();
        $idGroupe = $this->params()->fromRoute('id');
        try {
            if ($idGroupe > 0) {
                //groupe
                $groupeService = $this->getServiceLocator()->get('groupe_service');
                $groupe = $groupeService->getById($idGroupe);
                if (!$groupe) {
                    throw new Exception("Impossible d'effectuer cette opération.\n groupe introuvable.");
                }
                $modelData["groupe"] = $groupe;

                $sondageService = $this->getServiceLocator()->get('sondage_service');
                $sondages = $sondageService->lister(["groupe" => $groupe->getId(), "deletedAt" => NULL]);
                $modelData["sondages"] = $sondages;
            }
        } catch (Exception $exc) {

            $modelData["erreur"] = $exc->getMessage();
            $modelData["data"] = null;
            $modelData["code"] = 0;
        }

        return new ViewModel($modelData);
    }

    public function evaluerAction() {
        $this->layout()->setVariable("btnModalAjout", FALSE);
        $this->layout()->setVariable("menuPrincipal", "Questionnaie");
        $this->layout()->setVariable("icone", "fa fa-qrcode");
        $this->layout()->setVariable("lienPrincipal", "app/questionnaire");
        $this->layout()->setVariable("menuInterne", "questionnaire");
        $this->layout()->setVariable("titleControlleur", "Questionnaire - evaluation");
        //$this->layout()->setVariable("parametreActive", "active");
        $this->layout()->setVariable("questionnaireActive", "active");


        $this->init();
        $modelData = array();
        $idSondage = $this->params()->fromRoute('id');
        try {
            if ($idSondage > 0) {
                //groupe
                $sondageService = $this->getServiceLocator()->get('sondage_service');
                $sondage = $sondageService->getById($idSondage);
                if (!$sondage) {
                    $modelData["code"] = 0;
                    $modelData["msg"] = "Impossible d'effectuer cette opération.\n enquete introuvable.";
                    return new ViewModel($modelData);
                }
                $modelData["code"] = 1;
                $modelData["sondage"] = $sondage;

                $questionService = $this->getServiceLocator()->get('question_service');
                $questions = $questionService->lister(["sondage" => $sondage->getId(), "deletedAt" => NULL]);
                $modelData["questions"] = $questions;

                //Essayer de faire manyToOne
                $propositionService = $this->getServiceLocator()->get('proposition_service');
                $listesProposition = [];
                foreach ($questions as $question) {
                    $listesProposition[$question->getId()] = $propositionService->lister(["question" => $question->getId(), "deletedAt" => NULL]);
                }
                //print_r($listesProposition);exit;
                $modelData["propositions"] = $listesProposition;
            } else {
                return $this->redirect()->toRoute('app');
            }
        } catch (Exception $exc) {

            $modelData["erreur"] = $exc->getMessage();
            $modelData["data"] = null;
            $modelData["code"] = 0;
        }

        return new ViewModel($modelData);
    }

    public function ajouterAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["idSondage"])) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
            $this->em->getConnection()->beginTransaction();
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                $aParticiper = false; //Si le participant a réellement participer, éviter le cumul par participant Anonyme

                $sondageService = $this->getServiceLocator()->get('sondage_service');
                $sondage = $sondageService->getById($data["idSondage"]);
                if (!$sondage) {
                    $jsonData["code"] = 0;
                    $jsonData["msg"] = "Impossible d'effectuer cette opération.\n enquete introuvable.";
                    return new JsonModel($jsonData);
                }

                //Enregistrement du participant
                $participantService = $this->getServiceLocator()->get('participant_service');
                $email = $data["email"];
                $contact = $data["contact"];
                $dataParticipant = $email ? $participantService->getByEmail($email) : NULL;
                if (!$dataParticipant) {
                    $dataParticipant = $contact ? $participantService->getByContact($contact) : NULL;
                    if (!$dataParticipant) { //Creation du participant si inexistant
                        $sexe = $data["sexe"];
                        $nom = $data["fullName"] ? $data["fullName"] : 'Anonyme';
                        $participant = new Participant();
                        $participant->setCreatedAt($dateOperation);
                        $participant->setUpdatedAt($dateOperation);
                        $participant->setContact($contact);
                        $participant->setEmail($email);
                        $participant->setSexe($sexe);
                        $participant->setFullName($nom);
                        $participant->setCreatedBy($this->user);
                        $dataParticipant = $participantService->editer($participant);
                    }
                }

                $propositionService = $this->getServiceLocator()->get('proposition_service');
                $reponseService = $this->getServiceLocator()->get('reponse_service');
                $questionService = $this->getServiceLocator()->get('question_service');

                //Liste des propositions de reponse
                //questionDate
                $tabQuestionDate = isset($data['questionDate']) ? $data['questionDate'] : [];
                foreach ($tabQuestionDate as $idQuestion => $valeurReponse) {
                    $question = $questionService->getById($idQuestion);
                    $dataProposition = $propositionService->getByCritere(["question" => $idQuestion, "deletedAt" => NULL]);
                    if (!$dataProposition) {
                        $proposition = new Proposition();
                        $proposition->setLibelle($question->getLibelle());
                        $proposition->setQuestion($question);
                        $proposition->setCreatedBy($this->user);
                        $proposition->setCreatedAt($dateOperation);
                        $proposition->setUpdatedAt($dateOperation);
                        $dataProposition = $propositionService->editer($proposition);
                    }

                    $reponse = new Reponse();
                    $reponse->setLibre($valeurReponse);
                    $reponse->setParticipant($dataParticipant);
                    $reponse->setProposition($dataProposition);
                    $reponse->setCreatedAt($dateOperation);
                    $reponse->setUpdatedAt($dateOperation);
                    $reponse->setCreatedBy($this->user);
                    $dataReponse = $reponseService->editer($reponse);
                    $aParticiper = true;
                }

                //questionText
                $tabQuestionText = isset($data['questionText']) ? $data['questionText'] : [];
                foreach ($tabQuestionText as $idQuestion => $valeurReponse) {
                    $question = $questionService->getById($idQuestion);
                    $dataProposition = $propositionService->getByCritere(["question" => $idQuestion, "deletedAt" => NULL]);
                    if (!$dataProposition) {
                        $proposition = new Proposition();
                        $proposition->setLibelle($question->getLibelle());
                        $proposition->setQuestion($question);
                        $proposition->setCreatedBy($this->user);
                        $proposition->setCreatedAt($dateOperation);
                        $proposition->setUpdatedAt($dateOperation);
                        $dataProposition = $propositionService->editer($proposition);
                    }

                    $reponse = new Reponse();
                    $reponse->setLibre($valeurReponse);
                    $reponse->setParticipant($dataParticipant);
                    $reponse->setProposition($dataProposition);
                    $reponse->setCreatedAt($dateOperation);
                    $reponse->setUpdatedAt($dateOperation);
                    $reponse->setCreatedBy($this->user);
                    $dataReponse = $reponseService->editer($reponse);
                    $aParticiper = true;
                }

                //questionRadio
                $tabQuestionRadio = isset($data['questionRadio']) ? $data['questionRadio'] : [];
                foreach ($tabQuestionRadio as $idQuestion => $idProposition) {
                    $dataProposition = $propositionService->getById($idProposition);
                    if (!$dataProposition) {
                        continue;
                    }

                    $reponse = new Reponse();
                    //$reponse->setLibre($valeurReponse);
                    $reponse->setParticipant($dataParticipant);
                    $reponse->setProposition($dataProposition);
                    $reponse->setCreatedAt($dateOperation);
                    $reponse->setUpdatedAt($dateOperation);
                    $reponse->setCreatedBy($this->user);
                    $dataReponse = $reponseService->editer($reponse);
                    $aParticiper = true;
                }

                //questionCheckbox
                $tabQuestionCheckbox = isset($data['questionCheckbox']) ? $data['questionCheckbox'] : [];
                foreach ($tabQuestionCheckbox as $idProposition) { //$idProposition => $idProposition
                    $dataProposition = $propositionService->getById($idProposition);
                    if (!$dataProposition) {
                        continue;
                    }

                    $reponse = new Reponse();
                    //$reponse->setLibre($valeurReponse);
                    $reponse->setParticipant($dataParticipant);
                    $reponse->setProposition($dataProposition);
                    $reponse->setCreatedAt($dateOperation);
                    $reponse->setUpdatedAt($dateOperation);
                    $reponse->setCreatedBy($this->user);
                    $dataReponse = $reponseService->editer($reponse);
                    $aParticiper = true;
                }

                $tabQuestionMatriceSimple = isset($data['questionMatriceSimple']) ? $data['questionMatriceSimple'] : [];
                foreach ($tabQuestionMatriceSimple as $idProposition => $idPropositionEntete) { //$idProposition => $idProposition (Entete de matrice)
                    $dataProposition = $propositionService->getById($idProposition);
                    $dataColonne = $propositionService->getById($idPropositionEntete);
                    if (!$dataProposition) {
                        continue;
                    }

                    $reponse = new Reponse();
                    //$reponse->setLibre($valeurReponse);
                    $reponse->setParticipant($dataParticipant);
                    $reponse->setProposition($dataProposition);
                    $reponse->setColonne($dataColonne);
                    $reponse->setCreatedAt($dateOperation);
                    $reponse->setUpdatedAt($dateOperation);
                    $reponse->setCreatedBy($this->user);
                    $dataReponse = $reponseService->editer($reponse);
                    $aParticiper = true;
                }

                //questionEvaluationGraduee
                $tabQuestionEvaluationGraduee = isset($data['questionEvaluationGraduee']) ? $data['questionEvaluationGraduee'] : [];
                foreach ($tabQuestionEvaluationGraduee as $idProposition => $valeurReponse) {
                    $dataProposition = $propositionService->getById($idProposition);
                    if (!$dataProposition) {
                        continue;
                    }
                    $tabReponse = explode(',', $valeurReponse);
                    if (count($tabReponse) > 1) {
                        //Valeur min
                        $reponse = new Reponse();
                        $reponse->setLibre($tabReponse[0]);
                        $reponse->setParticipant($dataParticipant);
                        $reponse->setProposition($dataProposition);
                        $reponse->setCreatedAt($dateOperation);
                        $reponse->setUpdatedAt($dateOperation);
                        $reponse->setCreatedBy($this->user);
                        $dataReponse = $reponseService->editer($reponse);

                        //Valeur max
                        $reponse = new Reponse();
                        $reponse->setLibre($tabReponse[1]);
                        $reponse->setParticipant($dataParticipant);
                        $reponse->setProposition($dataProposition);
                        $reponse->setCreatedAt($dateOperation);
                        $reponse->setUpdatedAt($dateOperation);
                        $reponse->setCreatedBy($this->user);
                        $reponseService->editer($reponse);
                        $aParticiper = true;
                    } else {
                        $reponse = new Reponse();
                        $reponse->setLibre($valeurReponse);
                        $reponse->setParticipant($dataParticipant);
                        $reponse->setProposition($dataProposition);
                        $reponse->setCreatedAt($dateOperation);
                        $reponse->setUpdatedAt($dateOperation);
                        $reponse->setCreatedBy($this->user);
                        $reponseService->editer($reponse);
                        $aParticiper = true;
                    }
                }

                //Matrice
                $matQuestionMatrice = isset($data['questionMatrice']) ? $data['questionMatrice'] : []; //[idProposition][ordre]
                foreach ($matQuestionMatrice as $idProposition => $listeReponse) {
                    $dataProposition = $propositionService->getById($idProposition);
                    if (!$dataProposition || !count($listeReponse)) {
                        continue;
                    }
                    foreach ($listeReponse as $idPropositionEntete => $valeurReponse) {

                        $dataColonne = $propositionService->getById($idPropositionEntete);
                        if (!$dataColonne) {
                            continue;
                        }
                        $reponse = new Reponse();
                        $reponse->setLibre($valeurReponse);
                        $reponse->setParticipant($dataParticipant);
                        $reponse->setProposition($dataProposition);
                        $reponse->setColonne($dataColonne);
                        $reponse->setCreatedAt($dateOperation);
                        $reponse->setUpdatedAt($dateOperation);
                        $reponse->setCreatedBy($this->user);
                        $dataReponse = $reponseService->editer($reponse);
                        $aParticiper = true;
                    }
                }

                $matQuestionHierarchie = isset($data['questionHierarchie']) ? $data['questionHierarchie'] : []; //[idProposition][ordre]
                foreach ($matQuestionHierarchie as $idProposition => $listeReponse) {
                    $dataProposition = $propositionService->getById($idProposition);
                    if (!$dataProposition || !count($listeReponse)) {
                        continue;
                    }
                    foreach ($listeReponse as $index => $valeurReponse) {
                        if (!$dataColonne) {
                            continue;
                        }
                        $reponse = new Reponse();
                        $reponse->setLibre($index++);
                        $reponse->setParticipant($dataParticipant);
                        $reponse->setProposition($dataProposition);
                        $reponse->setCreatedAt($dateOperation);
                        $reponse->setUpdatedAt($dateOperation);
                        $reponse->setCreatedBy($this->user);
                        $dataReponse = $reponseService->editer($reponse);
                        $aParticiper = true;
                    }
                }
                if($aParticiper){
                    $sondage->setNombreParticipant($sondage->getNombreParticipant()+1);
                    $sondageService->editer($sondage);
                    
                    //Verifier si le participant est lié au sondage, sinon inserer
                    $participerService = $this->getServiceLocator()->get('participer_service');
                    $participer = $participerService->getOneByCriteria(["sondage" => $sondage, "participant" => $dataParticipant]);
                    if(!$participer){
                        $participer = new \Application\Model\Entity\Participer();
                        $participer->setSondage($sondage);
                        $participer->setParticipant($dataParticipant);
                        $participer->setEtat(TRUE);
                        $participerService->editer($participer);
                    }
                }
                $jsonData["data"] = json_decode($this->serializer->serialize($sondage, 'json'));
                $this->em->getConnection()->commit();
                return new JsonModel($jsonData);
            } catch (Exception $exc) {
                $this->em->getConnection()->rollback();
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function ajoutermobileAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["participant"])) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
            $this->em->getConnection()->beginTransaction();
            $aParticiper = false;
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                $participant = $this->serializer->deserialize($data["participant"], Participant::class, 'json');

                if (!$participant) {
                    throw new Exception("Participant introuvable");
                }
                $participant->setId(NULL);
                //Enregistrement du participant
                $participantService = $this->getServiceLocator()->get('participant_service');
                $propositionService = $this->getServiceLocator()->get('proposition_service');
                $questionService = $this->getServiceLocator()->get('question_service');
                $reponseService = $this->getServiceLocator()->get('reponse_service');

                $email = $participant->getEmail();
                $contact = $participant->getContact();
                $dataParticipant = $email ? $participantService->getByEmail($email) : NULL;
                if (!$dataParticipant) {
                    $dataParticipant = $contact ? $participantService->getByContact($contact) : NULL;
                }
                if (!$dataParticipant) {
                    $dataParticipant = $participantService->editer($participant);
                }
                $dataReponse = NULL;
                foreach ($participant->getReponses() as $reponse) {
                    $reponse->setId(NULL);
                    $reponse->setParticipant($dataParticipant);
                    $reponse->setCreatedAt($dateOperation);
                    $reponse->setUpdatedAt($dateOperation);
                    $reponse->setCreatedBy($this->user);
                    $proposition = $reponse->getProposition();
                    $reponse->setProposition(NULL);
                    $idProposition = $proposition->getId();
                    switch ($proposition->getQuestion()->getTypeQuestion()->getType()) {
                        //Reponse sans proposition
                        case "date":
                        case "textarea":
                        case "text":
                        case "input":
                            $dataProposition = $propositionService->getByCritere(["question" => $proposition->getQuestion()->getId(), "deletedAt" => NULL]);
                            
                            if (!$dataProposition) {
                                //Creation de la proposition de reponse
                                $proposition->setId(NULL);
                                $proposition->setQuestion($questionService->getById($proposition->getQuestion()->getId()));
                                $proposition->setCreatedBy($this->user);
                                $proposition->setCreatedAt($dateOperation);
                                $proposition->setUpdatedAt($dateOperation);
                                $dataProposition = $propositionService->editer($proposition);
                            }
                            $idProposition = $dataProposition->getId();

                            break;
                        default: //checkbox, radio, illustrationradio, imagecheckbox, imageradio, matrice, hierachie, evaluationgraduee
                            //Enregistrer la reponse à la sortie du switch
                            break;
                    }
                    //Eviter les persist en cascade
                    $dataProposition = $propositionService->getById($idProposition);
                    $reponse->setProposition($dataProposition);
                    
                    if($reponse->getColonne() !== NULL){
                        $idColonne = $reponse->getColonne()->getId();
                        $reponse->setColonne($propositionService->getById($idColonne));
                    }
                    //Enregistrement de la reponse
                    $dataReponse = $reponseService->editer($reponse);
                    $aParticiper = true;
                }
                $sondage = $dataReponse->getProposition()->getQuestion()->getSondage();
                if($aParticiper){
                    $sondageService = $this->getServiceLocator()->get('sondage_service');
                    $sondage->setNombreParticipant($sondage->getNombreParticipant()+1);
                    $sondageService->editer($sondage);
                    
                    //Verifier si le participant est lié au sondage, sinon inserer
                    $participerService = $this->getServiceLocator()->get('participer_service');
                    $participer = $participerService->getOneByCriteria(["sondage" => $sondage, "participant" => $dataParticipant]);
                    if(!$participer){
                        $participer = new \Application\Model\Entity\Participer();
                        $participer->setSondage($sondage);
                        $participer->setParticipant($dataParticipant);
                        $participer->setEtat(TRUE);
                        $participerService->editer($participer);
                    }
                }
                $jsonData["data"] = json_decode($this->serializer->serialize($sondage, 'json'));
                $this->em->getConnection()->commit();
                return new JsonModel($jsonData);
            } catch (Exception $exc) {
                $this->em->getConnection()->rollback();
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function imprimerAction() {
        $this->init();
        $modelData = array();
        $idSondage = $this->params()->fromRoute('id');
        $sondage = NULL;
        if ($idSondage > 0) {
            //groupe
            $sondageService = $this->getServiceLocator()->get('sondage_service');
            $sondage = $sondageService->getById($idSondage);
            if (!$sondage) {
                
            }

            $questionService = $this->getServiceLocator()->get('question_service');
            $questions = $questionService->lister(["sondage" => $sondage->getId(), "deletedAt" => NULL]);

            //Essayer de faire manyToOne
            $propositionService = $this->getServiceLocator()->get('proposition_service');
            $listesProposition = [];
            foreach ($questions as $question) {
                $listesProposition[$question->getId()] = $propositionService->lister(["question" => $question->getId(), "deletedAt" => NULL]);
            }
            $propositions = $listesProposition;
            $url = $this->request->getBasePath();
        } else {
            return $this->redirect()->toRoute('app');
        }

        $viewModelData = new ViewModel();
        $viewModelData->setTerminal(true);

        $enteteAvecLibelle = '';

        ob_start();
        require_once(dirname(__FILE__) . '/../../../view/application/questionnaire/imprimer.phtml');
        $content = ob_get_clean();

        try {
            $enteteAvecLibelle = '<p style="text-align:center;" class="label label-default">' . $sondage->getLibelle() . '</p>';
            $piedAvecNumero = '<p style="text-align:right;">{PAGENO}/{nbpg}</p>';
            $mpdf = new mPDF('ar', '', '', 32, 15, 15, 15, 10, 10);
            //$mpdf->setHeader($h);
            $mpdf->SetHTMLHeader($enteteAvecLibelle);
            $mpdf->SetHTMLFooter($piedAvecNumero);

            $mpdf->SetFont('ar_k_001');
            $mpdf->AddPage('P');
            $mpdf->SetDisplayMode('fullpage');
            // LOAD a stylesheet
            $stylesheet = file_get_contents(PUBLIC_PATH . '/css/custom-print-bootstrap.min.css');
            $mpdf->WriteHTML($stylesheet, 1); // The parameter 1 tells that this is css/style only and no body/html/text

            $mpdf->WriteHTML($content);
            $mpdf->Output('Fiche de questionnaire.pdf', 'I');
        } catch (Exception $e) {
            echo $e;
            exit;
        }
    }

}
