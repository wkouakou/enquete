<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 10 avr. 2017 A  05:00:40
  ##############  File     : SondageController.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Controller;

use Application\Model\Entity\Question;
use Application\Model\Entity\Sondage;
use DateTime;
use Exception;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class SondageController extends AbstractActionController {

    private $serializer;
    private $user = null;

    private function init() {
        $this->serializer = $this->getServiceLocator()->get('jms_serializer.serializer');
        $this->user = $this->identity();
    }

    public function indexAction() {
        $this->layout()->setVariable("btnModalAjout", TRUE);
        $this->layout()->setVariable("menuPrincipal", "Enquete");
        $this->layout()->setVariable("icone", "fa fa-search");
        $this->layout()->setVariable("lienPrincipal", "app/sondage");
        $this->layout()->setVariable("menuInterne", "sondage");
        $this->layout()->setVariable("titleControlleur", "Sondage");
        //$this->layout()->setVariable("parametreActive", "active");
        $this->layout()->setVariable("sondageActive", "active");

        $viewData = array();
        $this->init();
        $typeQuestionService = $this->getServiceLocator()->get('type_question_service');
        $viewData["typeQuestions"] = $typeQuestionService->lister(array('deletedAt' => NULL), array('libelle' => 'ASC'));

        $groupeService = $this->getServiceLocator()->get('groupe_service');
        $groupes = $groupeService->lister(array('deletedAt' => NULL));
        $viewData['listeGroupes'] = $groupes;

        return new ViewModel($viewData);
    }

    public function listerAction() {
        $jsonData = array();
        $this->init();
        $sondageService = $this->getServiceLocator()->get('sondage_service');

        $limit = $this->params()->fromQuery('limit', 10);
        $offset = $this->params()->fromQuery('offset', 0);
        $search = $this->params()->fromQuery('search', null);
        $groupe = $this->params()->fromQuery('groupe', null);
        $criteres = array('deletedAt' => NULL);
        if (isset($groupe) && is_numeric($groupe) && $groupe > 0)
            $criteres["groupe"] = $groupe;

        if ($search) {
            $criteres["question"] = "%" . $search . "%";
            $listeSondages = $sondageService->getByLibelleLike($criteres, array("quest.libelle" => "ASC"), $limit, $offset);
            $countData = $sondageService->getByLibelleLike($criteres);
        }
        else {
            $listeSondages = $sondageService->lister($criteres, array('createdAt' => 'DESC'), $limit, $offset);
            $countData = $sondageService->lister($criteres);
        }

        $participerService = $this->getServiceLocator()->get('participer_service');
        $tabData = array();
        foreach ($listeSondages as $sondage) {
            $nbParticipant = count($participerService->lister(array('deletedAt' => NULL, 'sondage' => $sondage->getId())));
            $sondage->setParticipants($nbParticipant);
            $tabData[] = json_decode($this->serializer->serialize($sondage, 'json'));
        }
        $countData = $sondageService->lister(array('deletedAt' => NULL));
        $jsonData["rows"] = $tabData;
        $jsonData["total"] = count($countData);
        return new JsonModel($jsonData);
    }

    public function ajouterAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["libelle"])) {
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                $sondageService = $this->getServiceLocator()->get('sondage_service');
                $groupeService = $this->getServiceLocator()->get('groupe_service');
                $groupe = $groupeService->getById($data["groupe"]);
                if (!$groupe) {
                    throw new Exception("Impossible d'effectuer cette opération.\nGroupe introuvable.");
                }

                //Retrouver un libelle
                $sondage = $sondageService->getByLibelleAndGroupe($data["libelle"], $data["groupe"]);
                if ($sondage) {
                    $sondage->setDeletedAt(NULL);
                    $sondage->setUpdatedAt($dateOperation);
                }
                else {
                    $sondage = new Sondage();
                    $sondage->setCreatedAt($dateOperation);
                    $sondage->setLibelle($data["libelle"]);
                    $sondage->setCreatedBy($this->user);
                    $sondage->setGroupe($groupe);
                }

                $sondage->setUpdatedAt($dateOperation);
                if (isset($data["etat"]))
                    $sondage->setEtat(TRUE);
                else
                    $sondage->setEtat(FALSE);

                $dataSondage = $sondageService->editer($sondage);
                $jsonData["data"] = json_decode($this->serializer->serialize($dataSondage, 'json'));
                return new JsonModel($jsonData);
            }
            catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function modifierAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        try {
            $this->init();
            $dateOperation = new DateTime('now');
            $data = $request->getPost();
            $sondageService = $this->getServiceLocator()->get('sondage_service');
            $sondage = $sondageService->getById($data["idSondage"]);
            if (!$sondage) {
                throw new Exception("Impossible d'effectuer cette opération.\nEnquete introuvable.");
            }

            $groupeService = $this->getServiceLocator()->get('groupe_service');
            $groupe = $groupeService->getById($data["groupe"]);
            if (!$groupe) {
                throw new Exception("Impossible d'effectuer cette opération.\nGroupe introuvable.");
            }

            //Retrouver un libelle
            $sondage2 = $sondageService->getByLibelleAndGroupe($data["libelle"], $data["groupe"]);
            if ($sondage2 && $sondage->getId() != $sondage2->getId() && $sondage2->getDeletedAt() == NULL) {
                throw new Exception("Cette enquete existe deja.\nVeuillez changer de libelle.");
            }
            elseif ($sondage2 && $sondage->getId() != $sondage2->getId() && $sondage2->getDeletedAt()) {
                throw new Exception("Cette enquete a ete supprimee.\nVeuillez recreer a nouveau.");
            }
            else {
                $sondage->setUpdatedAt($dateOperation);
                $sondage->setLibelle($data["libelle"]);
                $sondage->setGroupe($groupe);
                if (isset($data["etat"]))
                    $sondage->setEtat(TRUE);
                else
                    $sondage->setEtat(FALSE);
                $sondage->setCreatedBy($this->user);
            }

            $dataSondage = $sondageService->editer($sondage);
            $jsonData["data"] = json_decode($this->serializer->serialize($dataSondage, 'json'));
            return new JsonModel($jsonData);
        }
        catch (Exception $exc) {
            $jsonData["code"] = -1;
            $jsonData["data"] = NULL;
            $jsonData["msg"] = $exc->getMessage();
            return new JsonModel($jsonData);
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function supprimerAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["idSondage"])) {
            try {
                $this->init();
                $data = $request->getPost();
                $sondageService = $this->getServiceLocator()->get('sondage_service');
                $sondage = $sondageService->getById($data["idSondage"]);
                if (!$sondage) {
                    throw new Exception("Impossible d'effectuer cette opération.\nVeuillez réessayer plus tard.");
                }

                $dataSondage = $sondageService->supprimer($sondage);
                $jsonData["data"] = json_decode($this->serializer->serialize($dataSondage, 'json'));

                return new JsonModel($jsonData);
            }
            catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    //A supprimer
    public function questionsAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 0, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["idSondage"])) {
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                $questionService = $this->getServiceLocator()->get('question_service');
                $sondageService = $this->getServiceLocator()->get('sondage_service');
                $typeQuestionService = $this->getServiceLocator()->get('type_question_service');

                $sondage = $sondageService->getById($data["idSondage"]);
                if (!$sondage) {
                    throw new Exception("Impossible d'effectuer cette opération.\n enquete introuvable.");
                }
                $tabLibelleQuestion = $data["libelle"];
                $tabLibelleTypeQuestion = $data["typeQuestion"];
                $tabValeurHierachie = $data["valeurHierachie"];

                if (count($tabLibelleQuestion) > 0) {
                    foreach ($tabLibelleQuestion as $key => $libelle) {
                        if ($libelle) {
                            //Retrouver un libelle
                            $question = $questionService->getByLibelleAndTypeQuestionAndSondage($libelle, $tabLibelleTypeQuestion[$key], $sondage->getId());
                            if ($question) {
                                $question->setDeletedAt(NULL);
                            }
                            else {
                                $question = new Question();
                                $question->setCreatedAt($dateOperation);
                                $question->setLibelle($libelle);
                                //$question->setProposition($proposition);
                                //$question->setQuestion($question);
                                $question->setSondage($sondage);
                                $question->setTypeQuestion($typeQuestionService->getById($tabLibelleTypeQuestion[$key]));
                                $question->setCreatedBy($this->user);
                            }

                            if ($tabValeurHierachie[$key])
                                $question->setValeurHierachie($valeurHierachie[$key]);
                            $question->setUpdatedAt($dateOperation);
                            $question->setEtat(TRUE);

                            $dataQuestion = $questionService->editer($question);
                        }
                    }
                    $jsonData["code"] = 1;
                }
                else {
                    throw new Exception("Aucune enquete cree.");
                }
                return new JsonModel($jsonData);
            }
            catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function questionAction() {

        $idSondage = $this->params()->fromRoute('id');
        $limit = $this->params()->fromQuery('limit', 10);
        $offset = $this->params()->fromQuery('offset', 0);
        $search = $this->params()->fromQuery('search', null);
        $type = $this->params()->fromQuery('sondage');

        $this->layout()->setVariable("btnModalAjout", FALSE);
        $this->layout()->setVariable("menuPrincipal", "Enquete");
        $this->layout()->setVariable("icone", "fa fa-search");
        $this->layout()->setVariable("lienPrincipal", "app/sondage");
        $this->layout()->setVariable("menuInterne", "sondage");
        $this->layout()->setVariable("titleControlleur", "Sondage");
        $this->layout()->setVariable("sondageActive", "active");


        if ($idSondage > 0) {
            $viewData = array();
            $viewData["code"] = 1;

            $sondageService = $this->getServiceLocator()->get('sondage_service');
            $sondage = $sondageService->getById($idSondage);
            if (!$sondage) {
                $viewData["code"] = 0;
                $viewData["msg"] = "Enquete introuvable";
                
                return new ViewModel($viewData);
            }

            if (!$type) {
                $typePropositionService = $this->getServiceLocator()->get('type_proposition_service');
                $typeQuestionService    = $this->getServiceLocator()->get('type_question_service');
                $questionService        = $this->getServiceLocator()->get('question_service');
                $participerService      = $this->getServiceLocator()->get('participer_service');
                $criteres = array('deletedAt' => NULL);
                $viewData["code"] = 1;
                $viewData["sondage"]          = $sondage;
                $viewData["nbQuestion"]       = count($questionService->lister(array('deletedAt' => NULL, 'sondage'=>$sondage->getId())));
                $viewData["nbParticipant"]    = count($participerService->lister(array('deletedAt' => NULL, 'sondage'=>$sondage->getId())));
                $viewData["typeQuestions"]    = $typeQuestionService->lister($criteres, array('libelle' => 'ASC'));
                $viewData["typePropositions"] = $typePropositionService->lister($criteres, array('libelle' => 'ASC'));
                return new ViewModel($viewData);
            }
            else {
                $jsonData = array();
//                $this->init();
//                $messageUtilisateurService = $this->getServiceLocator()->get('message_utilisateur_service');
//                $criteres = array('deletedAt' => NULL);
//
//                $listeMessageUser = $messageUtilisateurService->lister($criteres, array('id' => 'DESC'), $limit, $offset);
//                $countData = $messageUtilisateurService->lister($criteres);
//
//                $tabData = array();
//                foreach ($listeMessageUser as $messageUser) {
//                    $tabData[] = json_decode($this->serializer->serialize($messageUser, 'json'));
//                }
//                $jsonData["rows"] = $tabData;
//                $jsonData["total"] = count($countData);
                return new JsonModel($jsonData);
            }
        }
        else {
            return new ViewModel(["code" => 0, "msg" => "Saisie invalide", "data" => NULL]);
        }
    }

}
