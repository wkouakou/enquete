<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 10 avr. 2017 A  05:00:56
  ##############  File     : TypePropositionController.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Controller;

use Application\Model\Entity\TypeProposition;
use DateTime;
use Exception;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class TypePropositionController extends AbstractActionController {

    private $serializer;
    private $user = null;

    private function init() {
        $this->serializer = $this->getServiceLocator()->get('jms_serializer.serializer');
        $this->user = $this->identity();
    }

    public function indexAction() {
        $this->layout()->setVariable("btnModalAjout", false);
        $this->layout()->setVariable("menuPrincipal", "Proposition");
        $this->layout()->setVariable("icone", "fa fa-question");
        $this->layout()->setVariable("lienPrincipal", "app/type-proposition");
        $this->layout()->setVariable("menuInterne", "type-proposition");
        $this->layout()->setVariable("titleControlleur", "Type proposition");
        //$this->layout()->setVariable("parametreActive", "active");
        $this->layout()->setVariable("typePropositionActive", "active");

        $viewData = array();
        return new ViewModel($viewData);
    }

    public function listerAction() {
        $jsonData = array();
        $this->init();
        $typePropositionService = $this->getServiceLocator()->get('type_proposition_service');

        $limit = $this->params()->fromQuery('limit', 10);
        $offset = $this->params()->fromQuery('offset', 0);
        //$search = $this->params()->fromQuery('search', null);
        $criteres = array('deletedAt' => NULL);

        $listeTypeQestions = $typePropositionService->lister($criteres, array('libelle' => 'ASC'), $limit, $offset);

        $tabData = array();
        foreach ($listeTypeQestions as $type_proposition) {
            $tabData[] = json_decode($this->serializer->serialize($type_proposition, 'json'));
        }
        $countData = $typePropositionService->lister($criteres);
        $jsonData["rows"] = $tabData;
        $jsonData["total"] = count($countData);
        return new JsonModel($jsonData);
    }

    public function ajouterAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["libelle"])) {
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                $typePropositionService = $this->getServiceLocator()->get('type_proposition_service');

                $type_proposition = new TypeProposition();
                $type_proposition->setCreatedAt($dateOperation);
                $type_proposition->setLibelle($data["libelle"]);
                $type_proposition->setType($data["type"]);
                $type_proposition->setCreatedBy($this->user);

                $type_proposition->setUpdatedAt($dateOperation);
                if (isset($data["etat"]))
                    $type_proposition->setEtat(TRUE);
                else
                    $type_proposition->setEtat(FALSE);

                $dataTypeQestion = $typePropositionService->editer($type_proposition);

                $jsonData["data"] = json_decode($this->serializer->serialize($dataTypeQestion, 'json'));
                return new JsonModel($jsonData);
            } catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function modifierAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        try {
            $this->init();
            $dateOperation = new DateTime('now');
            $data = $request->getPost();
            $typePropositionService = $this->getServiceLocator()->get('type_proposition_service');
            $type_proposition = $typePropositionService->getById($data["idTypeQestion"]);
            if (!$type_proposition) {
                throw new Exception("Impossible d'effectuer cette opération.\nVeuillez réessayer plus tard.");
            }

            //Retrouver un libelle
            $type_proposition2 = $typePropositionService->getByLibelle($data["libelle"]);
            if ($type_proposition2 && $type_proposition->getId() != $type_proposition2->getId() && $type_proposition2->getDeletedAt() == NULL) {
                throw new Exception("Ce type de proposition existe deja.\nVeuillez changer de libelle.");
            } elseif ($type_proposition2 && $type_proposition->getId() != $type_proposition2->getId() && $type_proposition2->getDeletedAt()) {
                throw new Exception("Ce type de proposition a ete supprime.\nVeuillez recreer a nouveau.");
            } else {
                $type_proposition->setUpdatedAt($dateOperation);
                $type_proposition->setLibelle($data["libelle"]);
                $type_proposition->setType($data["type"]);
                if (isset($data["etat"]))
                    $type_proposition->setEtat(TRUE);
                else
                    $type_proposition->setEtat(FALSE);
                $type_proposition->setCreatedBy($this->user);
            }

            $dataTypeQestion = $typePropositionService->editer($type_proposition);
            $jsonData["data"] = json_decode($this->serializer->serialize($dataTypeQestion, 'json'));
            return new JsonModel($jsonData);
        } catch (Exception $exc) {
            $jsonData["code"] = -1;
            $jsonData["data"] = NULL;
            $jsonData["msg"] = $exc->getMessage();
            return new JsonModel($jsonData);
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function supprimerAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["idTypeQestion"])) {
            try {
                $this->init();
                $data = $request->getPost();
                $typePropositionService = $this->getServiceLocator()->get('type_proposition_service');
                $type_proposition = $typePropositionService->getById($data["idTypeQestion"]);
                if (!$type_proposition) {
                    throw new Exception("Impossible d'effectuer cette opération.\nVeuillez réessayer plus tard.");
                }

                $dataTypeQestion = $typePropositionService->supprimer($type_proposition);
                $jsonData["data"] = json_decode($this->serializer->serialize($dataTypeQestion, 'json'));

                return new JsonModel($jsonData);
            } catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

}
