<?php

namespace Application\Controller;

use Application\Model\Entity\Utilisateur;
use DateTime;
use Exception;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class UtilisateurController extends AbstractActionController {

    private $em;
    private $serializer;
    private $user = null;

    private function init() {
        $this->em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $this->serializer = $this->getServiceLocator()->get('jms_serializer.serializer');
        $this->user = $this->identity();
    }

    public function indexAction() {
        //$this->layout("layout/stock");
        $this->layout()->setVariable("menuPrincipal", "Compte");
        $this->layout()->setVariable("lienPrincipal", "application/utilisateur");
        $this->layout()->setVariable("menuInterne", "Utilisateur");
        $this->layout()->setVariable("icone", "fa fa-users");
        $this->layout()->setVariable("btnModalAjout", true);
        $this->layout()->setVariable("titleControlleur", "Utilisateur");
        $this->layout()->setVariable("aclActive", "active");
        $this->layout()->setVariable("utilisateurActive", "active");

        $viewData = array();
        $roleService = $this->getServiceLocator()->get('role_service');
        $viewData["listeRoles"] = $roleService->lister();
        return new ViewModel($viewData);
    }

    public function addAction() {
        //sleep(3);
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost()) {
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                $utilisateurService = $this->getServiceLocator()->get('utilisateur_service');
                $roleService = $this->getServiceLocator()->get('role_service');

                $utilisateur = new Utilisateur();
                $utilisateur->setCreatedAt($dateOperation);
                $utilisateur->setFullName($data["fullName"]);
                $utilisateur->setEmail($data["email"]);
                $utilisateur->setContact($data["contact"]);
                $utilisateur->setCivilite($data["civilite"]);
                $utilisateur->setPassword($data["password"]);
                $role = $roleService->getRoleById($data["role"]);
                $utilisateur->setStatus(true);
                /* if (isset($data["role"]) && $data["role"]) {
                  $role = $roleService->getRoleById($data["role"]);
                  $utilisateur->setStatus(true);
                  $message = NULL;
                  } else {
                  $role = $roleService->getRoleByName("Membre");
                  $utilisateur->setStatus(FALSE);
                  $message = new Message();
                  $message->setSubject('Demande d\'inscription.');
                  $message->addFrom('infos@restodici.com', 'Restodici');
                  $message->addTo($utilisateur->getEmail(), $utilisateur->getFullName());
                  } */
                $utilisateur->setRole($role);
                $utilisateur->setUpdatedAt($dateOperation);
                $utilisateur->setCreatedBy($this->user);
                $dataUtilisateur = $utilisateurService->editer($utilisateur);
                /* if ($message) {
                  $annee = date("Y");
                  $idMembre = $dataUtilisateur->getId();
                  $hash = $annee * $idMembre * 2;
                  $message->addHtmlPart('Bonjour ' . $dataUtilisateur->getCivilite() . ' ' . $dataUtilisateur->getFullName() . ','
                  . '<br>Votre demande d\'inscription a bien été enregistrée.'
                  . '<br>Veuillez cliquer sur ce <a href="http://ait-ci.com/apps/inscription/' . $hash . '-' . sha1($dataUtilisateur->getEmail()) . '">lien</a> pour confirmer votre inscription.');
                  /** @var MailService $mailService *
                  $mailService = $this->getServiceLocator()->get('MailMan\Smtp');
                  $mailService->send($message);
                  } */
                $jsonData["data"] = json_decode($this->serializer->serialize($dataUtilisateur, 'json'));
                return new JsonModel($jsonData);
            }
            catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                //$jsonData["msg"] = $exc->getMessage();
                $jsonData["msg"] = "Erreur survenue, vérifier cet utilisateur ne dispose pas dejà d'un compte";
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function listAction() {
        $jsonData = array();
        $this->init();
        $utilisateurService = $this->getServiceLocator()->get('utilisateur_service');
        $limit = $this->params()->fromQuery('limit', 10);
        $offset = $this->params()->fromQuery('offset', 0);
        $role = $this->params()->fromQuery('role', 0);
        $critere = array('deletedAt' => NULL);
        if ($role) {
            $critere["role"] = $role;
        }
        $listeUtilisateurs = $utilisateurService->lister($critere, array(), $limit, $offset);
        $tabData = array();
        foreach ($listeUtilisateurs as $utilisateur) {
            $tabData[] = json_decode($this->serializer->serialize($utilisateur, 'json'));
        }
        $countData = $utilisateurService->lister($critere);
        $jsonData["rows"] = $tabData;
        $jsonData["total"] = count($countData);
        return new JsonModel($jsonData);
    }

    public function updateAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        $update = false;
        if ($request->isPost()) {
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                $utilisateurService = $this->getServiceLocator()->get('utilisateur_service');
                $utilisateur = $utilisateurService->getById($data["id"]);
                if (!$utilisateur) {
                    throw new Exception("Impossible d'effectuer cette opération.\nVeuillez réessayer plus tard.");
                }
                if (isset($data["fullName"]) && $data["fullName"]) {
                    $utilisateur->setFullName($data["fullName"]);
                    $update = true;
                }
                if (isset($data["contact"]) && $data["contact"]) {
                    $utilisateur->setContact($data["contact"]);
                    $update = true;
                }
                if (isset($data["password"]) && $data["password"]) {
                    $utilisateur->setPassword($data["password"]);
                    $update = true;
                }

                // Update registration IDs
                if (isset($data["androidId"]) && $data["androidId"]) {
                    $utilisateur->setAndroidId($data["androidId"]);
                    $update = true;
                }
                if (isset($data["iosId"]) && $data["iosId"]) {
                    $utilisateur->setIosId($data["iosId"]);
                    $update = true;
                }
                if (isset($data["blackberryId"]) && $data["blackberryId"]) {
                    $utilisateur->setBlackberryId($data["blackberryId"]);
                    $update = true;
                }
                if (isset($data["firefoxId"]) && $data["firefoxId"]) {
                    $utilisateur->setFirefoxId($data["androidId"]);
                    $update = true;
                }
                if (isset($data["windowId"]) && $data["windowId"]) {
                    $utilisateur->setWindowId($data["windowId"]);
                    $update = true;
                }
                // End update registration IDs

                if (isset($data["status"]) && $data["status"]) {
                    $utilisateur->setStatus($data["status"]);
                    $update = true;
                }
                if ($update) {
                    $utilisateur->setUpdatedAt($dateOperation);
                    $utilisateur->setCreatedBy($this->user);
                    $dataUtilisateur = $utilisateurService->editer($utilisateur);
                    $jsonData["data"] = json_decode($this->serializer->serialize($dataUtilisateur, 'json'));
                }
                else {
                    $jsonData["msg"] = "Aucun champs affecté";
                    $jsonData["data"] = json_decode($this->serializer->serialize($utilisateur, 'json'));
                }
                return new JsonModel($jsonData);
            }
            catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function inscriptionAction() {
        $this->layout("layout/login");
        $viewData = array("code" => 0, "msg" => "Demande non valide.");
        $hash = $this->params()->fromRoute("hash");

        list($hashId, $hashEmail) = split('-', $hash);
        $annee = (int) date("Y");
        $idMembre = $hashId / ($annee * 2);
        if ($idMembre && $hashEmail) {
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $utilisateurService = $this->getServiceLocator()->get('utilisateur_service');
                $utilisateur = $utilisateurService->getById($idMembre);
                if (!$utilisateur) {
                    throw new Exception("Impossible d'effectuer cette opération.\nVeuillez réessayer plus tard.");
                }
                elseif (sha1($utilisateur->getEmail()) !== $hashEmail) {
                    throw new Exception("Compte utilisateur invalide.\nVeuillez contacter l'administrateur.");
                }
                $utilisateur->setStatus(true);
                $utilisateur->setUpdatedAt($dateOperation);
                $viewData["code"] = 1;
                $viewData["msg"] = "Opération effectuée avec succès";
                $viewData["data"] = $utilisateurService->editer($utilisateur);
            }
            catch (Exception $exc) {
                $viewData["code"] = -1;
                $viewData["data"] = NULL;
                $viewData["msg"] = $exc->getMessage();
            }
        }
        return new ViewModel($viewData);
    }

    public function deleteAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost()) {
            try {
                $this->init();
                $data = $request->getPost();
                $utilisateurService = $this->getServiceLocator()->get('utilisateur_service');
                $utilisateur = $utilisateurService->getUtilisateurById($data["idUtilisateur"]);
                if (!$utilisateur) {
                    throw new Exception("Impossible d'effectuer cette opération.\nVeuillez réessayer plus tard.");
                }
                $dataUtilisateur = $utilisateurService->supprimer($utilisateur);
                $jsonData["data"] = json_decode($this->serializer->serialize($dataUtilisateur, 'json'));
                return new JsonModel($jsonData);
            }
            catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }
    
    public function missingAction(){
        $jsonData = [
            'code'=>0,
            'msg'=>'Utilisateur introuvable',
            'data'=>NULL
        ];
        return new JsonModel($jsonData);
    }

}
