<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 10 avr. 2017 A  05:00:56
  ##############  File     : TypeQuestionController.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Controller;

use Application\Model\Entity\TypeQuestion;
use DateTime;
use Exception;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class TypeQuestionController extends AbstractActionController {

    private $serializer;
    private $user = null;

    private function init() {
        $this->serializer = $this->getServiceLocator()->get('jms_serializer.serializer');
        $this->user = $this->identity();
    }

    public function indexAction() {
        $this->layout()->setVariable("btnModalAjout", FALSE);
        $this->layout()->setVariable("menuPrincipal", "Question");
        $this->layout()->setVariable("icone", "fa fa-question");
        $this->layout()->setVariable("lienPrincipal", "app/type-question");
        $this->layout()->setVariable("menuInterne", "type-question");
        $this->layout()->setVariable("titleControlleur", "Type question");
        //$this->layout()->setVariable("parametreActive", "active");
        $this->layout()->setVariable("typeQuestionActive", "active");

        $viewData = array();
        return new ViewModel($viewData);
    }

    public function listerAction() {
        $jsonData = array();
        $this->init();
        $typeQuestionService = $this->getServiceLocator()->get('type_question_service');

        $limit = $this->params()->fromQuery('limit', 10);
        $offset = $this->params()->fromQuery('offset', 0);
        $search = $this->params()->fromQuery('search', null);
        $criteres = array('deletedAt' => NULL);

        if ($search) {
            $criteres["question"] = "%" . $search . "%";
            $listeTypeQestions = $typeQuestionService->getByLibelleLike($criteres, array("quest.libelle" => "ASC"), $limit, $offset);
            $countData = $typeQuestionService->getByLibelleLike($criteres);
        }
        else {
            $listeTypeQestions = $typeQuestionService->lister($criteres, array('libelle' => 'ASC'), $limit, $offset);
            $countData = $typeQuestionService->lister($criteres);
        }

        //$listeTypeQestions = $typeQuestionService->lister(array('deletedAt' => NULL), array('libelle' => 'asc'), $limit, $offset);

        $tabData = array();
        foreach ($listeTypeQestions as $type_question) {
            $tabData[] = json_decode($this->serializer->serialize($type_question, 'json'));
        }
        $countData = $typeQuestionService->lister(array('deletedAt' => NULL));
        $jsonData["rows"] = $tabData;
        $jsonData["total"] = count($countData);
        return new JsonModel($jsonData);
    }

    public function ajouterAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["libelle"])) {
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                $typeQuestionService = $this->getServiceLocator()->get('type_question_service');

                //Retrouver un libelle
                $type_question = $typeQuestionService->getByLibelle($data["libelle"]);
                if ($type_question) {
                    $type_question->setDeletedAt(NULL);
                }
                else {
                    $type_question = new TypeQuestion();
                    $type_question->setCreatedAt($dateOperation);
                    $type_question->setLibelle($data["libelle"]);
                    $type_question->setType($data["type"]);
                    $type_question->setCreatedBy($this->user);
                }
                
                $type_question->setUpdatedAt($dateOperation);
                if (isset($data["etat"]))
                    $type_question->setEtat(TRUE);
                else
                    $type_question->setEtat(FALSE);

                $dataTypeQestion = $typeQuestionService->editer($type_question);

                $jsonData["data"] = json_decode($this->serializer->serialize($dataTypeQestion, 'json'));
                return new JsonModel($jsonData);
            }
            catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function modifierAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        try {
            $this->init();
            $dateOperation = new DateTime('now');
            $data = $request->getPost();
            $typeQuestionService = $this->getServiceLocator()->get('type_question_service');
            $type_question = $typeQuestionService->getById($data["idTypeQestion"]);
            if (!$type_question) {
                throw new Exception("Impossible d'effectuer cette opération.\nVeuillez réessayer plus tard.");
            }

            //Retrouver un libelle
            $type_question2 = $typeQuestionService->getByLibelle($data["libelle"]);
            if ($type_question2 && $type_question->getId() != $type_question2->getId() && $type_question2->getDeletedAt() == NULL) {
                throw new Exception("Ce type de question existe deja.\nVeuillez changer de libelle.");
            }
            elseif ($type_question2 && $type_question->getId() != $type_question2->getId() && $type_question2->getDeletedAt()) {
                throw new Exception("Ce type de question a ete supprime.\nVeuillez recreer a nouveau.");
            }
            else {
                $type_question->setUpdatedAt($dateOperation);
                $type_question->setLibelle($data["libelle"]);
                $type_question->setType($data["type"]);
                if (isset($data["etat"]))
                    $type_question->setEtat(TRUE);
                else
                    $type_question->setEtat(FALSE);
                $type_question->setCreatedBy($this->user);
            }

            $dataTypeQestion = $typeQuestionService->editer($type_question);
            $jsonData["data"] = json_decode($this->serializer->serialize($dataTypeQestion, 'json'));
            return new JsonModel($jsonData);
        }
        catch (Exception $exc) {
            $jsonData["code"] = -1;
            $jsonData["data"] = NULL;
            $jsonData["msg"] = $exc->getMessage();
            return new JsonModel($jsonData);
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function supprimerAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["idTypeQestion"])) {
            try {
                $this->init();
                $data = $request->getPost();
                $typeQuestionService = $this->getServiceLocator()->get('type_question_service');
                $type_question = $typeQuestionService->getById($data["idTypeQestion"]);
                if (!$type_question) {
                    throw new Exception("Impossible d'effectuer cette opération.\nVeuillez réessayer plus tard.");
                }

                $dataTypeQestion = $typeQuestionService->supprimer($type_question);
                $jsonData["data"] = json_decode($this->serializer->serialize($dataTypeQestion, 'json'));

                return new JsonModel($jsonData);
            }
            catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

}
