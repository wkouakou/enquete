<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 25 avr. 2017 A  06:10:48
  ##############  File     : StatistiqueController.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use mPDF;

class StatistiqueController extends AbstractActionController {

    private $serializer;
    private $user = null;

    private function init() {
        $this->serializer = $this->getServiceLocator()->get('jms_serializer.serializer');
        $this->user = $this->identity();
    }

    public function indexAction() {

        $viewData = array();
        return new ViewModel($viewData);
    }

    public function enqueteGroupeAction() {
        $this->layout()->setVariable("btnModalAjout", FALSE);
        $this->layout()->setVariable("menuPrincipal", "Statistique");
        $this->layout()->setVariable("icone", "fa fa-th-list");
        $this->layout()->setVariable("lienPrincipal", "app/statistique");
        $this->layout()->setVariable("menuInterne", "enquete-groupe");
        $this->layout()->setVariable("titleControlleur", "Enquete par groupe");
        $this->layout()->setVariable("statistiqueActive", "active");
        $this->layout()->setVariable("enqueteGroupeActive", "active");

        $viewData = array();
        return new ViewModel($viewData);
    }

    public function enqueteParticipantAction() {
        $this->layout()->setVariable("btnModalAjout", FALSE);
        $this->layout()->setVariable("menuPrincipal", "Statistique");
        $this->layout()->setVariable("icone", "fa fa-th-list");
        $this->layout()->setVariable("lienPrincipal", "app/statistique");
        $this->layout()->setVariable("menuInterne", "enquete-participant");
        $this->layout()->setVariable("titleControlleur", "Participant par enquete");
        $this->layout()->setVariable("statistiqueActive", "active");
        $this->layout()->setVariable("enqueteParticipantActive", "active");

        $viewData = array();

        return new ViewModel($viewData);
    }

    public function participerAction() {
        $jsonData = array();
        $this->init();

        $limit = $this->params()->fromQuery('limit', 10);
        $offset = $this->params()->fromQuery('offset', 0);
        //$search = $this->params()->fromQuery('search', null);
        $sondage = $this->params()->fromQuery('sondage', null);
        $genre = $this->params()->fromQuery('genre', null);
        if (isset($genre) && $genre > 0) {
            if ($genre == 1)
                $genre = 'M';
            else
                $genre = 'F';
        }
        else {
            $genre = NULL;
        }

        //$sondage = $sondage > 0 ? $sondage : NULL;
        $participerService = $this->getServiceLocator()->get('participer_service');
        $listeParticipes = $participerService->getCorrespondanceBy($genre, $sondage, $offset, $limit);
        $countData = $participerService->getCorrespondanceBy($genre, $sondage);

        $tabData = array();
        foreach ($listeParticipes as $participer) {
            $tabData[] = json_decode($this->serializer->serialize($participer, 'json'));
        }
        $jsonData["rows"] = $tabData;
        $jsonData["total"] = count($countData);
        return new JsonModel($jsonData);
    }

    public function enqueteAction() {
        $this->layout()->setVariable("btnModalAjout", FALSE);
        $this->layout()->setVariable("menuPrincipal", "Statistique");
        $this->layout()->setVariable("icone", "fa fa-th-list");
        $this->layout()->setVariable("lienPrincipal", "app/statistique");
        $this->layout()->setVariable("menuInterne", "enquete");
        $this->layout()->setVariable("titleControlleur", "Enquete");
//        $this->layout()->setVariable("statistiqueActive", "active");
//        $this->layout()->setVariable("enqueteGroupeActive", "active");

        $this->init();
        $modelData = array();
        $idSondage = $this->params()->fromRoute('id');
        try {
            if ($idSondage > 0) {
                //groupe
                $sondageService = $this->getServiceLocator()->get('sondage_service');
                $sondage = $sondageService->getById($idSondage);
                if (!$sondage) {
                    $modelData["code"] = 0;
                    $modelData["msg"] = "Impossible d'effectuer cette opération.\n enquete introuvable.";
                    return new ViewModel($modelData);
                }
                $modelData["code"] = 1;
                $modelData["sondage"] = $sondage;

                $questionService = $this->getServiceLocator()->get('question_service');
                $questions = $questionService->lister(["sondage" => $sondage->getId(), "deletedAt" => NULL]);
                $modelData["questions"] = $questions;

                //Regroupement des propositions par question
                $propositionService = $this->getServiceLocator()->get('proposition_service');
                $listesProposition = [];
                foreach ($questions as $question) {
                    $listesProposition[$question->getId()] = $propositionService->lister(["question" => $question->getId(), "deletedAt" => NULL]);
                }
                $modelData["propositions"] = $listesProposition;

                //Regroupement des reponses par proposition
                $reponseService = $this->getServiceLocator()->get('reponse_service');
                $listesReponse = [];
                foreach ($listesProposition as $propositions) {
                    foreach ($propositions as $proposition) {
                        $listesReponse[$proposition->getId()] = $reponseService->lister(["proposition" => $proposition->getId(), "deletedAt" => NULL]);
                    }
                }
                $modelData["reponses"] = $listesReponse;
            } else {
                return $this->redirect()->toRoute('app');
            }
        } catch (Exception $exc) {

            $modelData["erreur"] = $exc->getMessage();
            $modelData["data"] = null;
            $modelData["code"] = 0;
        }

        return new ViewModel($modelData);
    }

    public function imprimerEnqueteParticipantAction() {
        $sondage = NULL;
        $libelleEtat = "LISTE DE PARTICIPANTS";
        $enteteAvecLibelle = '';
        $critereEnquete = '';
        $listeParticipes = [];
        //Desactiver la vue
        $viewModelData = new ViewModel();
        $viewModelData->setTerminal(true);
        
        try {
            $request = $this->getRequest();
            if ($request->isPost() && isset($request->getPost()["sondageFilter"])) {
                $data = $request->getPost();
                
                $idSondage = $data["sondageFilter"] && ((int) $data["sondageFilter"] > 0) ? (int) $data["sondageFilter"] : NULL;
                $genre = NULL;
                
                if (isset($data["genreFilter"]) && $data["genreFilter"] > 0) {
                    $genre = $data["genreFilter"] == 1 ? 'M' : 'F';
                }
                
                $participerService = $this->getServiceLocator()->get('participer_service');
                $listeParticipes = $participerService->getCorrespondanceBy($genre, $idSondage);
                if(count($listeParticipes) > 0){
                    if (!$idSondage) { //Affichane de l'enquete pour chaque ligne
                        $sondage = $listeParticipes[0]->getSondage();
                    }
                    else{ //On masque l'affichage de l'enquete
                        $critereEnquete = strtoupper($listeParticipes[0]->getSondage()->getLibelle());
                        $libelleEtat = 'Participants - ' . $critereEnquete;
                    }
                }
            }
            
            
            ob_start();
            require_once(dirname(__FILE__) . '/../../../view/application/statistique/imprimer-enquete-participant.phtml');
            $content = ob_get_clean();
            
            $enteteAvecLibelle = '<p style="text-align:center;" class="label label-default">' . $libelleEtat . '</p>';
            $piedAvecNumero = '<p style="text-align:right;">{PAGENO}/{nbpg}</p>';
            $mpdf = new mPDF('ar', '', '', 32, 15, 15, 15, 10, 10);
            //$mpdf->setHeader($h);
            $mpdf->SetHTMLHeader($enteteAvecLibelle);
            $mpdf->SetHTMLFooter($piedAvecNumero);

            $mpdf->SetFont('ar_k_001');
            $mpdf->AddPage('P');
            $mpdf->SetDisplayMode('fullpage');
            // LOAD a stylesheet
            $stylesheet = file_get_contents(PUBLIC_PATH . '/css/custom-print-bootstrap.min.css');
            $mpdf->WriteHTML($stylesheet, 1); // The parameter 1 tells that this is css/style only and no body/html/text

            $mpdf->WriteHTML($content);
            $mpdf->Output($libelleEtat.'.pdf', 'I');
        } catch (Exception $e) {
            echo $e;
            exit;
        }
    }

}
