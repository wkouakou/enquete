<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 10 avr. 2017 A  05:00:22
  ##############  File     : ReponseController.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Controller;

class ReponseController extends AbstractActionController {

    private $serializer;
    private $user = null;

    private function init() {
        $this->serializer = $this->getServiceLocator()->get('jms_serializer.serializer');
        $this->user = $this->identity();
    }

    public function indexAction() {
        $this->layout()->setVariable("btnModalAjout", true);
        $this->layout()->setVariable("menuPrincipal", "Paramètre");
        $this->layout()->setVariable("icone", "fa fa-cogs");
        $this->layout()->setVariable("lienPrincipal", "app/commissariat");
        $this->layout()->setVariable("menuInterne", "commissariat");
        $this->layout()->setVariable("titleControlleur", "Commissariat");
        $this->layout()->setVariable("parametreActive", "active");
        $this->layout()->setVariable("commissariatActive", "active");

        $viewData = array();
        $communeService = $this->getServiceLocator()->get('commune_service');
        $viewData["listeCommunes"] = $communeService->lister(array("deletedAt" => null));
        return new ViewModel($viewData);
    }

    public function listerAction() {
        $jsonData = array();
        $this->init();
        $commissariatService = $this->getServiceLocator()->get('commissariat_service');

        $limit = $this->params()->fromQuery('limit', 10);
        $offset = $this->params()->fromQuery('offset', 0);
        $commune = $this->params()->fromQuery('commune', 0);
        $search = $this->params()->fromQuery('search');

        if ($commune) {
            $listeCommissariats = $commissariatService->lister(array('deletedAt' => NULL, "commune" => $commune), array('libelle' => 'asc'));
        } else {
            $listeCommissariats = $commissariatService->lister(array('deletedAt' => NULL), array('libelle' => 'asc'), $limit, $offset);
        }

        $tabData = array();
        foreach ($listeCommissariats as $commissariat) {
            $tabData[] = json_decode($this->serializer->serialize($commissariat, 'json'));
        }
        $countData = $commissariatService->lister(array('deletedAt' => NULL));
        $jsonData["rows"] = $tabData;
        $jsonData["total"] = count($countData);
        return new JsonModel($jsonData);
    }

    public function ajouterAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["libelle"])) {
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                $commissariatService = $this->getServiceLocator()->get('commissariat_service');
                $communeService = $this->getServiceLocator()->get('commune_service');

                $commissariat = new Commissariat();
                $commissariat->setCreatedAt($dateOperation);
                $commissariat->setUpdatedAt($dateOperation);
                $commissariat->setCommune($communeService->getById($data["commune"]));
                $commissariat->setLibelle($data["libelle"]);
                $commissariat->setAdresse($data["adresse"]);
                $commissariat->setTelephone($data["telephone"]);
                $commissariat->setCreatedBy($this->user);

                $dataCommissariat = $commissariatService->editer($commissariat);
                $jsonData["data"] = json_decode($this->serializer->serialize($dataCommissariat, 'json'));
                return new JsonModel($jsonData);
            } catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function modifierAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        try {
            $this->init();
            $dateOperation = new DateTime('now');
            $data = $request->getPost();
            $commissariatService = $this->getServiceLocator()->get('commissariat_service');
            $commissariat = $commissariatService->getById($data["idCommissariat"]);
            $communeService = $this->getServiceLocator()->get('commune_service');
            if (!$commissariat) {
                throw new Exception("Impossible d'effectuer cette opération.\nVeuillez réessayer plus tard.");
            }
            $commissariat->setCreatedAt($dateOperation);
            $commissariat->setUpdatedAt($dateOperation);
            $commissariat->setCommune($communeService->getById($data["commune"]));
            $commissariat->setLibelle($data["libelle"]);
            $commissariat->setAdresse($data["adresse"]);
            $commissariat->setTelephone($data["telephone"]);
            $commissariat->setCreatedBy($this->user);

            $dataCommissariat = $commissariatService->editer($commissariat);
            $jsonData["data"] = json_decode($this->serializer->serialize($dataCommissariat, 'json'));
            return new JsonModel($jsonData);
        } catch (Exception $exc) {
            $jsonData["code"] = -1;
            $jsonData["data"] = NULL;
            $jsonData["msg"] = $exc->getMessage();
            return new JsonModel($jsonData);
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function supprimerAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["idCommissariat"])) {
            try {
                $this->init();
                $data = $request->getPost();
                $commissariatService = $this->getServiceLocator()->get('commissariat_service');
                $commissariat = $commissariatService->getById($data["idCommissariat"]);
                if (!$commissariat) {
                    throw new Exception("Impossible d'effectuer cette opération.\nVeuillez réessayer plus tard.");
                }

                $dataCommissariat = $commissariatService->supprimer($commissariat);
                $jsonData["data"] = json_decode($this->serializer->serialize($dataCommissariat, 'json'));

                return new JsonModel($jsonData);
            } catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

}
