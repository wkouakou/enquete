<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 10 avr. 2017 A  04:59:51
  ##############  File     : PropositionController.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Controller;

use Application\Model\Entity\Proposition;
use DateTime;
use Exception;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class PropositionController extends AbstractActionController {

    private $serializer;
    private $user = null;

    private function init() {
        $this->serializer = $this->getServiceLocator()->get('jms_serializer.serializer');
        $this->user = $this->identity();
    }

    public function indexAction() {
        $this->layout()->setVariable("btnModalAjout", FALSE);
        $this->layout()->setVariable("menuPrincipal", "Proposition de reponse");
        $this->layout()->setVariable("icone", "fa fa-archive");
        $this->layout()->setVariable("lienPrincipal", "app/proposition");
        $this->layout()->setVariable("menuInterne", "proposition");
        $this->layout()->setVariable("titleControlleur", "Proposition");
        //$this->layout()->setVariable("parametreActive", "active");
        $this->layout()->setVariable("propositionActive", "active");

        $viewData = array();
        
        $questionService = $this->getServiceLocator()->get('question_service');
        $questions = $questionService->lister(array('deletedAt' => NULL));
        $viewData['listeQuestions'] = $questions;
        
        return new ViewModel($viewData);
    }

    public function listerAction() {
        $jsonData = array();
        $this->init();
        $propositionService = $this->getServiceLocator()->get('proposition_service');

        $limit = $this->params()->fromQuery('limit', 10);
        $offset = $this->params()->fromQuery('offset', 0);
        $search = $this->params()->fromQuery('search', null);
        $question = $this->params()->fromQuery('question', null);
        $criteres = array('deletedAt' => NULL);
        if (isset($question) && is_numeric($question) && $question > 0)
            $criteres["question"] = $question;

        if ($search) {
            $criteres["questions"] = "%" . $search . "%";
            $listePropositions = $propositionService->getByLibelleLike($criteres, array("quest.libelle" => "ASC"), $limit, $offset);
            $countData = $propositionService->getByLibelleLike($criteres);
        }
        else {
            $listePropositions = $propositionService->lister($criteres, array('libelle' => 'ASC'), $limit, $offset);
            $countData = $propositionService->lister($criteres);
        }

        //$listePropositions = $propositionService->lister(array('deletedAt' => NULL), array('libelle' => 'asc'), $limit, $offset);

        $tabData = array();
        foreach ($listePropositions as $proposition) {
            $tabData[] = json_decode($this->serializer->serialize($proposition, 'json'));
        }
        $countData = $propositionService->lister($criteres);
        $jsonData["rows"] = $tabData;
        $jsonData["total"] = count($countData);
        return new JsonModel($jsonData);
    }

    public function ajouterAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["libelle"])) {
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                $propositionService = $this->getServiceLocator()->get('proposition_service');
                $questionService = $this->getServiceLocator()->get('question_service');
                $question = $questionService->getById($data["question"]);
                if (!$question) {
                    throw new Exception("Impossible d'effectuer cette opération.\nQuestion non trouve.");
                }

                //Retrouver un libelle
                $proposition = $propositionService->getByLibelleAndQuestion($data["libelle"], $data["question"]);
                if ($proposition) {
                    $proposition->setDeletedAt(NULL);
                }
                else {
                    $proposition = new Proposition();
                    $proposition->setCreatedAt($dateOperation);
                    $proposition->setLibelle($data["libelle"]);
                    $proposition->setQuestion($question);
                    $proposition->setCreatedBy($this->user);
                }
                $proposition->setUpdatedAt($dateOperation);
                if (isset($data["etat"]))
                    $proposition->setEtat(TRUE);
                else
                    $proposition->setEtat(FALSE);

                $dataProposition = $propositionService->editer($proposition);
                $jsonData["data"] = json_decode($this->serializer->serialize($dataProposition, 'json'));
                return new JsonModel($jsonData);
            }
            catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function modifierAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        try {
            $this->init();
            $dateOperation = new DateTime('now');
            $data = $request->getPost();
            $propositionService = $this->getServiceLocator()->get('proposition_service');
            $proposition = $propositionService->getById($data["idProposition"]);
            if (!$proposition) {
                throw new Exception("Impossible d'effectuer cette opération.\nProposition introuvable.");
            }

            $questionService = $this->getServiceLocator()->get('question_service');
            $question = $questionService->getById($data["question"]);
            if (!$question) {
                throw new Exception("Impossible d'effectuer cette opération.\nQuestion introuvable.");
            }

            //Retrouver un libelle
            $proposition2 = $propositionService->getByLibelleAndQuestion($data["libelle"], $data["question"]);
            if ($proposition2 && $proposition->getId() != $proposition2->getId() && $proposition2->getDeletedAt() == NULL) {
                throw new Exception("Cette proposition de reponse existe deja.\nVeuillez changer de libelle.");
            }
            elseif ($proposition2 && $proposition->getId() != $proposition2->getId() && $proposition2->getDeletedAt()) {
                throw new Exception("Cette proposition de reponse a ete supprimee.\nVeuillez recreer a nouveau.");
            }
            else {
                $proposition->setUpdatedAt($dateOperation);
                $proposition->setLibelle($data["libelle"]);
                $proposition->setQuestion($question);
                if (isset($data["etat"]))
                    $proposition->setEtat(TRUE);
                else
                    $proposition->setEtat(FALSE);
                $proposition->setCreatedBy($this->user);
            }

            $dataProposition = $propositionService->editer($proposition);
            $jsonData["data"] = json_decode($this->serializer->serialize($dataProposition, 'json'));
            return new JsonModel($jsonData);
        }
        catch (Exception $exc) {
            $jsonData["code"] = -1;
            $jsonData["data"] = NULL;
            $jsonData["msg"] = $exc->getMessage();
            return new JsonModel($jsonData);
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function supprimerAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["idProposition"])) {
            try {
                $this->init();
                $data = $request->getPost();
                $propositionService = $this->getServiceLocator()->get('proposition_service');
                $proposition = $propositionService->getById($data["idProposition"]);
                if (!$proposition) {
                    throw new Exception("Impossible d'effectuer cette opération.\nVeuillez réessayer plus tard.");
                }

                $dataProposition = $propositionService->supprimer($proposition);
                $jsonData["data"] = json_decode($this->serializer->serialize($dataProposition, 'json'));

                return new JsonModel($jsonData);
            }
            catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

}
