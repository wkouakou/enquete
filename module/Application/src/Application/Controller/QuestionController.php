<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 10 avr. 2017 A  05:00:06
  ##############  File     : QuestionController.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Controller;

use Application\Model\Entity\Entete;
use Application\Model\Entity\Proposition;
use Application\Model\Entity\Question;
use Application\Utils\UploadFile;
use DateTime;
use Exception;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class QuestionController extends AbstractActionController {

    private $serializer;
    private $user = null;

    private function init() {
        $this->serializer = $this->getServiceLocator()->get('jms_serializer.serializer');
        $this->user = $this->identity();
    }

    public function indexAction() {
        $this->layout()->setVariable("btnModalAjout", FALSE);
        $this->layout()->setVariable("menuPrincipal", "Question");
        $this->layout()->setVariable("icone", "fa fa-book");
        $this->layout()->setVariable("lienPrincipal", "app/question");
        $this->layout()->setVariable("menuInterne", "question");
        $this->layout()->setVariable("titleControlleur", "Question");
        //$this->layout()->setVariable("parametreActive", "active");
        $this->layout()->setVariable("questionActive", "active");

        $viewData = array();

        $sondageService = $this->getServiceLocator()->get('sondage_service');
        $sondages = $sondageService->lister(array('deletedAt' => NULL));
        $viewData['listeEnquetes'] = $sondages;

        $typeQuestionService = $this->getServiceLocator()->get('type_question_service');
        $typesQuestion = $typeQuestionService->lister(array('deletedAt' => NULL));
        $viewData['listeTypesQuestion'] = $typesQuestion;

        $questionService = $this->getServiceLocator()->get('question_service');
        $questions = $questionService->lister(array('deletedAt' => NULL));
        $viewData['listeQuestions'] = $questions;

        $propositionService = $this->getServiceLocator()->get('proposition_service');
        $propositions = $propositionService->lister(array('deletedAt' => NULL));
        $viewData['listePropositions'] = $propositions;



        return new ViewModel($viewData);
    }

    public function listerAction() {
        $jsonData = array();
        $this->init();
        $questionService = $this->getServiceLocator()->get('question_service');

        $limit = $this->params()->fromQuery('limit', 10);
        $offset = $this->params()->fromQuery('offset', 0);
        $search = $this->params()->fromQuery('search', null);
        $sondage = $this->params()->fromQuery('sondage', null);
        $criteres = array('deletedAt' => NULL);
        if (isset($sondage) && is_numeric($sondage) && $sondage > 0) {
            $criteres["sondage"] = $sondage;
        }

        if ($search) {
            $criteres["question"] = "%" . $search . "%";
            $listeQuestions = $questionService->getByLibelleLike($criteres, array("quest.libelle" => "ASC"), $limit, $offset);
            $countData = $questionService->getByLibelleLike($criteres);
        } else {
            $listeQuestions = $questionService->lister($criteres, array('libelle' => 'ASC'), $limit, $offset);
            $countData = $questionService->lister($criteres);
        }

        //$listeQuestions = $questionService->lister(array('deletedAt' => NULL), array('libelle' => 'asc'), $limit, $offset);

        $tabData = array();
        foreach ($listeQuestions as $question) {
            $tabData[] = json_decode($this->serializer->serialize($question, 'json'));
        }
        //$countData = $questionService->lister(array('deletedAt' => NULL));
        $jsonData["rows"] = $tabData;
        $jsonData["total"] = count($countData);
        return new JsonModel($jsonData);
    }

    public function ajouterAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["libelle"])) {
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                $questionService = $this->getServiceLocator()->get('question_service');
                if ($data["question"])
                    $question = $questionService->getById($data["question"]);

                $sondageService = $this->getServiceLocator()->get('sondage_service');
                $sondage = $sondageService->getById($data["sondage"]);
                if (!$sondage) {
                    throw new Exception("Impossible d'effectuer cette opération.\nSondage introuvable.");
                }

                $propositionService = $this->getServiceLocator()->get('proposition_service');
                if ($data["proposition"])
                    $proposition = $propositionService->getById($data["proposition"]);

                $typeQuestionService = $this->getServiceLocator()->get('type_question_service');
                $typeQuestion = $typeQuestionService->getById($data["typeQuestion"]);
                if (!$typeQuestion) {
                    throw new Exception("Impossible d'effectuer cette opération.\nType de question introuvable.");
                }
//                print_r($typeQuestion);
//                exit;
                //Retrouver un libelle
                $question = $questionService->getByLibelleAndTypeQuestionAndSondage($data["libelle"], $data["typeQuestion"], $data["sondage"]);
                if ($question) {
                    $question->setDeletedAt(NULL);
                } else {
                    $question = new Question();
                    $question->setCreatedAt($dateOperation);
                    $question->setLibelle($data["libelle"]);
                    $question->setProposition($proposition);
                    $question->setQuestion($question);
                    $question->setSondage($sondage);
                    $question->setTypeQuestion($typeQuestion);
                    $question->setCreatedBy($this->user);
                }

                $question->setUpdatedAt($dateOperation);
                if (isset($data["etat"]))
                    $question->setEtat(TRUE);
                else
                    $question->setEtat(FALSE);

                $dataQuestion = $questionService->editer($question);

                if ($typeQuestion->getType() == 'matrice') {
                    $tabLigne = $data["lignes"];
                    $tabColonne = $data["colonnes"];

                    if (count($tabLigne) > 0) {
                        $propositionService = $this->getServiceLocator()->get('proposition_service');
                        foreach ($tabLigne as $key => $libelle) {
                            if ($libelle) {
                                //Retrouver un libelle
                                $proposition = $propositionService->getByLibelleAndQuestion($libelle, $question->getId());
                                if ($proposition) {
                                    $proposition->setDeletedAt(NULL);
                                } else {
                                    $proposition = new Proposition();
                                    $proposition->setCreatedAt($dateOperation);
                                    $proposition->setLibelle($libelle);
                                    $proposition->setQuestion($question);
                                    $proposition->setCreatedBy($this->user);
                                }
                                $proposition->setUpdatedAt($dateOperation);
                                $proposition->setEtat(TRUE);

                                $dataProposition = $propositionService->editer($proposition);
                            }
                        }
                        $jsonData["code"] = 1;
                    }

                    if (count($tabColonne) > 0) {
                        $enteteService = $this->getServiceLocator()->get('entete_service');
                        foreach ($tabLigne as $key => $libelle) {
                            if ($libelle) {
                                //Retrouver un libelle
                                $entete = $enteteService->getByLibelleAndQuestion($libelle, $question->getId());
                                if ($entete) {
                                    $entete->setDeletedAt(NULL);
                                } else {
                                    $entete = new Entete();
                                    $entete->setCreatedAt($dateOperation);
                                    $entete->setLibelle($libelle);
                                    $entete->setQuestion($question);
                                    $entete->setCreatedBy($this->user);
                                }
                                $entete->setUpdatedAt($dateOperation);
                                $entete->setEtat(TRUE);

                                $dataEntete = $enteteService->editer($entete);
                            }
                        }
                        $jsonData["code"] = 1;
                    }
                }

                $jsonData["data"] = json_decode($this->serializer->serialize($dataQuestion, 'json'));
                return new JsonModel($jsonData);
            } catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function modifierAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        try {
            $this->init();
            $dateOperation = new DateTime('now');
            $data = $request->getPost();
            $questionService = $this->getServiceLocator()->get('question_service');
            $question = $questionService->getById($data["idQuestion"]);
            if (!$question) {
                throw new Exception("Impossible d'effectuer cette opération.\nVeuillez réessayer plus tard.");
            }
            if ($data["question"])
                $questionDep = $questionService->getById($data["question"]);

            $sondageService = $this->getServiceLocator()->get('sondage_service');
            $sondage = $sondageService->getById($data["sondage"]);
            if (!$sondage) {
                throw new Exception("Impossible d'effectuer cette opération.\nSondage introuvable.");
            }

            $propositionService = $this->getServiceLocator()->get('proposition_service');
            if ($data["proposition"])
                $proposition = $propositionService->getById($data["proposition"]);

            $typeQuestionService = $this->getServiceLocator()->get('type_question_service');
            $typeQuestion = $typeQuestionService->getById($data["typeQuestion"]);
            if (!$typeQuestion) {
                throw new Exception("Impossible d'effectuer cette opération.\nType de question introuvable.");
            }

            //Retrouver un libelle
            $question2 = $questionService->getByLibelleAndTypeQuestionAndSondage($data["libelle"], $data["typeQuestion"], $data["sondage"]);
            if ($question2 && $question->getId() != $question2->getId() && $question2->getDeletedAt() == NULL) {
                throw new Exception("Cette question existe deja.\nVeuillez changer de libelle.");
            } elseif ($question2 && $question->getId() != $question2->getId() && $question2->getDeletedAt()) {
                throw new Exception("Cette question a ete supprimee.\nVeuillez recreer a nouveau.");
            } else {
                $question->setUpdatedAt($dateOperation);
                $question->setLibelle($data["libelle"]);
                $question->setProposition($proposition);
                $question->setQuestion($questionDep);
                $question->setSondage($sondage);
                $question->setTypeQuestion($typeQuestion);
                $question->setCreatedBy($this->user);

                if (isset($data["etat"]))
                    $question->setEtat(TRUE);
                else
                    $question->setEtat(FALSE);
            }

            $dataQuestion = $questionService->editer($question);
            $jsonData["data"] = json_decode($this->serializer->serialize($dataQuestion, 'json'));
            return new JsonModel($jsonData);
        } catch (Exception $exc) {
            $jsonData["code"] = -1;
            $jsonData["data"] = NULL;
            $jsonData["msg"] = $exc->getMessage();
            return new JsonModel($jsonData);
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function supprimerAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["idQuestion"])) {
            try {
                $this->init();
                $data = $request->getPost();
                $questionService = $this->getServiceLocator()->get('question_service');
                $question = $questionService->getById($data["idQuestion"]);
                if (!$question) {
                    throw new Exception("Impossible d'effectuer cette opération.\nVeuillez réessayer plus tard.");
                }

                $dataQuestion = $questionService->supprimer($question);
                $jsonData["data"] = json_decode($this->serializer->serialize($dataQuestion, 'json'));

                return new JsonModel($jsonData);
            } catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function propositionsAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 0, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["idQuestion"])) {
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                $questionService = $this->getServiceLocator()->get('question_service');
                $propositionService = $this->getServiceLocator()->get('proposition_service');

                $question = $questionService->getById($data["idQuestion"]);
                if (!$question) {
                    throw new Exception("Impossible d'effectuer cette opération.\n question introuvable.");
                }
                $tabLibelleProposition = $data["libelle"];

                if (count($tabLibelleProposition) > 0) {
                    foreach ($tabLibelleProposition as $key => $libelle) {
                        if ($libelle) {
                            //Retrouver un libelle
                            $proposition = $propositionService->getByLibelleAndQuestion($libelle, $question->getId());
                            if ($proposition) {
                                $proposition->setDeletedAt(NULL);
                            } else {
                                $proposition = new Proposition();
                                $proposition->setCreatedAt($dateOperation);
                                $proposition->setLibelle($libelle);
                                $proposition->setQuestion($question);
                                $proposition->setCreatedBy($this->user);
                            }
                            $proposition->setUpdatedAt($dateOperation);
                            $proposition->setEtat(TRUE);

                            $dataProposition = $propositionService->editer($proposition);
                        }
                    }
                    $jsonData["code"] = 1;
                } else {
                    throw new Exception("Aucune enquete cree.");
                }
                return new JsonModel($jsonData);
            } catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function ajouterQuestionPropositionsAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["libelleQuestion"])) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
            $this->em->getConnection()->beginTransaction();
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();

                $sondageService = $this->getServiceLocator()->get('sondage_service');
                $sondage = $sondageService->getById($data["idSondage"]);
                if (!$sondage) {
                    throw new Exception("Impossible d'effectuer cette opération.\nSondage introuvable.");
                }

                $typeQuestionService = $this->getServiceLocator()->get('type_question_service');
                $typeQuestion = $typeQuestionService->getById($data["typeQuestion"]);
                if (!$typeQuestion) {
                    throw new Exception("Impossible d'effectuer cette opération.\nType de question introuvable.");
                }
                $type = $typeQuestion->getType();

                //Download image'
                $lien = NULL;
                if ($type == 'illustrationradio' || $type == 'illustrationcheckbox') {
                    $serviceUpload = new UploadFile();
                    $files = $request->getFiles()->toArray();
                    if (isset($files["lien"]) && $files["lien"]["name"]) {
                        $serviceUpload->relativePath = "enquete/image/";
                        if ($fileInfo = $serviceUpload->native($files['lien'], 'lien')) {
                            $lien = $fileInfo->getLien();
                        }
                    }
                }

                $dataQuestion = NULL;
                if ($data["libelleQuestion"]) {
                    $questionService = $this->getServiceLocator()->get('question_service');

                    $question = new Question();
                    $question->setLibelle($data["libelleQuestion"]);
                    $question->setLien($lien);
                    $question->setSondage($sondage);
                    $question->setTypeQuestion($typeQuestion);
                    $question->setCreatedAt($dateOperation);
                    $question->setUpdatedAt($dateOperation);
                    $question->setCreatedBy($this->user);
                    $dataQuestion = $questionService->editer($question);
                }

                //Recuperation des propositions de reponses
                $propositionService = $this->getServiceLocator()->get('proposition_service');

                switch ($type) {
                    case 'radio':
                    case 'checkbox':
                    case "hierachie":
                    case 'illustrationradio':
                    case 'illustrationcheckbox':
                        $tabLibelleProposition = $data["libelleProposition"] ? $data["libelleProposition"] : NULL;
                        if (count($tabLibelleProposition) > 0 && $dataQuestion) {
                            foreach ($tabLibelleProposition as $key => $libelle) {
                                if ($libelle) {
                                    $proposition = new Proposition();
                                    $proposition->setLibelle($libelle);
                                    $proposition->setQuestion($dataQuestion);
                                    $proposition->setCreatedBy($this->user);
                                    $proposition->setCreatedAt($dateOperation);
                                    $proposition->setUpdatedAt($dateOperation);
                                    $proposition->setEtat(TRUE);
                                    $propositionService->editer($proposition);
                                }
                            }
                        }
                        break;

                    case 'imageradio':
                    case 'imagecheckbox':
                        $serviceUpload = new UploadFile();
                        $files = $request->getFiles()->toArray();
                        if (isset($files["tabLien"])) {
                            $serviceUpload->relativePath = "enquete/image/";
                            foreach ($files["tabLien"] as $key => $file) {
                                if ($fileInfo = $serviceUpload->native($files['tabLien'][$key], 'lien')) {
                                    $lien = $fileInfo->getLien();
                                    $proposition = new Proposition();
                                    $proposition->setLibelle($lien);
                                    $proposition->setQuestion($dataQuestion);
                                    $proposition->setCreatedBy($this->user);
                                    $proposition->setCreatedAt($dateOperation);
                                    $proposition->setUpdatedAt($dateOperation);
                                    $proposition->setEtat(TRUE);
                                    $propositionService->editer($proposition);
                                }
                            }
                        }

                        break;

                    case 'matrice':

                        $typePropositionService = $this->getServiceLocator()->get('type_proposition_service');
                        $typeProposition = $typePropositionService->getById($data["typeProposition"]);
                        if (!$typeProposition) {
                            return new JsonModel(array("code" => 0, "msg" => "Type Proposition introuvable", "data" => NULL));
                        }

                        $tabLibellePropositionLigne = $data["libelleLigne"] ? $data["libelleLigne"] : NULL;
                        $tabLibellePropositionColonne = $data["libelleColone"] ? $data["libelleColone"] : NULL;

                        if (count($tabLibellePropositionLigne) > 0 && $dataQuestion) {
                            foreach ($tabLibellePropositionLigne as $key => $libelleLigne) {
                                if ($libelleLigne) {
                                    $proposition = new Proposition();
                                    $proposition->setLibelle($libelleLigne);
                                    $proposition->setQuestion($dataQuestion);
                                    $proposition->setCreatedBy($this->user);
                                    $proposition->setCreatedAt($dateOperation);
                                    $proposition->setUpdatedAt($dateOperation);
                                    $proposition->setEtat(TRUE);
                                    $propositionService->editer($proposition);
                                }
                            }
                        }

                        if (count($tabLibellePropositionColonne) > 0 && $dataQuestion) {
                            foreach ($tabLibellePropositionColonne as $key => $libelleColonne) {
                                if ($libelleColonne) {
                                    $proposition = new Proposition();
                                    $proposition->setLibelle($libelleColonne);
                                    $proposition->setQuestion($dataQuestion);
                                    $proposition->setCreatedBy($this->user);
                                    $proposition->setCreatedAt($dateOperation);
                                    $proposition->setUpdatedAt($dateOperation);
                                    $proposition->setEtat(TRUE);
                                    $proposition->setTypeProposition($typeProposition);
                                    $propositionService->editer($proposition);
                                }
                            }
                        }
                        break;

                    case 'evaluationgraduee':
                        $typePropositionService = $this->getServiceLocator()->get('type_proposition_service');
//                        $typeProposition = $typePropositionService->getById($data["typeProposition"]);
//                        if (!$typeProposition) {
//                            return new JsonModel(array("code" => 0, "msg" => "Type Proposition introuvable", "data" => NULL));
//                        }

                        //Valeur de l'échelle
                        $tabLibelleProposition = $data["libelleProposition"] ? $data["libelleProposition"] : [];
                        //Valeur minimale de la question hierarchique
                        $tabLibellePropositionLigne = $data["libelleLigne"] ? $data["libelleLigne"] : [];
                        //Valeur maximale de la question hierarchique
                        $tabLibellePropositionColonne = $data["libelleColone"] ? $data["libelleColone"] : [];

                        if (count($tabLibelleProposition) > 0 && count($tabLibellePropositionLigne) > 0 && count($tabLibellePropositionColonne) > 0 && $dataQuestion) {
                            if(isset($tabLibelleProposition[1], $tabLibelleProposition[2])){
                                $libelleMinMax = $tabLibelleProposition[1] . ';;;' . $tabLibelleProposition[2];
                            }
                            else{
                                $libelleMinMax = $dataQuestion->getLibelle();
                            }
                            $proposition = new Proposition();
                            $proposition->setLibelle($libelleMinMax);
                            $proposition->setQuestion($dataQuestion);
                            $proposition->setStep($tabLibelleProposition[0]);
                            $proposition->setMin($tabLibellePropositionLigne[0]);
                            $proposition->setMax($tabLibellePropositionColonne[0]);
                            $proposition->setChoixmultiple(isset($data["choixMultiple"]) ? TRUE : FALSE);
                            $proposition->setCreatedBy($this->user);
                            $proposition->setCreatedAt($dateOperation);
                            $proposition->setUpdatedAt($dateOperation);
                            $proposition->setEtat(TRUE);
                            $propositionService->editer($proposition);
                        }
                        break;

                    default:
                        break;
                }

                $jsonData["data"] = json_decode($this->serializer->serialize($dataQuestion, 'json'));
                $this->em->getConnection()->commit();
                return new JsonModel($jsonData);
            } catch (Exception $exc) {
                $this->em->getConnection()->rollback();
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

}
