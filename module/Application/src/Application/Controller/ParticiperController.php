<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 10 avr. 2017 A  04:59:34
  ##############  File     : ParticiperController.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Controller;

use DateTime;
use Exception;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class ParticiperController extends AbstractActionController {

    private $serializer;
    private $user = null;

    private function init() {
        $this->serializer = $this->getServiceLocator()->get('jms_serializer.serializer');
        $this->user = $this->identity();
    }

    public function indexAction() {
        $this->layout()->setVariable("btnModalAjout", FALSE);
        $this->layout()->setVariable("menuPrincipal", "Enquete");
        $this->layout()->setVariable("icone", "fa fa-search");
        $this->layout()->setVariable("lienPrincipal", "app/sondage");
        $this->layout()->setVariable("menuInterne", "sondage");
        $this->layout()->setVariable("titleControlleur", "Inscription des participants");
        $this->layout()->setVariable("sondageActive", "active");

        $this->init();
        $modelData = array();
        $idSondage = $this->params()->fromRoute('id');
        try {
            if ($idSondage > 0) {
                //sondage
                $sondageService = $this->getServiceLocator()->get('sondage_service');
                $sondage = $sondageService->getById($idSondage);
                if (!$sondage) {
                    throw new Exception("Impossible d'effectuer cette opération.\n enquete introuvable.");
                }
                $modelData["sondage"] = $sondage;

                //Lister les participant en general
                $participantService = $this->getServiceLocator()->get('participant_service');
                //$listeParticipants = $participantService->lister($criteres, array('fullName' => 'ASC'), $limit, $offset);
                $modelData["participants"] = $participantService->lister(array('deletedAt' => NULL), array('fullName' => 'ASC'));
                //Lister les participants du sondage selectionne
                $participerService = $this->getServiceLocator()->get('participer_service');
                $modelData["participantsSondage"] = $participerService->lister(array('deletedAt' => NULL, 'sondage' => $idSondage));
            }
        }
        catch (Exception $exc) {

            $modelData["erreur"] = $exc->getMessage();
            $modelData["data"] = null;
            $modelData["success"] = 0;
        }

        return new ViewModel($modelData);
    }

    public function listerAction() {
        $jsonData = array();
        $this->init();
        $participerService = $this->getServiceLocator()->get('participer_service');

        $limit = $this->params()->fromQuery('limit', 10);
        $offset = $this->params()->fromQuery('offset', 0);
        $participant = $this->params()->fromQuery('participant', 0);
        $utilisateurID = $this->params()->fromQuery('utilisateur', 0);
        $search = $this->params()->fromQuery('search');
        $criteres = array('deletedAt' => NULL);

        if (isset($participant) && is_numeric($participant) && $participant > 0) {
            $criteres["participant"] = $participant;
        }
        elseif (isset($utilisateurID) && is_numeric($utilisateurID) && $utilisateurID > 0) {
            $utilisateurService = $this->getServiceLocator()->get('utilisateur_service');
            $utilisateur = $utilisateurService->getById($utilisateurID);
            if (!$utilisateur || $utilisateur->getParticipant() == NULL) {
                $jsonData = [
                    'code' => 0,
                    'msg' => 'Utilisateur introuvable',
                    'data' => NULL
                ];
                return new JsonModel($jsonData);
            }
            else {
                $criteres["participant"] = $utilisateur->getParticipant()->getId();
            }
        }

        $listeParticiper = $participerService->lister($criteres, array(), $limit, $offset);

        $tabData = array();
        foreach ($listeParticiper as $participer) {
            $tabData[] = json_decode($this->serializer->serialize($participer, 'json'));
        }
        $countData = $participerService->lister($criteres);
        $jsonData["rows"] = $tabData;
        $jsonData["total"] = count($countData);
        return new JsonModel($jsonData);
    }

    public function ajouterAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 0, "msg" => "Participant(s) ajouté(s) a l'enquete avec succès.");
        if ($request->isPost() && isset($request->getPost()["sondage"])) {
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                $idSondage = $data["sondage"];
                $sondageService = $this->getServiceLocator()->get('sondage_service');
                $sondage = $sondageService->getById($idSondage);
                if (!$sondage) {
                    throw new Exception("Impossible d'effectuer cette opération.\nEnquete introuvable.");
                }
                //Tableau des inscriptions a une enquete (sondage)
                $tabParticiperSondage = $data["participerSondage"];

                if (count($tabParticiperSondage) > 0) {
                    $participerService = $this->getServiceLocator()->get('participer_service');
                    $participantService = $this->getServiceLocator()->get('participant_service');
                    //Lister les participants dans participer en general
                    $participantsInscrit = $participerService->lister(array('deletedAt' => NULL, 'sondage' => $sondage->getId()));
                    //Les supprimer dans la base
                    foreach ($participantsInscrit as $participerId) {
                        $participer = $participerService->getById($participerId);
                        if ($participer) {
                            $participerService->supprimer($participer);
                        }
                    }


                    foreach ($tabParticiperSondage as $idParticipant) {
                        $participerSondage = $participerService->getOneByCriteria(array('participant' => $idParticipant, 'sondage' => $sondage->getId()));

                        if (!$participerSondage) {
                            $participerSondage = new \Application\Model\Entity\Participer();
                            $participerSondage->setCreatedAt($dateOperation);
                            $participerSondage->setUpdatedAt($dateOperation);
                            $participerSondage->setEtat(TRUE);
                            $participerSondage->setLibelle(NULL);
                            $participerSondage->setParticipant($participantService->getById($idParticipant));
                            $participerSondage->setSondage($sondage);
                            $participerSondage->setCreatedBy($this->user);
                        }
                        else {
                            $participerSondage->setDeletedAt(NULL);
                        }
                        $participerService->editer($participerSondage);
                    }

                    $jsonData["code"] = 1;
                }
                else {
                    throw new Exception("Aucun participant inscrit pour cette enquette.");
                }
                return new JsonModel($jsonData);
            }
            catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function sondageAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 0, "msg" => "Je participe aux enquetes.");
        if ($request->isPost()) {
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();

                $utilisateurID = $data["utilisateurID"];
                if ($utilisateurID) {
                    $utilisateurService = $this->getServiceLocator()->get('utilisateur_service');
                    $utilisateur = $utilisateurService->getById($data["utilisateurID"]);
                    if (!$utilisateur || $utilisateur->getParticipant() == NULL) {
                        return array("code" => 0, "msg" => "Utilisateur introuvable");
                    }
                    else {
                        $idParticipant = $utilisateur->getParticipant()->getId();
                    }

                    $participantService = $this->getServiceLocator()->get('participant_service');
                    $participant = $participantService->getById($idParticipant);
                    if (!$participant) {
                        return array("code" => 0, "msg" => "Utilisateur introuvable");
                    }
                    
                    //Tableau des inscriptions a une enquete (sondage)
                    $tabParticiperSondage = $data["sondages"];

                    if (count($tabParticiperSondage) > 0) {
                        $participerService = $this->getServiceLocator()->get('participer_service');
                        $sondageService = $this->getServiceLocator()->get('sondage_service');
                        //Lister les participants dans participer en general
                        $participantsInscrit = $participerService->lister(array('deletedAt' => NULL, 'participant' => $participant->getId()));
                        //Les supprimer dans la base
                        foreach ($participantsInscrit as $participerId) {
                            $participer = $participerService->getById($participerId);
                            if ($participer) {
                                $participerService->supprimer($participer);
                            }
                        }

                        foreach ($tabParticiperSondage as $idSonage) {
                            $participerSondage = $participerService->getOneByCriteria(array('participant' => $idParticipant, 'sondage' => $idSonage));

                            if (!$participerSondage) {
                                $participerSondage = new \Application\Model\Entity\Participer();
                                $participerSondage->setCreatedAt($dateOperation);
                                $participerSondage->setUpdatedAt($dateOperation);
                                $participerSondage->setEtat(TRUE);
                                $participerSondage->setLibelle(NULL);
                                $participerSondage->setParticipant($participant);
                                $participerSondage->setSondage($sondageService->getById($idSonage));
                                $participerSondage->setCreatedBy($utilisateur);
                            }
                            else {
                                $participerSondage->setDeletedAt(NULL);
                            }
                            $participerService->editer($participerSondage);
                        }

                        $jsonData["code"] = 1;
                    }
                    else {
                        throw new Exception("Aucun participant inscrit pour cette enquette.");
                    }
                }

                return new JsonModel($jsonData);
            }
            catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function modifierAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        try {
            $this->init();
            $dateOperation = new DateTime('now');
            $data = $request->getPost();
            $participerService = $this->getServiceLocator()->get('participer_service');
            $participer = $participerService->getById($data["idCommissariat"]);
            $participantService = $this->getServiceLocator()->get('participant_service');
            if (!$participer) {
                throw new Exception("Impossible d'effectuer cette opération.\nVeuillez réessayer plus tard.");
            }
            $participer->setCreatedAt($dateOperation);
            $participer->setUpdatedAt($dateOperation);
            $participer->setCommune($participantService->getById($data["participant"]));
            $participer->setLibelle($data["libelle"]);
            $participer->setAdresse($data["adresse"]);
            $participer->setTelephone($data["telephone"]);
            $participer->setCreatedBy($this->user);

            $dataCommissariat = $participerService->editer($participer);
            $jsonData["data"] = json_decode($this->serializer->serialize($dataCommissariat, 'json'));
            return new JsonModel($jsonData);
        }
        catch (Exception $exc) {
            $jsonData["code"] = -1;
            $jsonData["data"] = NULL;
            $jsonData["msg"] = $exc->getMessage();
            return new JsonModel($jsonData);
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function supprimerAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["idCommissariat"])) {
            try {
                $this->init();
                $data = $request->getPost();
                $participerService = $this->getServiceLocator()->get('participer_service');
                $participer = $participerService->getById($data["idCommissariat"]);
                if (!$participer) {
                    throw new Exception("Impossible d'effectuer cette opération.\nVeuillez réessayer plus tard.");
                }

                $dataCommissariat = $participerService->supprimer($participer);
                $jsonData["data"] = json_decode($this->serializer->serialize($dataCommissariat, 'json'));

                return new JsonModel($jsonData);
            }
            catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

}
