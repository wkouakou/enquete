<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 10 avr. 2017 A  04:59:01
  ##############  File     : GroupeController.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Controller;

use Application\Model\Entity\Groupe;
use Application\Model\Entity\Proposition;
use Application\Model\Entity\Question;
use Application\Model\Entity\Sondage;
use DateTime;
use Exception;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class GroupeController extends AbstractActionController {

    private $serializer;
    private $user = null;

    private function init() {
        $this->serializer = $this->getServiceLocator()->get('jms_serializer.serializer');
        $this->user = $this->identity();
    }

    public function indexAction() {
        $this->layout()->setVariable("btnModalAjout", true);
        $this->layout()->setVariable("menuPrincipal", "Groupe");
        $this->layout()->setVariable("icone", "fa fa-object-group");
        $this->layout()->setVariable("lienPrincipal", "app/groupe");
        $this->layout()->setVariable("menuInterne", "groupe");
        $this->layout()->setVariable("titleControlleur", "Groupe");
        //$this->layout()->setVariable("parametreActive", "active");
        $this->layout()->setVariable("groupeActive", "active");

        $viewData = array();
        return new ViewModel($viewData);
    }

    public function listerAction() {
        $jsonData = array();
        $this->init();
        $groupeService = $this->getServiceLocator()->get('groupe_service');

        $limit = $this->params()->fromQuery('limit', 10);
        $offset = $this->params()->fromQuery('offset', 0);
        $search = $this->params()->fromQuery('search', null);
        $criteres = array('deletedAt' => NULL);

        if ($search) {
            $criteres["question"] = "%" . $search . "%";
            $listeGroupes = $groupeService->getByLibelleLike($criteres, array("quest.id" => "DESC"), $limit, $offset);
            $countData = $groupeService->getByLibelleLike($criteres);
        } else {
            $listeGroupes = $groupeService->lister($criteres, array('id' => 'DESC'), $limit, $offset);
            $countData = $groupeService->lister($criteres);
        }

        $sondageService = $this->getServiceLocator()->get('sondage_service');
        $tabData = [];
        $nbSondage = 0;
        foreach ($listeGroupes as $groupe) {
            $nbSondage = count($sondageService->lister(["groupe" => $groupe->getId(), "deletedAt" => NULL]));
            $groupe->setSondages($nbSondage);
            $tabData[] = json_decode($this->serializer->serialize($groupe, 'json'));
        }
        $countData = $groupeService->lister(array('deletedAt' => NULL));
        $jsonData["rows"] = $tabData;
        $jsonData["total"] = count($countData);
        return new JsonModel($jsonData);
    }

    public function ajouterAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["libelle"])) {
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                $groupeService = $this->getServiceLocator()->get('groupe_service');

                $tabLibelle = $data["libelle"];
                $tabEtat = $data["etat"];
                if (count($tabLibelle) > 0) {
                    foreach ($tabLibelle as $key => $libelle) {
                        if ($libelle) {
                            //Retrouver un libelle
                            $groupe = $groupeService->getByLibelle($libelle);
                            if ($groupe) {
                                $groupe->setDeletedAt(NULL);
                            } else {
                                $groupe = new Groupe();
                                $groupe->setCreatedAt($dateOperation);
                                $groupe->setLibelle($libelle);
                                $groupe->setCreatedBy($this->user);
                            }
                            $groupe->setUpdatedAt($dateOperation);
                            if (isset($tabEtat[$key]))
                                $groupe->setEtat(TRUE);
                            else
                                $groupe->setEtat(FALSE);

                            $dataGroupe = $groupeService->editer($groupe);
                        }
                    }
                    $jsonData["code"] = 1;
                }
                else {
                    throw new Exception("Aucune enquete cree.");
                }
                //$jsonData["data"] = json_decode($this->serializer->serialize($dataGroupe, 'json'));
                return new JsonModel($jsonData);
            } catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function modifierAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        try {
            $this->init();
            $dateOperation = new DateTime('now');
            $data = $request->getPost();
            $groupeService = $this->getServiceLocator()->get('groupe_service');
            $groupe = $groupeService->getById($data["idGroupe"]);
            if (!$groupe) {
                throw new Exception("Impossible d'effectuer cette opération.\ngroupe introuvable.");
            }

            //Retrouver un libelle
            $groupe2 = $groupeService->getByLibelle($data["libelle"]);
            if ($groupe2 && $groupe->getId() != $groupe2->getId() && $groupe2->getDeletedAt() == NULL) {
                throw new Exception("Ce groupe existe deja.\nVeuillez changer de libelle.");
            } elseif ($groupe2 && $groupe->getId() != $groupe2->getId() && $groupe2->getDeletedAt()) {
                throw new Exception("Ce groupe a ete supprime.\nVeuillez recreer a nouveau.");
            } else {

                $groupe->setUpdatedAt($dateOperation);
                $groupe->setLibelle($data["libelle"]);
                if (isset($data["etat"]))
                    $groupe->setEtat(TRUE);
                else
                    $groupe->setEtat(FALSE);
                $groupe->setCreatedBy($this->user);
            }

            $dataGroupe = $groupeService->editer($groupe);
            $jsonData["data"] = json_decode($this->serializer->serialize($dataGroupe, 'json'));
            return new JsonModel($jsonData);
        } catch (Exception $exc) {
            $jsonData["code"] = -1;
            $jsonData["data"] = NULL;
            $jsonData["msg"] = $exc->getMessage();
            return new JsonModel($jsonData);
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function supprimerAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["idGroupe"])) {
            try {
                $this->init();
                $data = $request->getPost();
                $groupeService = $this->getServiceLocator()->get('groupe_service');
                $groupe = $groupeService->getById($data["idGroupe"]);
                if (!$groupe) {
                    throw new Exception("Impossible d'effectuer cette opération.\nVeuillez réessayer plus tard.");
                }

                $dataGroupe = $groupeService->supprimer($groupe);
                $jsonData["data"] = json_decode($this->serializer->serialize($dataGroupe, 'json'));

                return new JsonModel($jsonData);
            } catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function sondagesAction() {
        $request = $this->getRequest();
        $jsonData = array("code" => 0, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["idGroupe"])) {
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                $groupeService = $this->getServiceLocator()->get('groupe_service');
                $sondageService = $this->getServiceLocator()->get('sondage_service');
                $groupe = $groupeService->getById($data["idGroupe"]);
                if (!$groupe) {
                    throw new Exception("Impossible d'effectuer cette opération.\ngroupe introuvable.");
                }
                $tabLibelleSondage = $data["libelle"];
                if (count($tabLibelleSondage) > 0) {
                    foreach ($tabLibelleSondage as $libelle) {
                        if ($libelle) {
                            //Retrouver un libelle
                            $sondage = $sondageService->getByLibelleAndGroupe($libelle, $groupe->getId());
                            if ($sondage) {
                                $sondage->setDeletedAt(NULL);
                                $sondage->setUpdatedAt($dateOperation);
                            } else {
                                $sondage = new Sondage();
                                $sondage->setCreatedAt($dateOperation);
                                $sondage->setLibelle($libelle);
                                $sondage->setCreatedBy($this->user);
                                $sondage->setGroupe($groupe);
                            }
                            $sondage->setEtat(TRUE);
                            $dataSondage = $sondageService->editer($sondage);
                        }
                    }
                    $jsonData["code"] = 1;
                } else {
                    throw new Exception("Aucune enquete cree.");
                }
                return new JsonModel($jsonData);
            } catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function questionnaireAction() {
        $this->layout()->setVariable("btnModalAjout", false);
        $this->layout()->setVariable("menuPrincipal", "Enquete");
        $this->layout()->setVariable("icone", "fa fa-search");
        $this->layout()->setVariable("lienPrincipal", "#");
        $this->layout()->setVariable("menuInterne", "sondage");
        $this->layout()->setVariable("titleControlleur", "Creation de questionnaire");
        //$this->layout()->setVariable("parametreActive", "active");
        $this->layout()->setVariable("questActive", "active");


        $viewData = array();
        $this->init();
        $typeQuestionService = $this->getServiceLocator()->get('type_question_service');
        $viewData["typeQuestions"] = $typeQuestionService->lister(array('deletedAt' => NULL), array('libelle' => 'ASC'));

        return new ViewModel($viewData);
    }

    public function ajouterQuestionnaireAction() {

        $request = $this->getRequest();
        $jsonData = array("code" => 0, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost()) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
            $this->em->getConnection()->beginTransaction();
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                $groupeService = $this->getServiceLocator()->get('groupe_service');
                $sondageService = $this->getServiceLocator()->get('sondage_service');
                $questionService = $this->getServiceLocator()->get('question_service');
                $propositionService = $this->getServiceLocator()->get('proposition_service');
                $typeQuestionService = $this->getServiceLocator()->get('type_question_service');

                //Groupe 
                $groupeId = $data["groupeID"];
                if ($groupeId && is_numeric($groupeId) && $groupeId > 0) {
                    $dataGroupe = $groupeService->getById($groupeId);
                    if (!$dataGroupe) {
                        throw new Exception("Impossible d'effectuer cette opération.\ngroupe introuvable.");
                    }
                } else {
                    $libelleGroupe = $data["groupeLibelle"];
                    if ($libelleGroupe) {
                        //Retrouver un libelle
                        $groupe = $groupeService->getByLibelle($libelleGroupe);
                        if ($groupe) {
                            $groupe->setDeletedAt(NULL);
                        } else {
                            $groupe = new Groupe();
                            $groupe->setCreatedAt($dateOperation);
                            $groupe->setLibelle($libelleGroupe);
                            $groupe->setCreatedBy($this->user);
                        }
                        $groupe->setUpdatedAt($dateOperation);
                        $groupe->setEtat(TRUE);

                        $dataGroupe = $groupeService->editer($groupe);
                    } else {
                        throw new Exception("Impossible d'effectuer cette opération.\n groupe non saisi.");
                    }
                }

                //Enquete 
                $sondageLibelle = $data["sondageLibelle"];
                if ($sondageLibelle) {
                    $sondage = $sondageService->getByLibelleAndGroupe($sondageLibelle, $dataGroupe->getId());
                    if ($sondage) {
                        $sondage->setDeletedAt(NULL);
                    } else {
                        $sondage = new Sondage();
                        $sondage->setCreatedAt($dateOperation);
                        $sondage->setLibelle($sondageLibelle);
                        $sondage->setCreatedBy($this->user);
                        $sondage->setGroupe($dataGroupe);
                    }

                    $sondage->setUpdatedAt($dateOperation);
                    $sondage->setEtat(TRUE);

                    $dataSondage = $sondageService->editer($sondage);
                } else {
                    throw new Exception("Impossible d'effectuer cette opération.\n enquete non saisie.");
                }

                //Question question
                $tabQuestion = $data["question"];
                $tabTypeQuestion = $data["typeQuestion"];
                $tabValeurHierachie = $data["valeurHierachie"];
                $nombreQuestion = count($tabQuestion);

                if ($nombreQuestion > 0) {
                    $ordreQuestion = 1;
                    foreach ($tabQuestion as $key => $questionLibelle) {
                        $question = $questionService->getByLibelleAndTypeQuestionAndSondage($questionLibelle, $tabTypeQuestion[$key], $dataSondage->getId());
                        if ($question) {
                            $question->setDeletedAt(NULL);
                        } else {
                            $question = new Question();
                            $question->setCreatedAt($dateOperation);
                            $question->setLibelle($questionLibelle);
                            //$question->setProposition($proposition);
                            //$question->setQuestion($question);
                            if ($tabValeurHierachie[$key])
                                $question->setValeurHierachie($tabValeurHierachie[$key]);
                            $question->setSondage($dataSondage);
                            $question->setTypeQuestion($typeQuestionService->getById($tabTypeQuestion[$key]));
                            $question->setCreatedBy($this->user);
                        }

                        $question->setUpdatedAt($dateOperation);
                        $question->setEtat(TRUE);
                        $dataQuestion = $questionService->editer($question);

                        //Proposition de reponse
                        $tabProposition = $data["propositionReponse_" . $ordreQuestion];
                        if (count($tabProposition) > 0) {
                            foreach ($tabProposition as $propositionLibelle) {

                                $proposition = $propositionService->getByLibelleAndQuestion($propositionLibelle, $dataQuestion->getId());
                                if ($proposition) {
                                    $proposition->setDeletedAt(NULL);
                                } else {
                                    $proposition = new Proposition();
                                    $proposition->setCreatedAt($dateOperation);
                                    $proposition->setLibelle($propositionLibelle);
                                    $proposition->setQuestion($dataQuestion);
                                    $proposition->setCreatedBy($this->user);
                                }
                                $proposition->setUpdatedAt($dateOperation);
                                $proposition->setEtat(TRUE);
                                $propositionService->editer($proposition);
                            }
                        }

                        $ordreQuestion = $ordreQuestion + 1;
                    }
                } else {
                    throw new Exception("Aucune question cree.");
                }
                $jsonData["code"] = 1;
                $jsonData["data"] = json_decode($this->serializer->serialize($dataSondage, 'json'));
                $this->em->getConnection()->commit();
                return new JsonModel($jsonData);
            } catch (Exception $exc) {
                $this->em->getConnection()->rollback();
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

    public function utilisateursAction() { //Liste
        $jsonData = array();
        $this->init();
        $groupeUtilisateurService = $this->getServiceLocator()->get('groupe_utilisateur_service');

        $limit = $this->params()->fromQuery('limit', 10);
        $offset = $this->params()->fromQuery('offset', 0);
        $groupe = $this->params()->fromQuery('groupe', NULL);
        $criteres = array('deletedAt' => NULL);

        if ($groupe) {
            $criteres["groupe"] = $groupe;
        }
        
        $listeGroupesUtilisateur = $groupeUtilisateurService->lister($criteres, array('id' => 'DESC'), $limit, $offset);

        $tabData = [];
        foreach ($listeGroupesUtilisateur as $groupeUtilisateur) {
            $tabData[] = json_decode($this->serializer->serialize($groupeUtilisateur->getUtilisateur(), 'json'));
        }
        $countData = $groupeUtilisateurService->lister($criteres);
        $jsonData["rows"] = $tabData;
        $jsonData["total"] = count($countData);
        return new JsonModel($jsonData);
    }

    public function utilisateurAction() { //Ajout, modification
        $request = $this->getRequest();
        $jsonData = array("code" => 1, "msg" => "Opération effectuée avec succès.");
        if ($request->isPost() && isset($request->getPost()["utilisateur"])) {
            try {
                $this->init();
                $dateOperation = new DateTime('now');
                $data = $request->getPost();
                $utilisateurService = $this->getServiceLocator()->get('utilisateur_service');
                $groupeService = $this->getServiceLocator()->get('groupe_service');
                $groupeUtilisateurService = $this->getServiceLocator()->get('groupe_utilisateur_service');

                $utilisateur = $utilisateurService->getById($data["utilisateur"]);
                $groupe = $groupeService->getById($data["groupe"]);

                if (!$utilisateur || !$groupe) {
                    throw new Exception("Impossible d'ajouter l'utilisateur.");
                }

                $groupeUtilisateur = new \Application\Model\Entity\GroupeUtilisateur();
                $groupeUtilisateur->setCreatedAt($dateOperation);
                $groupeUtilisateur->setUpdatedAt($dateOperation);
                $groupeUtilisateur->setUtilisateur($utilisateur);
                $groupeUtilisateur->setGroupe($groupe);
                $groupeUtilisateur->setCreatedBy($this->user);
                $dataGroupeUtilisateur = $groupeUtilisateurService->editer($groupeUtilisateur);

                $jsonData["data"] = json_decode($this->serializer->serialize($dataGroupeUtilisateur, 'json'));
                return new JsonModel($jsonData);
            } catch (Exception $exc) {
                $jsonData["code"] = -1;
                $jsonData["data"] = NULL;
                $jsonData["msg"] = $exc->getMessage();
                return new JsonModel($jsonData);
            }
        }
        return new JsonModel(array("code" => 0, "msg" => "Saisie invalide", "data" => NULL));
    }

}
