<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 9 avr. 2017 A  19:28:16
  ##############  File     : ParticiperService.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Services;

interface ParticiperService {

    //put your code here 

    public function getOneByCriteria($criteres = array());

    public function getCorrespondanceBy($sexe = null, $sondage = null, $offset = null, $limit = null);
}
