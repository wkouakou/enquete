<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 9 avr. 2017 A  19:31:07
  ##############  File     : QuestionService.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Services;

interface QuestionService {
    
    public function getByLibelleLike($criteres = array(), $orderBy = array(), $limit = null, $offset = null);
    
    function getByLibelleAndTypeQuestionAndSondage($libelle, $typeQuestionId, $sondageID);
}
