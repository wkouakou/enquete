<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Services;

use Zend\ServiceManager\ServiceLocatorAwareInterface;

/**
 *
 * @author wilfried
 */
interface BaseService extends ServiceLocatorAwareInterface {

    function editer($object);
    
    function getById($objectId);
    
    function lister($criteria = array(), $orderBy = null, $limit = null, $offset = null);
    
    function supprimer($object);
}
