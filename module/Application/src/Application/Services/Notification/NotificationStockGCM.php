<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Stock\Services\Notification;

/**
 * Description of NotificationStockGCM
 *
 * @author Wilfried
 */
use Exception;
use Thread;
use Zend\EventManager\ListenerAggregateInterface;

class NotificationStockGCM extends Thread implements ListenerAggregateInterface {

    private $url = 'https://android.googleapis.com/gcm/send'; //https://gcm-http.googleapis.com/gcm/send
    //private $url = 'https://gcm-http.googleapis.com/gcm/send'; //
    public $placeServerApiKey = "AIzaSyBi1LgxHkwzqr-GjCME77967EhSSl4hrRs"; //Place
    public $partnerServerApiKey = "AIzaSyAgw7djeUeu-I7ORCNQrX9tldHgR0NpAho"; //Collar partner
    public $serverApiKey = "AIzaSyAgw7djeUeu-I7ORCNQrX9tldHgR0NpAho";
    public $collapseKey = "COMMANDE_ATTENTE";
    private $devices = array();
    private $message;
    private $data = false;
    private $sendDateTime = null;

    public function __construct() {
        
    }

    public function run(){
        ob_start();
        try {
            $rep = $this->sendNotif();
            var_dump($rep);
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
        $content = ob_get_clean();
        file_put_contents("gcm_statut.txt", $content . "\n", FILE_APPEND);
    }

    public function setServerApiKey($serverApiKey) {
        $this->serverApiKey = $serverApiKey;
        return $this;
    }

    public function setMessage($message) {
        $this->message = $message;
        return $this;
    }

    public function setData($data) {
        $this->data = $data;
        return $this;
    }

    public function setSendDateTime($sendDateTime) {
        $this->sendDateTime = $sendDateTime;
        return $this;
    }

    /*
      Set the devices to send to
      @param $deviceIds array of device tokens to send to
     */

    public function setCollapseKey($collapseKey) {
        $this->collapseKey = $collapseKey;
    }

    public function setDevices($deviceIds) {

        if (is_array($deviceIds)) {
            $this->devices = $deviceIds;
        } else {
            $this->devices = array($deviceIds);
        }
    }

    protected function initField() {
        if (!is_array($this->devices) || count($this->devices) == 0) {
            $this->error("No devices set");
        }

        if (strlen($this->serverApiKey) < 8) {
            $this->error("Server API Key not set");
        }

        $fields = array(
            'registration_ids' => $this->devices,
            'collapse_key' => $this->collapseKey,
            'data' => array("message" => $this->message),
        );
        if (is_array($this->data)) {
            foreach ($this->data as $key => $value) {
                $fields['data'][$key] = $value;
            }
        }
        return $fields;
    }

    /*
      Send the message to the device
      @param $message The message to send
      @param $data Array of data to accompany the message
     */

    public function sendNotif() {
        $headers = array(
            'Authorization:key=' . $this->serverApiKey,
            'Content-Type:application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $this->url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->initField()));

        // Avoids problem with https certificate
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // Execute post
        $result = curl_exec($ch);

        // Close connection
        curl_close($ch);

        return $result;
    }

    public function error($msg) {
        ob_start();
        echo "Android send notification failed with error:";
        echo "\t" . $msg;
        $content = ob_get_clean();
        file_put_contents("gmc_error.txt", $content . "\n", FILE_APPEND);
    }

    public function attach(\Zend\EventManager\EventManagerInterface $events) {
        $sharedEvents = $events->getSharedManager();
        $this->listeners[] = $sharedEvents->attach('Application\Events\EventNotification', 'NotificationStockGCM', array($this, 'onSendNotification'), 100);
    }

    public function detach(\Zend\EventManager\EventManagerInterface $events = null) {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

    public function onSendNotification($e) {
        $params = $e->getParams();
        $notif = new NotificationGCM();
        if($params["plateforme"] == "place"){ //Plateforme : place = client, pro = partenaire
            $notif->setServerApiKey($notif->placeServerApiKey);
        }else{
            $notif->setServerApiKey($notif->partnerServerApiKey);
        }
        $notif->setCollapseKey($params["collapseKey"]);
        $notif->setDevices($params["devices"]);
        $notif->setMessage($params["message"]);
        $notif->setData($params["data"]);
        $notif->start();
        //ob_start();
        //echo $params["plateforme"] . " : " . $notif->collapseKey." : " . $notif->serverApiKey;
        //$content = ob_get_clean();
        //file_put_contents("gcm_info.txt", $content . "\n", FILE_APPEND);
    }
}
