<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Stock\Services\Notification;

/**
 * Description of NotificationStockSMS
 *
 * @author Wilfried
 */

use DateTime;
use Exception;
use Thread;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;

class NotificationStockSMS extends Thread implements ListenerAggregateInterface {

    private $postUrl = "http://intapi.24X7SMS.com/api/sendsms/xml";
    private $urlSendSms = 'http://www.ait-ci.com/sms/application/api/sending';
    private $urlAccountInfo = 'http://www.ait-ci.com/sms/application/api/compte';
    private $login = 'justinkouadio@gmail.com';
    private $password = 'irVjbN';
    private $senderAdress = 'COLLAR PRO';
    private $address;
    private $message;
    private $sendDateTime = null;

    public function run() {
        ob_start();
        try {
            $this->sendSms();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

        $content = ob_get_clean();
        file_put_contents("sms_statut.txt", $content . "\n", FILE_APPEND);
    }

    private function initField() {
        // XML-formatted data
        $xmlString = "<SMS>";
        $xmlString .= "<authentification>";
        $xmlString .= "<username>" . $this->login . "</username>";
        $xmlString .= "<password>" . $this->password . "</password>";
        $xmlString .= "</authentification>";
        $xmlString .= "<message>";
        $xmlString .= "<sender>" . $this->senderAdress . "</sender>";
        $xmlString .= "<text>" . $this->message . "</text>";
        if ($this->sendDateTime !== null) {
            $xmlString .= "<sendDateTime>" . $this->sendDateTime . "</sendDateTime>";
        }
        $xmlString .= "</message>";
        $xmlString .= "<recipients>";
        $xmlString .= "<gsm>" . $this->address . "</gsm>";
        $xmlString .= "</recipients>";
        $xmlString .= "</SMS>";
        // previously formatted XML data becomes value of "XML" POST variable
        return "XML=" . urlencode($xmlString);
    }

    private function initPostData() {
        return array(
            "login" => $this->login,
            "password" => $this->password,
            "destinataire" => $this->address,
            "message" => $this->message
        );
    }

    private function initAccountInfoData() {
        return array(
            "login" => $this->login,
            "password" => $this->password
        );
    }

    public function sendSms() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->urlSendSms);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->initPostData());
        // response of the POST request
        $response = curl_exec($ch);
        curl_close($ch);
        // write out the response
        return $response;
    }

    public function sendAccountInfo() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->urlAccountInfo);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->initAccountInfoData());
        // response of the POST request
        $response = curl_exec($ch);
        curl_close($ch);
        // write out the response
        return $response;
    }

    public function setAddress($address) {
        $this->address = $address;
        return $this;
    }

    public function setMessage($message) {
        $this->message = $message;
        return $this;
    }

    public function setSendDateTime($sendDateTime) {
        $this->sendDateTime = $sendDateTime;
        return $this;
    }

    public function attach(EventManagerInterface $events) {
        $sharedEvents = $events->getSharedManager();
        $this->listeners[] = $sharedEvents->attach('Application\Events\EventNotification', 'NotificationStockSMS', array($this, 'onSendNotification'), 100);
    }

    public function detach(EventManagerInterface $events = null) {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

    public function onSendNotification($e) {
        $params = $e->getParams();
        if ($params["destinataire"] && $params["message"] && strlen($params["destinataire"]) == 8) {
            $notif = new NotificationSMS();
            $notif->setAddress($params["destinataire"]);
            $notif->setMessage($params["message"]);
            $notif->start();
        }
        else {
            ob_start();
            print("Erreur survenue");
            print(" : ");
            print($params["destinataire"]);
            print(" / ");
            print($params["message"]);
            $content = ob_get_clean();
            file_put_contents("sms_statut.txt", $content . "\n", FILE_APPEND);
        }
    }
    
    public static function isValidPeriode($dateEnvoi, $minute = 5){
        $dateOperation = new DateTime("now");
        $dateDiff = $dateOperation->diff($dateEnvoi);
        if($dateDiff->days > 0 || $dateDiff->h > 1 || $dateDiff->i > $minute){
            return false;
        }
        return true;
    }
    
    public static function filterMobileNumber($mobileNumber, $codePays = 225){
        if(strlen($mobileNumber) == 8){
            return $mobileNumber;
        }
        elseif((substr($mobileNumber, 0, 1) == '+' || substr($mobileNumber, 0, 2) == '00') && (substr($mobileNumber, 1, 3) == $codePays || substr($mobileNumber, 2, 3) == $codePays) && ($numero = substr($mobileNumber, -8))){
            return $numero;
        }
        return false;
    }
    
    public static function genTokenSms($length = 6){
        $ranges = array(range('a', 'z'), range('A', 'Z'), range(1, 9));
        $code = '';
        for ($i = 0; $i < $length; $i++) {
            $rkey = array_rand($ranges);
            $vkey = array_rand($ranges[$rkey]);
            $code .= $ranges[$rkey][$vkey];
        }
        return $code;
    }
}
