<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Stock\Services\Notification;

use Exception;
//use Thread;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Mail;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

/**
 * Description of NotificationStockMail
 *
 * @author Wilfried
 */
class NotificationStockMail implements ListenerAggregateInterface {

    private $transport;
    private $from = "wkouakou@gmail.com";
    private $to;
    private $cc = array();
    private $bcc = array();
    private $sender = "Collar Place";
    private $replyTo = "wkouakou@gmail.com";
    private $subject = "Informations";
    private $differe = false;

    /* private $sendAt;

      public function __construct() {
      $this->sendAt = new DateTime('now');
      } */

    public function run() {
        $this->sendMail();
    }

    private function sendMail() {
        ob_start();
        try {
            $mes = 'Bonjour <b>M. KOUAKOU</b><br>Votre commande a été prise en compte.<br>Un commercial vous contactera dans l\'heure';
            $html = new MimePart($mes);
            $html->type = "text/html";

            $body = new MimeMessage();
            $body->addPart($html);

            $message = new Mail\Message();
            $message->setFrom($this->from);
            $message->addTo($this->to);
            $message->addReplyTo($this->replyTo);
            if ($this->cc) {
                $message->addCc($this->cc);
            }
            if ($this->bcc) {
                $message->addBcc($this->bcc);
            }
            $message->setSender($this->getFrom());
            $message->setSubject($this->subject);
            $message->setEncoding("UTF-8");
            $message->setBody($body);
            $this->getTransport()->send($message);
            echo "Mail envoyé à " . $this->to;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }

        $content = ob_get_clean();
        file_put_contents("mail_statut.txt", $content . "\n", FILE_APPEND);
    }

    public function getTransport() {
        if (!$this->transport) {
            $smtpOptions = new SmtpOptions();
            $smtpOptions->setHost('mail.progicia-afrik.com')
                    ->setName('mail.progicia-afrik.com') //Ok mais necessite d'activer la connexion des applications moins sécurisées dans les params de gmail
                    //->setName('smtp-mail.outlook.com') //OK
                    ->setPort(25)
                    ->setConnectionClass('login')
                    ->setConnectionConfig(array(
                        'username' => 'no-reply-afrik.com',
                        'password' => '@no-reply@progicia@',
                        'ssl' => 'ssl',
                            )
            );
            $this->transport = new SmtpTransport($smtpOptions);
        }
        return $this->transport;
    }

    public function attach(EventManagerInterface $events) {
        $sharedEvents = $events->getSharedManager();
        $this->listeners[] = $sharedEvents->attach('Application\Events\EventNotification', 'NotificationStockMail', array($this, 'onSendNotification'), 100);
    }

    public function detach(EventManagerInterface $events) {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

    public function onSendNotification($e) {

        try {
            $params = $e->getParams();
            $notif = new NotificationMail();
            $notif->setTo($params["to"]);
            $notif->setSubject($params["subject"]);
            $notif->run();
        } catch (Exception $exc) {
            ob_start();
            echo $exc->getMessage();
            $content = ob_get_clean();
            file_put_contents("mail_statut_error.txt", $content . "\n", FILE_APPEND);
        }
    }

    public function getFrom() {
        return $this->from;
    }

    public function getTo() {
        return $this->to;
    }

    public function getCc() {
        return $this->cc;
    }

    public function getBcc() {
        return $this->bcc;
    }

    public function getSender() {
        return $this->sender;
    }

    public function getReplyTo() {
        return $this->replyTo;
    }

    public function getSubject() {
        return $this->subject;
    }

    public function getDiffere() {
        return $this->differe;
    }

    public function setFrom($from) {
        $this->from = $from;
        return $this;
    }

    public function setTo($to) {
        $this->to = $to;
        return $this;
    }

    public function setCc($cc) {
        $this->cc = $cc;
        return $this;
    }

    public function setBcc($bcc) {
        $this->bcc = $bcc;
        return $this;
    }

    public function setSender($sender) {
        $this->sender = $sender;
        return $this;
    }

    public function setReplyTo($replyTo) {
        $this->replyTo = $replyTo;
        return $this;
    }

    public function setSubject($subject) {
        $this->subject = $subject;
        return $this;
    }

    public function setDiffere($differe) {
        $this->differe = $differe;
        return $this;
    }

}
