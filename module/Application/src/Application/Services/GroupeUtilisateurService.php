<?php

/*
  ##############  Author   : Wilfried
  ##############  Email    : wkouakou@gmail.com
  ##############  Date     : 09/08/2017
  ##############  File     : GroupeUtilisateurService.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Services;

interface GroupeUtilisateurService {
    
    public function getByLibelleLike($criteres = array(), $orderBy = array(), $limit = null, $offset = null);
    
    function getByLibelle($libelle);
}
