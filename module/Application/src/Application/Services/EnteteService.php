<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 7 juil. 2017 A  06:29:23
  ##############  File     : EnteteService.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Services;

interface EnteteService {
    
    public function getByLibelleLike($criteres = array(), $orderBy = array(), $limit = null, $offset = null);
    
    function getByLibelleAndQuestion($libelle, $questionId);
}
