<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 9 avr. 2017 A  19:32:59
  ##############  File     : TypeQuestionService.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Services;

interface TypeQuestionService {
    
    public function getByLibelleLike($criteres = array(), $orderBy = array(), $limit = null, $offset = null);
    
    function getByLibelle($libelle);
}
