<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 10 avr. 2017 A  04:51:25
  ##############  File     : TypePropositionServiceImpl.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Services\Impl;

use Application\Services\TypePropositionService;

class TypePropositionServiceImpl extends BaseServiceImpl implements TypePropositionService {

    public function init() {
        parent::init();
        $this->setRepository($this->em->getRepository('Application\Model\Entity\TypeProposition'));
    }

    public function getById($objectId) {
        $this->init();
        return parent::getById($objectId);
    }

    public function lister($criteria = array(), $orderBy = null, $limit = null, $offset = null) {
        $this->init();
        return parent::lister($criteria, $orderBy, $limit, $offset);
    }
}
