<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 7 juil. 2017 A  06:30:03
  ##############  File     : EnteteServiceImpl.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Services\Impl;

use Application\Services\EnteteService;
use Exception;

class EnteteServiceImpl extends BaseServiceImpl  implements EnteteService {
    
    public function init() {
        parent::init();
        $this->setRepository($this->em->getRepository('Application\Model\Entity\Entete'));
    }
    
    public function getById($objectId) {
        $this->init();
        return parent::getById($objectId);
    }
    
    public function lister($criteria = array(), $orderBy = null, $limit = null, $offset = null) {
        $this->init();
        return parent::lister($criteria, $orderBy, $limit, $offset);
    }

    public function getByLibelleLike($criteres = array(), $orderBy = array(), $limit = null, $offset = null) {
        try {
            $this->init();
            return $this->getRepository()->getByCritereLike($criteres, $orderBy, $limit, $offset);
        }
        catch (Exception $exc) {
            return array();
        }
    }

    public function getByLibelleAndQuestion($libelle, $questionId) {
        $this->init();
        return $this->getRepository()->findOneBy(array("libelle"=>$libelle, "question"=>$questionId));
    }

}


