<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Services\Impl;

use Application\Services\UtilisateurService;

/**
 *
 * @author wilfried
 */
class UtilisateurServiceImpl extends BaseServiceImpl  implements UtilisateurService {
    
    public function init() {
        parent::init();
        $this->setRepository($this->em->getRepository('Application\Model\Entity\Utilisateur'));
    }
    
    public function getById($objectId) {
        $this->init();
        return parent::getById($objectId);
    }
    
    public function lister($criteria = array(), $orderBy = null, $limit = null, $offset = null) {
        $this->init();
        return parent::lister($criteria, $orderBy, $limit, $offset);
    }
    
    public function getByEmail($email) {
        $this->init();
        return $this->getRepository()->findOneByEmail($email);
    }
}
