<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 10 avr. 2017 A  04:53:06
  ##############  File     : ParticiperServiceImpl.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Services\Impl;

use Application\Services\ParticiperService;

class ParticiperServiceImpl extends BaseServiceImpl  implements ParticiperService {
    
    public function init() {
        parent::init();
        $this->setRepository($this->em->getRepository('Application\Model\Entity\Participer'));
    }
    
    public function getById($objectId) {
        $this->init();
        return parent::getById($objectId);
    }
    
    public function lister($criteria = array(), $orderBy = null, $limit = null, $offset = null) {
        $this->init();
        return parent::lister($criteria, $orderBy, $limit, $offset);
    }

    public function getOneByCriteria($criteria = array()) {
        $this->init();
        return $this->getRepository()->findOneBy($criteria);
    }

    public function getCorrespondanceBy($sexe = null, $sondage = null, $offset = null, $limit = null) {
        try {
            $this->init();
            return $this->getRepository()->findCorrespondanceBy($sexe, $sondage, $offset, $limit);
        } catch (Exception $exc) {
            return [];
        }
    }
}


