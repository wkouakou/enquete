<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 10 avr. 2017 A  04:52:39
  ##############  File     : PropositionServiceImpl.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Services\Impl;

use Application\Services\PropositionService;

class PropositionServiceImpl extends BaseServiceImpl  implements PropositionService {
    
    public function init() {
        parent::init();
        $this->setRepository($this->em->getRepository('Application\Model\Entity\Proposition'));
    }
    
    public function getById($objectId) {
        $this->init();
        return parent::getById($objectId);
    }
    
    public function lister($criteria = array(), $orderBy = null, $limit = null, $offset = null) {
        $this->init();
        return parent::lister($criteria, $orderBy, $limit, $offset);
    }

    public function getByLibelleLike($criteres = array(), $orderBy = array(), $limit = null, $offset = null) {
        try {
            $this->init();
            return $this->getRepository()->getByCritereLike($criteres, $orderBy, $limit, $offset);
        }
        catch (Exception $exc) {
            return array();
        }
    }

    public function getByLibelleAndQuestion($libelle, $questionId) {
        $this->init();
        return $this->getRepository()->findOneBy(array("libelle"=>$libelle, "question"=>$questionId));
    }

    public function getByCritere($criteres = array()) {
        $this->init();
        return $this->getRepository()->findOneBy($criteres);
    }

}


