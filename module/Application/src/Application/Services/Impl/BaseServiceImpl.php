<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Services\Impl;

use Application\Services\BaseService;

/**
 * Description of BaseServiceImpl
 *
 * @author wilfried
 */
class BaseServiceImpl implements BaseService {

    use \Zend\ServiceManager\ServiceLocatorAwareTrait;

    protected $em;
    protected $repository;
    protected $storage;
    protected $flush = true;
    
    public function init() {
        $this->setEm($this->getServiceLocator()->get('doctrine.entitymanager.orm_default'));
    }
    
    public function editer($object) {
        try {
            $this->init();
            $this->em->persist($object);
            if ($this->getFlush() === TRUE) {
                $this->em->flush();
            }
            return $object;
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue lors de l'édition.\n" . $exc->getMessage());
        }
    }

    public function getById($objectId) {
        try {
            $this->init();
            return $this->repository->find($objectId);
        } catch (Exception $exc) {
            throw new Exception("Aucun enregistrement trouvé.");
        }
    }

    public function lister($criteria = array(), $orderBy = null, $limit = null, $offset = null) {
        try {
            $this->init();
            return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
            
        } catch (Exception $exc) {
            throw new Exception("Aucun enregistrement trouvé.");
        }
    }

    public function supprimer($object) {
        try {
            $this->init();
            $this->em->remove($object);
            if ($this->getFlush() === TRUE) {
                $this->em->flush();
            }
            return $object;
        } catch (Exception $exc) {
            throw new Exception("Erreur survenue lors de la suppression.");
        }
    }
    
    public function getEm() {
        return $this->em;
    }

    public function getRepository() {
        return $this->repository;
    }

    public function getStorage() {
        if (!$this->storage) {
            $this->storage = $this->getServiceLocator()->get('AitAuthStorage');
        }
        return $this->storage;
    }

    public function getFlush() {
        return $this->flush;
    }

    public function setEm($em) {
        $this->em = $em;
        return $this;
    }

    public function setRepository($repository) {
        $this->repository = $repository;
        return $this;
    }

    public function setStorage($storage) {
        $this->storage = $storage;
        return $this;
    }

    public function setFlush($flush) {
        $this->flush = $flush;
        return $this;
    }
}
