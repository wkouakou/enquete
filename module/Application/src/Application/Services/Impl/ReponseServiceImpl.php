<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 10 avr. 2017 A  04:52:00
  ##############  File     : ReponseServiceImpl.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Services\Impl;

use Application\Services\ReponseService;

class ReponseServiceImpl extends BaseServiceImpl  implements ReponseService {
    
    public function init() {
        parent::init();
        $this->setRepository($this->em->getRepository('Application\Model\Entity\Reponse'));
    }
    
    public function getById($objectId) {
        $this->init();
        return parent::getById($objectId);
    }
    
    public function lister($criteria = array(), $orderBy = null, $limit = null, $offset = null) {
        $this->init();
        return parent::lister($criteria, $orderBy, $limit, $offset);
    }
}


