<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 10 avr. 2017 A  04:50:30
  ##############  File     : GroupeUtilisateurServiceImpl.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Services\Impl;

use Application\Services\GroupeUtilisateurService;
use Exception;

class GroupeUtilisateurServiceImpl extends BaseServiceImpl  implements GroupeUtilisateurService {
    
    public function init() {
        parent::init();
        $this->setRepository($this->em->getRepository('Application\Model\Entity\GroupeUtilisateur'));
    }
    
    public function getById($objectId) {
        $this->init();
        return parent::getById($objectId);
    }
    
    public function lister($criteria = array(), $orderBy = null, $limit = null, $offset = null) {
        $this->init();
        return parent::lister($criteria, $orderBy, $limit, $offset);
    }

    public function getByLibelleLike($criteres = array(), $orderBy = array(), $limit = null, $offset = null) {
        try {
            $this->init();
            return $this->getRepository()->getByCritereLike($criteres, $orderBy, $limit, $offset);
        }
        catch (Exception $exc) {
            return array();
        }
    }

    public function getByLibelle($libelle) {
        $this->init();
        return $this->getRepository()->findOneByLibelle($libelle);
    }

}


