<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 9 avr. 2017 A  19:27:34
  ##############  File     : ParticipantService.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Services;

interface ParticipantService {
    
    public function getByNameLike($criteres = array(), $orderBy = array(), $limit = null, $offset = null);
    
    function getByEmail($email);
    
    function getByContact($contact);
}
