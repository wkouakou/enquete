<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 9 avr. 2017 A  19:32:22
  ##############  File     : SondageService.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Services;

interface SondageService {
    
    public function getByLibelleLike($criteres = array(), $orderBy = array(), $limit = null, $offset = null);
    
    function getByLibelleAndGroupe($libelle, $groupeId);
}