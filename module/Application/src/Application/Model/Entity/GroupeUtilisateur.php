<?php

/*
  ##############  Author   : Wilfried
  ##############  Email    : wkouakou@gmail.com
  ##############  Date     : 11 Juillet 2017 A  20:12
  ##############  File     : GroupeUtilisateur.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="app_groupe_utilisateur")
 */
class GroupeUtilisateur extends BaseEntity {
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
    */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Groupe")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $groupe;
    
    /**
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $utilisateur;
    
    public function getId() {
        return $this->id;
    }

    public function getGroupe() {
        return $this->groupe;
    }

    public function getUtilisateur() {
        return $this->utilisateur;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setGroupe($groupe) {
        $this->groupe = $groupe;
        return $this;
    }

    public function setUtilisateur($utilisateur) {
        $this->utilisateur = $utilisateur;
        return $this;
    }
}
