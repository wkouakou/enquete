<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 9 avr. 2017 A  19:00:07
  ##############  File     : Participer.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Application\Model\Repository\ParticiperRepository")
 * @ORM\Table(name="app_participer")
 */
class Participer extends BaseEntity {
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
    */
    private $id;
    
    /** @ORM\Column(type="string", length=255, nullable=true) */
    private $libelle;   
    
    /**
     * @ORM\ManyToOne(targetEntity="Sondage")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $sondage;
    
    /**
     * @ORM\ManyToOne(targetEntity="Participant")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $participant;
    
    //Getters and setters
    function getId() {
        return $this->id;
    }

    function getLibelle() {
        return $this->libelle;
    }

    function getSondage() {
        return $this->sondage;
    }

    function getParticipant() {
        return $this->participant;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setLibelle($libelle) {
        $this->libelle = $libelle;
    }

    function setSondage($sondage) {
        $this->sondage = $sondage;
    }

    function setParticipant($participant) {
        $this->participant = $participant;
    }


}
