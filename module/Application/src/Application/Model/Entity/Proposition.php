<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 9 avr. 2017 A  18:52:04
  ##############  File     : Proposition.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Application\Model\Repository\PropositionRepository")
 * @ORM\Table(name="app_proposition")
 */
class Proposition extends BaseEntity {
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
    * @JMS\Type("integer")
    */
    private $id;
    
    /** @ORM\Column(type="string", length=255)
     * @JMS\Type("string") */
    private $libelle;  
    
    /** @ORM\Column(type="integer", nullable=true)
     * @JMS\Type("integer")
     * Pour l'évaluation graduée */
    private $min = 0;
    
    /** @ORM\Column(type="integer", nullable=true)
     * @JMS\Type("integer")
     * Pour l'évaluation graduée */
    private $max = 0;
    
    /** @ORM\Column(type="integer", nullable=true)
     * @JMS\Type("integer")
     * Pour l'évaluation graduée */
    private $step = 0;
    
    /** @ORM\Column(type="boolean", nullable=true)
     * @JMS\Type("boolean")
     * Pour l'évaluation graduée, possibilité de choisir un intervalle */
    private $choixmultiple = false;
    
    /**
     * @ORM\ManyToOne(targetEntity="Question")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     * @JMS\Type("Application\Model\Entity\Question")
     */
    private $question;
    
    /**
     * @ORM\ManyToOne(targetEntity="TypeProposition")
     * @JMS\SerializedName("typeProposition")
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id")
     * @JMS\Type("Application\Model\Entity\TypeProposition")
     */
    private $typeProposition;
    
    //Getters and setters
    public function getId() {
        return $this->id;
    }

    public function getLibelle() {
        return $this->libelle;
    }

    public function getMin() {
        return $this->min;
    }

    public function getMax() {
        return $this->max;
    }

    public function getStep() {
        return $this->step;
    }

    public function getChoixmultiple() {
        return $this->choixmultiple;
    }

    public function getQuestion() {
        return $this->question;
    }

    public function getTypeProposition() {
        return $this->typeProposition;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setLibelle($libelle) {
        $this->libelle = $libelle;
        return $this;
    }

    public function setMin($min) {
        $this->min = $min;
        return $this;
    }

    public function setMax($max) {
        $this->max = $max;
        return $this;
    }

    public function setStep($step) {
        $this->step = $step;
        return $this;
    }

    public function setChoixmultiple($choixmultiple) {
        $this->choixmultiple = $choixmultiple;
        return $this;
    }

    public function setQuestion($question) {
        $this->question = $question;
        return $this;
    }

    public function setTypeProposition($typeProposition) {
        $this->typeProposition = $typeProposition;
        return $this;
    }
}