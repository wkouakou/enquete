<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Model\Entity;

/**
 * Description of BaseEntity
 *
 * @author wilfried
 */

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Gedmo\Mapping\Annotation as Gedmo;
/**
 * @ORM\MappedSuperclass
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class BaseEntity {
    
    /** @ORM\Column(type="boolean")
     * @JMS\Type("boolean") */
    private $etat = true;

    /**
     * @Gedmo\Timestampable(on="create")
     * @JMS\SerializedName("createdAt")
     * @JMS\Type("DateTime<'d-m-Y H:i:s'>")
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @JMS\SerializedName("updatedAt")
     * @JMS\Type("DateTime<'d-m-Y H:i:s'>")
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Model\Entity\Utilisateur")
     * @JMS\Exclude
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id")
     */
    protected $createdBy;
    
    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     * @JMS\SerializedName("deletedAt")
     * @JMS\Type("DateTime<'d-m-Y H:i:s'>") 
     */
    protected $deletedAt;
    
    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("editeePar")
     */
    public function getEditeePar() {
        return $this->getCreatedBy() !== null ? $this->getCreatedBy()->getFullName() : '';
    }

    public function getCreatedAt() {
        return $this->createdAt;
    }

    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    public function getCreatedBy() {
        return $this->createdBy;
    }

    public function getDeletedAt() {
        return $this->deletedAt;
    }

    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
        return $this;
    }

    public function setDeletedAt($deletedAt) {
        $this->deletedAt = $deletedAt;
        return $this;
    }
    
    public function getEtat() {
        return $this->etat;
    }

    public function setEtat($etat) {
        $this->etat = $etat;
        return $this;
    }

    //Ajout pour la recherche
    public function objetProperties() {
       $key = get_object_vars($this);
       return array_keys($key);
    }
}
