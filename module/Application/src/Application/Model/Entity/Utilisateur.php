<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Model\Entity;

/**
 * Description of Utilisateur
 *
 * @author wilfried
 */

use AuthAcl\Model\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Application\Model\Repository\UtilisateurRepository")
 * @ORM\Table(name="auth_utilisateur")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Utilisateur extends User{
    
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
    */
    private $id;
    
    /** @ORM\Column(type="text", nullable=true)
     * @JMS\Exclude */
    private $androidId;
    
    /** @ORM\Column(type="string", nullable=true, options={"default"="M."}, length=8) */
    private $civilite = "M.";
    
    /** @ORM\Column(type="text", nullable=true)
     * @JMS\Exclude */
    private $iosId;
    
    /** @ORM\Column(type="text", nullable=true)
     * @JMS\Exclude */
    private $blackberryId;
    
    /** @ORM\Column(type="text", nullable=true)
     * @JMS\Exclude */
    private $firefoxId;
    
    /** @ORM\Column(type="text", nullable=true)
     * @JMS\Exclude */
    private $windowId;

    /**
     * @ORM\ManyToOne(targetEntity="Participant")
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id")
     */
    private $participant;
    
    /**
     * @ORM\ManyToOne(targetEntity="AuthAcl\Model\Entity\Role")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $role;

    /**
     * @Gedmo\Timestampable(on="update")
     * @JMS\SerializedName("updatedAt")
     * @JMS\Type("DateTime<'d-m-Y H:i:s'>") 
     * @ORM\Column(type="datetime",nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @JMS\Exclude
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id")
     */
    private $createdBy;
    
    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     * @JMS\Exclude
     * @JMS\Type("DateTime<'d-m-Y H:i:s'>") 
     */
    private $deletedAt;
    
    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("editeePar")
     */
    public function getEditeePar() {
        return $this->getCreatedBy() !== null ? $this->getCreatedBy()->getFullName() : '';
    }
    public function getParticipant() {
        return $this->participant;
    }

    public function setParticipant($participant) {
        $this->participant = $participant;
        return $this;
    }

        public function getId() {
        return $this->id;
    }

    public function getAndroidId() {
        return $this->androidId;
    }

    public function getCivilite() {
        return $this->civilite;
    }

    public function getIosId() {
        return $this->iosId;
    }

    public function getBlackberryId() {
        return $this->blackberryId;
    }

    public function getFirefoxId() {
        return $this->firefoxId;
    }

    public function getWindowId() {
        return $this->windowId;
    }

    public function getRole() {
        return $this->role;
    }

    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    public function getCreatedBy() {
        return $this->createdBy;
    }

    public function getDeletedAt() {
        return $this->deletedAt;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setAndroidId($androidId) {
        $this->androidId = $androidId;
        return $this;
    }

    public function setCivilite($civilite) {
        $this->civilite = $civilite;
        return $this;
    }

    public function setIosId($iosId) {
        $this->iosId = $iosId;
        return $this;
    }

    public function setBlackberryId($blackberryId) {
        $this->blackberryId = $blackberryId;
        return $this;
    }

    public function setFirefoxId($firefoxId) {
        $this->firefoxId = $firefoxId;
        return $this;
    }

    public function setWindowId($windowId) {
        $this->windowId = $windowId;
        return $this;
    }

    public function setRole($role) {
        $this->role = $role;
        return $this;
    }

    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
        return $this;
    }

    public function setDeletedAt($deletedAt) {
        $this->deletedAt = $deletedAt;
        return $this;
    }


}

