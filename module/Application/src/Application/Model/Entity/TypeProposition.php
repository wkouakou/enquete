<?php

/*
  ##############  Author   : Wilfried
  ##############  Email    : wkouakou@gmail.com
  ##############  Date     : 11 Juillet 2017
  ##############  File     : TypeProposition.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="par_type_proposition")
 */
class TypeProposition extends BaseEntity {
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="smallint")
    * @JMS\Type("integer")
    */
    private $id;
    
    /** @ORM\Column(type="string", length=255, unique=true)
     * @JMS\Type("string") */
    private $libelle; 
    
    /** @ORM\Column(type="string", length=255, nullable=true)
     * @JMS\Type("string") */
    private $type;   
    
    //Getters and setters
    public function getId() {
        return $this->id;
    }

    public function getLibelle() {
        return $this->libelle;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setLibelle($libelle) {
        $this->libelle = $libelle;
        return $this;
    }
    
    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
        return $this;
    }
}