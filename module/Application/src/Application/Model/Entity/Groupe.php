<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 9 avr. 2017 A  18:23:27
  ##############  File     : Groupe.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Application\Model\Repository\GroupeRepository")
 * @ORM\Table(name="par_groupe")
 */
class Groupe extends BaseEntity {
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="smallint")
     * @JMS\Type("integer")
    */
    private $id;
    
    /** @ORM\Column(type="string", length=255, unique=true)
     * @JMS\Type("string") */
    private $libelle;   
    /**
     * @var integer 
     */
    protected $sondages = 0; 
    
    //Getters and setters
    public function getId() {
        return $this->id;
    }

    public function getLibelle() {
        return $this->libelle;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setLibelle($libelle) {
        $this->libelle = $libelle;
        return $this;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("sondages")
     * Pour le nombre de personne dans le sondage
     */
    public function getSondages() {
        return $this->sondages;
    }
    public function setSondages($sondages) {
        $this->sondages = $sondages;
        return $this;
    }
}