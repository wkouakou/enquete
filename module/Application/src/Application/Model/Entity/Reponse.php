<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 9 avr. 2017 A  19:05:04
  ##############  File     : Reponse.php
  ##############  Edit Part ###################
  ##############  Date     : 11 Juillet 2017 20:19
  ##############  Author   : wkouakou@gmail.com
 */

namespace Application\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Application\Model\Repository\ReponseRepository")
 * @ORM\Table(name="app_reponse")
 */
class Reponse extends BaseEntity {
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
     * @JMS\Type("integer")
    */
    private $id;  
    
    /** @ORM\Column(type="integer", nullable=true)
     * @JMS\Type("integer")
     * Pour l'évaluation graduée */
    private $mini = 0;
    
    /** @ORM\Column(type="integer", nullable=true)
     * @JMS\Type("integer")
     * Pour l'évaluation graduée */
    private $maxi = 0;
    
    /** @ORM\Column(type="integer", nullable=true)
     * @JMS\SerializedName("colonneIndex")
     * @JMS\Type("integer")
     * Pour l'évaluation graduée */
    private $colonneIndex = 0;
    
    /** @ORM\Column(type="string", length=255, nullable=true)
     * @JMS\Type("string") */
    private $libre;
    
    /**
     * @ORM\ManyToOne(targetEntity="Proposition",cascade={"refresh"})
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     * @JMS\Type("Application\Model\Entity\Proposition")
     */
    private $proposition;
    
    /**
     * @ORM\ManyToOne(targetEntity="Proposition")
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id")
     * @JMS\Type("Application\Model\Entity\Proposition")
     */
    private $colonne;
    
    /**
     * @ORM\ManyToOne(targetEntity="Participant", inversedBy="reponses")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     * @JMS\Type("Application\Model\Entity\Participant")
     */
    private $participant;
    
    //Getters and setters
    public function getId() {
        return $this->id;
    }

    public function getLibre() {
        return $this->libre;
    }

    public function getProposition() {
        return $this->proposition;
    }

    public function getColonne() {
        return $this->colonne;
    }

    public function getParticipant() {
        return $this->participant;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setLibre($libre) {
        $this->libre = $libre;
        return $this;
    }

    public function setProposition($proposition) {
        $this->proposition = $proposition;
        return $this;
    }

    public function setColonne($colonne) {
        $this->colonne = $colonne;
        return $this;
    }

    public function setParticipant($participant) {
        $this->participant = $participant;
        return $this;
    }
    
    public function getColonneIndex() {
        return $this->colonneIndex;
    }
    
    public function getMini() {
        return $this->mini;
    }

    public function getMaxi() {
        return $this->maxi;
    }

    public function setMini($mini) {
        $this->mini = $mini;
        return $this;
    }

    public function setMaxi($maxi) {
        $this->maxi = $maxi;
        return $this;
    }

    public function setColonneIndex($colonneIndex) {
        $this->colonneIndex = $colonneIndex;
        return $this;
    }
}
