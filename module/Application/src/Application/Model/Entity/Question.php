<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 9 avr. 2017 A  18:44:32
  ##############  File     : Question.php
  ##############  Edit Part ###################
  ##############  Date     : 11 Juillet 2017 A 20:17
  ##############  Author   : wkouakou@gmail.com
 */

namespace Application\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Application\Model\Repository\QuestionRepository")
 * @ORM\Table(name="app_question")
 */
class Question extends BaseEntity {
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
     * @JMS\Type("integer")
    */
    private $id;
    
    /** @ORM\Column(type="text")
     * @JMS\Type("string") */
    private $libelle;
    
    /** @ORM\Column(type="string", length=255, nullable=true)
     * @JMS\Type("string") */
    private $lien;
    
    /**
     * @ORM\ManyToOne(targetEntity="TypeQuestion")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     * @JMS\SerializedName("typeQuestion")
     * @JMS\Type("Application\Model\Entity\TypeQuestion")
     */
    private $typeQuestion;
    
    /**
     * @ORM\ManyToOne(targetEntity="Sondage")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     * @JMS\Type("Application\Model\Entity\Sondage")
     */
    private $sondage;
    
    /**
     * @ORM\ManyToOne(targetEntity="Question")
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id")
     * @JMS\Type("Application\Model\Entity\Question")
     */
    private $question;
    
    /**
     * @ORM\ManyToOne(targetEntity="Proposition")
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id")
     * @JMS\Type("Application\Model\Entity\Proposition")
     */
    private $proposition;
    
    //Getters and setters
    function getId() {
        return $this->id;
    }

    function getLibelle() {
        return $this->libelle;
    }

    function getTypeQuestion() {
        return $this->typeQuestion;
    }

    function getSondage() {
        return $this->sondage;
    }

    function getQuestion() {
        return $this->question;
    }

    function getProposition() {
        return $this->proposition;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setLibelle($libelle) {
        $this->libelle = $libelle;
    }

    function setTypeQuestion($typeQuestion) {
        $this->typeQuestion = $typeQuestion;
    }

    function setSondage($sondage) {
        $this->sondage = $sondage;
    }

    function setQuestion($question) {
        $this->question = $question;
    }

    function setProposition($proposition) {
        $this->proposition = $proposition;
    }
    
    public function getLien() {
        return $this->lien;
    }

    public function setLien($lien) {
        $this->lien = $lien;
        return $this;
    }


}
