<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 9 avr. 2017 A  18:36:58
  ##############  File     : Sondage.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Application\Model\Repository\SondageRepository")
 * @ORM\Table(name="app_sondage")
 */
class Sondage extends BaseEntity {
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="smallint")
     * @JMS\Type("integer")
    */
    private $id;
    
    /** @ORM\Column(type="string", length=255)
     * @JMS\Type("string") */
    private $libelle;   
    
    /**
     * @ORM\ManyToOne(targetEntity="Groupe")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     * @JMS\Type("Application\Model\Entity\Groupe")
     */
    private $groupe;
    
    /** @ORM\Column(type="integer", nullable=true, options={"default": 0})
     * @JMS\Exclude
     * @JMS\SerializedName("nombreParticipant") */
    private $nombreParticipant = 0; 
    
    /**
     * @var integer 
     */
    protected $participants = 0; 
    
    //Getters and setters
    public function getId() {
        return $this->id;
    }

    public function getLibelle() {
        return $this->libelle;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function setLibelle($libelle) {
        $this->libelle = $libelle;
        return $this;
    }
    
    function getGroupe() {
        return $this->groupe;
    }

    function setGroupe($groupe) {
        $this->groupe = $groupe;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("participants")
     * Pour le nombre de personne dans le sondage
     */
    public function getParticipants() {
        return $this->participants;
    }
    public function setParticipants($participants) {
        $this->participants = $participants;
        return $this;
    }

    public function getNombreParticipant() {
        return $this->nombreParticipant;
    }

    public function setNombreParticipant($nombreParticipant) {
        $this->nombreParticipant = $nombreParticipant;
        return $this;
    }
}