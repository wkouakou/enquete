<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 9 avr. 2017 A  18:56:06
  ##############  File     : Participant.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Application\Model\Repository\ParticipantRepository")
 * @ORM\Table(name="app_participant")
 */
class Participant extends BaseEntity {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @JMS\Type("integer")
     */
    private $id;

    /** @ORM\Column(type="string", length=96)
     * @JMS\SerializedName("fullName")
     * @JMS\Type("string") */
    private $fullName;

    /** @ORM\Column(type="string", length=16, unique=true, nullable=true)
     * @JMS\Type("string") */
    private $contact;

    /** @ORM\Column(type="string", length=16, nullable=true)
     * @JMS\Type("string") */
    private $sexe;

    /** @ORM\Column(type="string", length=96, unique=true, nullable=true)
     * @JMS\Type("string") */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity="Application\Model\Entity\Reponse", mappedBy="participant")
     * @JMS\Type("ArrayCollection<Application\Model\Entity\Reponse>")
     */
    private $reponses;

    //Getters and setters
    function getId() {
        return $this->id;
    }

    public function getSexe() {
        return $this->sexe;
    }

    public function setSexe($sexe) {
        $this->sexe = $sexe;
        return $this;
    }
    
    function getFullName() {
        return $this->fullName;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("nom")
     */
    function getNom() {
        return in_array(strtolower($this->fullName), ["anonyme", "inconnu", "nc"]) || strstr(strtolower($this->fullName), "anonyme") ? $this->fullName . ' ' . $this->id : $this->fullName;
    }

    function getContact() {
        return $this->contact;
    }

    function getEmail() {
        return $this->email;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setFullName($fullName) {
        $this->fullName = $fullName;
    }

    function setContact($contact) {
        $this->contact = $contact;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    public function getReponses() {
        return $this->reponses;
    }

    public function setReponses($reponses) {
        $this->reponses = $reponses;
        return $this;
    }

}
