<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 9 avr. 2017 A  19:14:24
  ##############  File     : ParticiperRepository.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Model\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Exception;

class ParticiperRepository extends EntityRepository {
    
    public function findCorrespondanceBy($sexe = null, $sondage = null, $offset = null, $limit = null) {
        try {
            $qb = $this->createQueryBuilder('p')
                    ->where("p.deletedAt IS NULL");

            //
            if ($sexe !== null) {
                $qb->join('Application\Model\Entity\Participant', 'pa', Join::WITH, 'p.participant = pa.id');
                $qb->andWhere("pa.sexe = :sexeParticipant");
                $qb->setParameter("sexeParticipant", $sexe);
            }
            //
            if ($sondage) {
                $qb->join('Application\Model\Entity\Sondage', 'so', Join::WITH, 'p.sondage = so.id');
                $qb->andWhere("so.id = :sondageId");
                $qb->setParameter("sondageId", $sondage);
            }

            if ($offset !== null) {
                $qb->setFirstResult($offset);
            }
            if ($limit) {
                $qb->setMaxResults($limit);
            }

            $query = $qb->getQuery();
            
//            ob_start();
//            echo $query->getDQL();
//            $content = ob_get_clean();
//            file_put_contents("participer_repo.txt", $content . "\n", FILE_APPEND);
            
            return $query->getResult();
        } catch (Exception $exc) {
//            ob_start();
//            echo $exc->getMessage();
//            $content = ob_get_clean();
//            file_put_contents("erreur_participer_repo.txt", $content . "\n", FILE_APPEND);
            return array();
        }
    }

}