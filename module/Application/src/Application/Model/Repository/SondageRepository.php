<?php

/*
  ##############  Author   : AFOLABI Jamal Deen
  ##############  Email    : jamaldeen25@gmail.com
  ##############  Date     : 9 avr. 2017 A  19:09:27
  ##############  File     : SondageRepository.php
  ##############  Edit Part ###################
  ##############  Date     :
  ##############  Author   :
 */

namespace Application\Model\Repository;

use Doctrine\ORM\EntityRepository;

class SondageRepository extends EntityRepository {
    
    public function getByCritereLike($criteres, $orderBy, $limit, $offset) {
        try {
            $qb = $this->createQueryBuilder('quest')
                    ->where("quest.id IS NOT NULL");
            //echo $criteres["question"];exit;
            $qb->andWhere($qb->expr()->orX($qb->expr()->like("quest.libelle", ":question")));
            $first = true;
            foreach ($orderBy as $sort => $order) {
                if ($first) {
                    $qb->orderBy($sort, $order);
                } else {
                    $first = false;
                    $qb->addOrderBy($sort, $order);
                }
            }

            if ($offset !== null) {
                $qb->setFirstResult($offset);
            }
            if ($limit) {
                $qb->setMaxResults($limit);
            }

            //Criteres
            if (array_key_exists("deletedAt", $criteres)) {
                if ($criteres["deletedAt"]) {
                    $qb->andWhere("quest.deletedAt = :deletedAt");
                    $qb->setParameter("deletedAt", $criteres["deletedAt"]);
                } else {
                    $qb->andWhere("quest.deletedAt IS NULL");
                }
            }
            $qb->setParameter("question", $criteres["question"]);
            foreach ($criteres as $critere => $valeurRecherchee) {
                if ($critere != "question" && $critere != "deletedAt") {
                    $qb->andWhere("quest.".$critere . " = :" . $critere);
                    $qb->setParameter($critere, $valeurRecherchee);
                }
            }
            $query = $qb->getQuery();
            return $query->getResult();
        } catch (Exception $exc) {
            return array();
        }
    }
}