<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Application\Utils\ServiceLocatorFactory;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module {

    protected $whitelist = array(
        'auth',
        'inactive',
        'missing',
        'auth/default',
        'auth/logout',
        
        'mobile/login',
        'mobile/logout',
        
        'app/participant/ajouter',
        'application/pathologie',
    );

    public function onBootstrap(MvcEvent $e) {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $eventManager->attach('dispatch', array($this, 'loadConfiguration'));
//        $eventManager->attach(new NotificationGCM());
//        $eventManager->attach(new NotificationSMS());
//        $eventManager->attach(new NotificationMail());
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function loadConfiguration(MvcEvent $e) {
        $application = $e->getApplication();
        $sm = $application->getServiceManager();
        ServiceLocatorFactory::setInstance($sm);
        $controller = $e->getTarget();
        $controllerClass = get_class($controller);
        $moduleNamespace = substr($controllerClass, 0, strpos($controllerClass, '\\'));

        //Whitelist
//        $config = $sm->get('Config');
//        $whiteList = $config["whitelist"];
        //$sm = $e->getApplication()->getServiceManager();
        $router = $sm->get('router');
        $request = $sm->get('request');
        $matchedRoute = $router->match($request);
        $routeName = $matchedRoute->getMatchedRouteName();
        $auth = $sm->get('doctrine.authenticationservice.orm_default');

        $heders = $request->getHeaders();

        if ($heders->get("iToken") !== FALSE) {
            $token = $heders->get("iToken")->getFieldValue();
            
            if ($token) {//Si le token existe
                $futFile = file_get_contents(__DIR__ . '/../../config/param.txt');
                $scripts = new \Zend\Crypt\Password\Bcrypt();
                $scripts->setCost(12);
                if ($scripts->verify($futFile, $token)) { //Comparaison du token ok
                    return;
                }
            }

            if (!in_array($routeName, $this->whitelist)) { //si route protege alors rediriger sur la route mobileroute
                $resp = new \Zend\Http\Response();
                $resp->getHeaders()->clearHeaders();
                $resp->getHeaders()->addHeaderLine(
                        'Location', $e->getRouter()->assemble(array(), array('name' => 'mobileroute'))
                );
                $resp->setStatusCode(302);
                return $resp;
            }
            return;
        }


        $template = "layout/layout";
        switch ($moduleNamespace) {
            case "Application":
                $template = "layout/layout";
                break;
            case "AuthAcl":
                $template = "layout/login";
                break;
            default:
                $template = "layout/layout";
                break;
        }
        //Layout
        $layout = $e->getViewModel();
        $layout->setTemplate($template);

        //Authentification result
        if (!$auth->hasIdentity() && !in_array($routeName, $this->whitelist)) {
            $response = $e->getResponse();
            $response->getHeaders()->addHeaderLine(
                    'Location', $e->getRouter()->assemble(array(), array('name' => 'auth'))
            );
            $response->setStatusCode(302);
            return $response;
        }

        if (!in_array($routeName, $this->whitelist) && $auth->getIdentity()->getStatus() === false) {
            $response = $e->getResponse();
            $response->getHeaders()->addHeaderLine(
                    'Location', $e->getRouter()->assemble(array(), array('name' => 'inactive'))
            );
            $response->setStatusCode(302);
            return $response;
        }
        
        return;
    }

}
