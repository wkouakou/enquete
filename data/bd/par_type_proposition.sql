-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 17 Juillet 2017 à 06:08
-- Version du serveur :  5.5.21
-- Version de PHP :  5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `2017_it_enquete`
--

-- --------------------------------------------------------

--
-- Structure de la table `par_type_proposition`
--

CREATE TABLE IF NOT EXISTS `par_type_proposition` (
  `id` smallint(6) NOT NULL,
  `etat` tinyint(1) NOT NULL,
  `libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `createdBy_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `par_type_proposition`
--

INSERT INTO `par_type_proposition` (`id`, `etat`, `libelle`, `type`, `createdAt`, `updatedAt`, `deletedAt`, `createdBy_id`) VALUES
(1, 1, 'Choix simple', 'radio', NULL, NULL, NULL, NULL),
(2, 1, 'Choix multiple', 'checkbox', NULL, NULL, NULL, NULL),
(3, 1, 'Choix texte', 'text', NULL, NULL, NULL, NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `par_type_proposition`
--
ALTER TABLE `par_type_proposition`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_5C3FB5D1A4D60759` (`libelle`), ADD KEY `IDX_5C3FB5D13174800F` (`createdBy_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `par_type_proposition`
--
ALTER TABLE `par_type_proposition`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `par_type_proposition`
--
ALTER TABLE `par_type_proposition`
ADD CONSTRAINT `FK_5C3FB5D13174800F` FOREIGN KEY (`createdBy_id`) REFERENCES `auth_utilisateur` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
